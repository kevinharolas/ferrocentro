package ferreteriakyms.controller.ventas;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Connection;
import java.sql.DriverManager;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.event.ValueChangeEvent;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

import ferreteriakyms.controller.JSFUtil;
import ferreteriakyms.controller.factura.ReporteFactura;
import ferreteriakyms.controller.factura.ReporteProforma;
import ferreteriakyms.controller.seguridades.BeanSegLogin;
import ferreteriakyms.model.ventas.managers.ManagerVentas;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import ferreteriakyms.model.core.entities.Cliente;
import ferreteriakyms.model.core.entities.DetalleVenta;
import ferreteriakyms.model.core.entities.MetodoPago;
import ferreteriakyms.model.core.entities.TipoPago;
import ferreteriakyms.model.core.entities.Producto;
import ferreteriakyms.model.core.entities.Proforma;
import ferreteriakyms.model.core.entities.ThmEmpleado;
import ferreteriakyms.model.core.entities.Venta;
import ferreteriakyms.model.seguridades.managers.ManagerSeguridades;

@Named
@SessionScoped
public class BeanKymsVentas implements Serializable {

	@EJB
	private ManagerVentas mVenta;
	@Inject
	private BeanSegLogin beanSegLogin;;

	// LISTA PARA CALCULAR TOTALES
	private List<DetalleVenta> listaTotal;
	private double subtotalFactura;
	private double IVAFactura;
	private double totalFactura;

	public BeanKymsVentas() {
	}

	@PostConstruct
	public void inicializar() {
		listaClientes = mVenta.allClientes();
		listaProductos = mVenta.allProductos();
		listaPagos = mVenta.allPagos();

		listaFacturas = mVenta.allFacturas();
		listaMetodoPagos = mVenta.allMetodoPagos();
		listaFacturasParaDevolver = mVenta.findFacturaDevolverByEstadoDevolver(false);
		nuevoCliente = new Cliente();
		listaProformas = mVenta.allProformas();

		detallesDevolucion = new ArrayList<DetalleVenta>();
		devolverTodo = true;
		PagoSeleccionado = true;
		descuentoFactura = "";

		numFactura = mVenta.numeroFactura(Venta.class, "idVenta");
		numProforma = mVenta.numeroFactura(Proforma.class, "idProforma");

	}

	// ============================== NAVEGACION ================================
	public String Facturacion() {
		Limpiar();
		return "facturacion";
	}

	public String Proforma() {
		Limpiar();
		return "proforma";
	}

	public String RegesarAfacturacion() {
		Limpiar();
		return "facturacion";
	}

	public String RegesarAproforma() {
		Limpiar();
		return "proforma";
	}

	private void Limpiar() {
		informacionCliente = new Cliente();
		subtotalFactura = 0;
		IVAFactura = 0;
		totalFactura = 0;
		descuentoFactura = "";
	}

	// ============================== CLIENTE ================================

	private List<Cliente> listaClientes;
	private Cliente nuevoCliente;
	private Cliente editarCliente;
	private String identificacionCliente; // Variable de autocomplete
	private Cliente informacionCliente; // Infomacion, despues de ser seleccionado con el autocomplete

	public void actionListenerInsertarCliente() {
		try {
			mVenta.insertarCliente(beanSegLogin.getLoginDTO(), nuevoCliente);
			nuevoCliente = new Cliente();
			listaClientes = mVenta.allClientes();
			JSFUtil.crearMensajeINFO("Cliente insertado");
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}

	public void actionListenerEliminarCliente(Cliente cli) {
		try {
			mVenta.eliminarCliente(cli);
			listaClientes = mVenta.allClientes();
			JSFUtil.crearMensajeINFO("Cliente eliminado");
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}

	public void actionListenerActualizarCliente() {
		try {
			mVenta.actualizarCliente(editarCliente);
			JSFUtil.crearMensajeINFO("Cliente actualizado");
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}

	public void actionSelectClient(Cliente cli) {
		editarCliente = cli;
	}

	// ================================= REGISTRAR FACTURA
	// ============================================
	private List<Producto> listaProductos;
	private List<TipoPago> listaPagos;
	private List<MetodoPago> listaMetodoPagos;
	private Venta factura; // Factura registrar
	private Venta facturaPagar; // Factura que se va a pagar
	private Venta facturaVer; // Factura seleccionada para ver
	private int cantidad;
	private int idPago;
	private int idMetodoPago;
	private String descuentoFactura;
	private Date fechPago;
	private boolean PagoSeleccionado; // Select para saber si paga al contado o queda pendiente
	private int mostrarStock;
	private String descripcionProducto;
	private Producto productoSeleccionado; // Producto seleccionado con el autocomplete
	private String numFactura;

	public List<String> completeTextCliente(String query) { // Buscar Cliente
		String queryLowerCase = query.toLowerCase();
		List<String> clientList = new ArrayList<>();
		for (Cliente cliente : listaClientes) {
			clientList.add(cliente.getIdentificacion());
		}
		return clientList.stream().filter(t -> t.toLowerCase().startsWith(queryLowerCase)).collect(Collectors.toList());
	}

	public List<String> completeTextProducto(String query) { // BuscarProducto
		String queryLowerCase = query.toLowerCase();
		List<String> prodList = new ArrayList<>();
		for (Producto prod : listaProductos) {
			prodList.add(prod.getDescripcion() + ": " + prod.getUnidadMedida().getDescripcion());
		}
		return prodList.stream().filter(t -> t.toLowerCase().startsWith(queryLowerCase)).collect(Collectors.toList());
	}

	public void obtenerInformacionCliente(AjaxBehaviorEvent e) {
		try {
			informacionCliente = mVenta.findOneClient(identificacionCliente);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	public void handleChange() { // Bloquea el calendario de credito
		if (idPago == 2) {
			PagoSeleccionado = false;
		} else {
			PagoSeleccionado = true;
		}
	}

	public void actionListenerAdicionarProducto() throws Exception {

		double resta = productoSeleccionado.getStock() - cantidad;

		if (resta > 0) {
			factura = mVenta.adicionarProducto(factura, cantidad, productoSeleccionado);
			CalcularFactura(factura);
			cantidad = 0;

			int cont = 0;
			int posicion = 0;
			for (Producto p : listaProductos) {
				if (p.getIdProducto() == productoSeleccionado.getIdProducto()) {
					posicion = cont;
				}
				cont++;
			}
			listaProductos.remove(posicion);

			if (resta <= productoSeleccionado.getStockMinimo()) {
				JSFUtil.crearMensajeWARN("Productos por terminarse STOCK " + resta);
			}

		} else {
			JSFUtil.crearMensajeERROR("No existe suficiente productos");
		}
	}

	public void actionListenerEliminarDetalle(int rowIndex) { // Modificar eliminacion de detalle en la interfaz;
		Producto p = factura.getDetalleVentas().get(rowIndex).getProducto();
		listaProductos.add(p);
		factura.getDetalleVentas().remove(rowIndex);
		CalcularFactura(factura);
	}

	public void actionListenerRegistrarFactura() {
		try {
			mVenta.registrarFactura(beanSegLogin.getLoginDTO(), factura, identificacionCliente, idPago,
					PagoSeleccionado, fechPago, idMetodoPago, Double.parseDouble(descuentoFactura));
			factura = new Venta();
			listaProductos = mVenta.allProductos();
			listaFacturas = mVenta.allFacturas();
			informacionCliente = new Cliente();
			listaFacturasParaDevolver = mVenta.findFacturaDevolverByEstadoDevolver(false);
			numFactura = mVenta.numeroFactura(Venta.class, "idVenta");
			JSFUtil.crearMensajeINFO("Factura registrada correctamente");
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
		subtotalFactura = 0;
		IVAFactura = 0;
		totalFactura = 0;
		mostrarStock = 0;
	}

	private void CalcularFactura(Venta facVer) {
		subtotalFactura = 0;
		IVAFactura = 0;
		totalFactura = 0;
		for (int i = 0; i < facVer.getDetalleVentas().size(); i++) {
			subtotalFactura += facVer.getDetalleVentas().get(i).getPrecio().doubleValue();
		}
		IVAFactura = Math.round((subtotalFactura * 0.12) * 100.0) / 100.0;
		totalFactura = Math.round((subtotalFactura + IVAFactura) * 100.0) / 100.0;
	}

	public void MostrarStock(AjaxBehaviorEvent e) {
		try {

			String[] parts = descripcionProducto.split(":");
			productoSeleccionado = mVenta.findOneProducto(parts[0]);
			mostrarStock = productoSeleccionado.getStock();
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	public void GenerarPDFactura() {

		ReporteFactura m = new ReporteFactura();
		try {
			m.Generar(facturaVer, subtotalFactura, IVAFactura);
			JSFUtil.crearMensajeINFO("Reporte generado");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	// ================================= REGISTRAR PROFORMA
	// ============================================
	private Proforma proforma; // Proforma registrar
	private Proforma proformVer; // Proforma seleccionada para ver
	private Proforma proformFacturar; // Proforma seleccionada para ver
	private int cantidadDetProforma;
	private List<Proforma> listaProformas;
	private List<Proforma> listaProformasPorFecha;
	private Date fechaVencimientoProforma;
	private String numProforma;

	public void actionListenerAdicionarProductoProforma() throws Exception {

		double resta = productoSeleccionado.getStock() - cantidadDetProforma;

		if (resta > 0) {

			proforma = mVenta.adicionarProductoProforma(proforma, cantidadDetProforma, productoSeleccionado);
			CalcularProforma(proforma);
			cantidadDetProforma = 0;

			int cont = 0;
			int posicion = 0;
			for (Producto p : listaProductos) {
				if (p.getIdProducto() == productoSeleccionado.getIdProducto()) {
					posicion = cont;
				}
				cont++;
			}
			listaProductos.remove(posicion);

			if (resta <= productoSeleccionado.getStockMinimo()) {
				JSFUtil.crearMensajeWARN("Productos por terminarse STOCK: " + resta);
			}

		} else {
			JSFUtil.crearMensajeERROR("No existe suficientes productos");
		}

	}

	public void actionListenerEliminarDetalleProforma(int rowIndex) { // Modificar eliminacion de detalle en la
																		// interfaz;
		Producto p = proforma.getDetalleProformas().get(rowIndex).getProducto();
		listaProductos.add(p);
		proforma.getDetalleProformas().remove(rowIndex);
		CalcularProforma(proforma);
	}

	public void actionListenerRegistrarProfroma() {
		try {
			mVenta.registrarProforma(beanSegLogin.getLoginDTO(), proforma, identificacionCliente,
					fechaVencimientoProforma, Double.parseDouble(descuentoFactura));
			proforma = new Proforma();
			listaProductos = mVenta.allProductos();
			listaProformas = mVenta.allProformas();
			numProforma = mVenta.numeroFactura(Proforma.class, "idProforma");
			JSFUtil.crearMensajeINFO("Proforma registrada correctamente");
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
		subtotalFactura = 0;
		IVAFactura = 0;
		totalFactura = 0;
		mostrarStock = 0;
	}

	public void actionListenerRegistrarProformaParaFacturar() { // Facturar Profroma
		try {
			Venta facturaProforma = new Venta();
			facturaProforma.setDetalleVentas(new ArrayList<DetalleVenta>());
			facturaProforma.setFecha(new Date());
			facturaProforma.setTotal(new BigDecimal(0));
			facturaProforma.setEstado(true);

			for (int i = 0; i < proformFacturar.getDetalleProformas().size(); i++) {
				DetalleVenta detalle = new DetalleVenta();
				detalle.setCantidad(proformFacturar.getDetalleProformas().get(i).getCantidad());
				detalle.setPrecio(new BigDecimal(proformFacturar.getDetalleProformas().get(i).getPrecio().doubleValue())
						.setScale(2, RoundingMode.HALF_UP));
				detalle.setVenta(facturaProforma);
				detalle.setProducto(proformFacturar.getDetalleProformas().get(i).getProducto());
				facturaProforma.getDetalleVentas().add(detalle);

			}

			facturaProforma.setTotal(facturaProforma.getTotal()
					.add(new BigDecimal(proformFacturar.getTotal().doubleValue()).setScale(2, RoundingMode.HALF_UP)));
			facturaProforma.setCliente(proformFacturar.getCliente());
			facturaProforma.setImpuesto(
					new BigDecimal(proformFacturar.getImpuesto().doubleValue()).setScale(2, RoundingMode.HALF_UP));
			mVenta.registrarProformaFactura(beanSegLogin.getLoginDTO(), facturaProforma, idPago, PagoSeleccionado,
					fechPago, idMetodoPago);
			facturaProforma = new Venta();
			listaProductos = mVenta.allProductos();
			listaFacturas = mVenta.allFacturas();
			listaFacturasParaDevolver = mVenta.findFacturaDevolverByEstadoDevolver(false);
			numFactura = mVenta.numeroFactura(Venta.class, "idVenta");
			JSFUtil.crearMensajeINFO("Factura registrada correctamente");
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
		subtotalFactura = 0;
		IVAFactura = 0;
		totalFactura = 0;
		mostrarStock = 0;
	}

	public String actionCargarProforma(Proforma prof) {
		proformVer = prof;
		CalcularProforma(proformVer);
		return "modeloProforma";
	}

	public String actionCargarProformaParaFacturar(Proforma prof) {
		proformFacturar = prof;
		CalcularProforma(proformFacturar);
		return "facturarProforma";
	}

	public void actionListenerAnularProforma(Proforma prof) {
		try {
			mVenta.anularProforma(prof);
			JSFUtil.crearMensajeINFO("Proforma Anulada");
			listaProformas = mVenta.allProformas();
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}

	public void actionCargarFacturarProforma(Proforma prof) {
		try {
			mVenta.anularProforma(prof);
			JSFUtil.crearMensajeINFO("Proforma Anulada");
			listaProformas = mVenta.allProformas();
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}

	public void actionListarProformasByFechas(AjaxBehaviorEvent e) {
		try {
			listaProformasPorFecha = mVenta.findProformaByFechas(fechaInicio, fechaFin);
		} catch (Exception e1) {
			System.out.println("Aun nos has seleccionado la fecha:" + e1.getMessage());
			e1.printStackTrace();
		}
	}

	private void CalcularProforma(Proforma proforma) {
		subtotalFactura = 0;
		IVAFactura = 0;
		totalFactura = 0;
		for (int i = 0; i < proforma.getDetalleProformas().size(); i++) {
			subtotalFactura += proforma.getDetalleProformas().get(i).getPrecio().doubleValue();
		}
		IVAFactura = Math.round((subtotalFactura * 0.12) * 100.0) / 100.0;
		totalFactura = Math.round((subtotalFactura + IVAFactura) * 100.0) / 100.0;
	}

	public void GenerarPDFProforma() {

		ReporteProforma m = new ReporteProforma();
		try {
			m.Generar(proformVer, subtotalFactura, IVAFactura);
			JSFUtil.crearMensajeINFO("Reporte generado");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	// ========================== PAGAR FACTURAS CON CREDITO
	// ==================================
	private double valorPagarFactura;

	public void actionCargarFacturaPagar(Venta fac) {
		facturaPagar = fac;
	}

	public void actionListenerPagarFactura() {

		try {
			mVenta.pagarFactura(facturaPagar, valorPagarFactura);
			JSFUtil.crearMensajeINFO("Pago Registrado");
			listaFacturasPorEstado = mVenta.findFacturaByEstadoVenta();
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}

	// ==================================== VER FACTURAS
	// =========================================================
	private List<Venta> listaFacturas;
	private List<Venta> listaFacturasPorEstado;
	private List<Venta> listaFacturasPorFecha;
	private List<Venta> listaFacturasPorCliente;
	private int buscarCliente; // Para listar facturas por cliente
	private Date fechaInicio; // Buscar por fecha
	private Date fechaFin; // Buscar por fecha

	public String actionCargarFactura(Venta factura) {
		facturaVer = factura;
		CalcularFactura(facturaVer);
		return "modeloFactura";
	}

	public void actionListenerAnularFactura(Venta factura) {
		try {
			mVenta.anularFactura(factura);
			JSFUtil.crearMensajeINFO("Factura Anulada");
			listaFacturas = mVenta.allFacturas();
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}

	public void actionListarAllFacturas() {
		listaFacturas = mVenta.allFacturas();
	}

	public void actionListarFacturasByEstado() {
		listaFacturasPorEstado = mVenta.findFacturaByEstadoVenta();
	}

	public void actionListarFacturasByFechas(AjaxBehaviorEvent e) {
		try {
			listaFacturasPorFecha = mVenta.findFacturaByFechas(fechaInicio, fechaFin);
		} catch (Exception e1) {
			System.out.println("Aun nos has seleccionado la fecha:" + e1.getMessage());
			e1.printStackTrace();
		}
	}

	public void actionListarFacturasByCliente() {
		listaFacturasPorCliente = mVenta.findFacturaByCliente(buscarCliente);
	}

	// =============================== VER FACTURAS PARA DEVOLUCION
	// =========================
	private List<Venta> listaFacturasParaDevolver;
	private Venta facturaDevolucion; // Factura seleccionada para devolucion
	private List<DetalleVenta> detallesDevolucion; // Para adicionar al arreglo que se va a enviar al servidor
	private String descripcionDevolucion;
	private int cantidadDevuelto;
	private boolean devolverTodo;

	public String actionSelectFacturaDevolucion(Venta fac) {
		descripcionDevolucion = "";
		facturaDevolucion = fac;
		return "devolucion";
	}

	public void actionAdicionarDetalleDevolucion(DetalleVenta dta) { // Para adicionar al arreglo que se va a enviar al
																		// servidor
		JSFUtil.crearMensajeINFO("Detalle Seleccionado");
		detallesDevolucion.add(dta);
	}

	public void actionListenerRegistrarDevolucion() {
		try {
			mVenta.registarDevolucion(beanSegLogin.getLoginDTO(), facturaDevolucion, descripcionDevolucion,
					detallesDevolucion, devolverTodo);
			JSFUtil.crearMensajeINFO("Devolución Registrada");
			facturaDevolucion = new Venta();
			descripcionDevolucion = "";
			devolverTodo = true;
			listaFacturasParaDevolver = mVenta.findFacturaDevolverByEstadoDevolver(false);
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}

	/* ======================= GETTERS AND SETTERS=================== */
	public List<Cliente> getListaClientes() {
		return listaClientes;
	}

	public void setListaClientes(List<Cliente> listaClientes) {
		this.listaClientes = listaClientes;
	}

	public List<TipoPago> getListaPagos() {
		return listaPagos;
	}

	public void setListaPagos(List<TipoPago> listaPagos) {
		this.listaPagos = listaPagos;
	}

	public List<Producto> getListaProductos() {
		return listaProductos;
	}

	public void setListaProductos(List<Producto> listaProductos) {
		this.listaProductos = listaProductos;
	}

	public Venta getFactura() {
		return factura;
	}

	public void setFactura(Venta factura) {
		this.factura = factura;
	}

	public List<Venta> getListaFacturas() {
		return listaFacturas;
	}

	public void setListaFacturas(List<Venta> listaFacturas) {
		this.listaFacturas = listaFacturas;
	}

	public int getIdPago() {
		return idPago;
	}

	public void setIdPago(int idPago) {
		this.idPago = idPago;
	}

	public List<DetalleVenta> getListaDetalle() {
		return listaTotal;
	}

	public void setListaDetalle(List<DetalleVenta> listaTotal) {
		this.listaTotal = listaTotal;
	}

	public Date getFechPago() {
		return fechPago;
	}

	public void setFechPago(Date fechPago) {
		this.fechPago = fechPago;
	}

	public Venta getFacturaVer() {
		return facturaVer;
	}

	public void setFacturaVer(Venta facturaVer) {
		this.facturaVer = facturaVer;
	}

	public double getSubtotalFactura() {
		return subtotalFactura;
	}

	public void setSubtotalFactura(double subtotalFactura) {
		this.subtotalFactura = subtotalFactura;
	}

	public double getIVAFactura() {
		return IVAFactura;
	}

	public void setIVAFactura(double iVAFactura) {
		IVAFactura = iVAFactura;
	}

	public List<DetalleVenta> getListaTotal() {
		return listaTotal;
	}

	public void setListaTotal(List<DetalleVenta> listaTotal) {
		this.listaTotal = listaTotal;
	}

	public double getTotalFactura() {
		return totalFactura;
	}

	public void setTotalFactura(double totalFactura) {
		this.totalFactura = totalFactura;
	}

	public boolean getPagoSeleccionado() {
		return PagoSeleccionado;
	}

	public void setPagoSeleccionado(boolean idPagoSeleccionado) {
		this.PagoSeleccionado = idPagoSeleccionado;
	}

	public Cliente getNuevoCliente() {
		return nuevoCliente;
	}

	public void setNuevoCliente(Cliente nuevoCliente) {
		this.nuevoCliente = nuevoCliente;
	}

	public List<Venta> getListaFacturasPorEstado() {
		return listaFacturasPorEstado;
	}

	public void setListaFacturasPorEstado(List<Venta> listaFacturasPorEstado) {
		this.listaFacturasPorEstado = listaFacturasPorEstado;
	}

	public List<Venta> getListaFacturasPorFecha() {
		return listaFacturasPorFecha;
	}

	public void setListaFacturasPorFecha(List<Venta> listaFacturasPorFecha) {
		this.listaFacturasPorFecha = listaFacturasPorFecha;
	}

	public List<Venta> getListaFacturasPorCliente() {
		return listaFacturasPorCliente;
	}

	public void setListaFacturasPorCliente(List<Venta> listaFacturasPorCliente) {
		this.listaFacturasPorCliente = listaFacturasPorCliente;
	}

	public int getBuscarCliente() {
		return buscarCliente;
	}

	public void setBuscarCliente(int buscarCliente) {
		this.buscarCliente = buscarCliente;
	}

	public Cliente getEditarCliente() {
		return editarCliente;
	}

	public void setEditarCliente(Cliente editarCliente) {
		this.editarCliente = editarCliente;
	}

	public Venta getFacturaPagar() {
		return facturaPagar;
	}

	public void setFacturaPagar(Venta facturaPagar) {
		this.facturaPagar = facturaPagar;
	}

	public double getValorPagarFactura() {
		return valorPagarFactura;
	}

	public void setValorPagarFactura(double valorPagarFactura) {
		this.valorPagarFactura = valorPagarFactura;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public Venta getFacturaDevolucion() {
		return facturaDevolucion;
	}

	public void setFacturaDevolucion(Venta facturaDevolucion) {
		this.facturaDevolucion = facturaDevolucion;
	}

	public List<DetalleVenta> getDetallesDevolucion() {
		return detallesDevolucion;
	}

	public void setDetallesDevolucion(List<DetalleVenta> detallesDevolucion) {
		this.detallesDevolucion = detallesDevolucion;
	}

	public String getDescripcionDevolucion() {
		return descripcionDevolucion;
	}

	public void setDescripcionDevolucion(String descripcionDevolucion) {
		this.descripcionDevolucion = descripcionDevolucion;
	}

	public int getCantidadDevuelto() {
		return cantidadDevuelto;
	}

	public void setCantidadDevuelto(int cantidadDevuelto) {
		this.cantidadDevuelto = cantidadDevuelto;
	}

	public boolean isDevolverTodo() {
		return devolverTodo;
	}

	public void setDevolverTodo(boolean devolverTodo) {
		this.devolverTodo = devolverTodo;
	}

	public String getIdentificacionCliente() {
		return identificacionCliente;
	}

	public void setIdentificacionCliente(String identificacionCliente) {
		this.identificacionCliente = identificacionCliente;
	}

	public List<Venta> getListaFacturasParaDevolver() {
		return listaFacturasParaDevolver;
	}

	public void setListaFacturasParaDevolver(List<Venta> listaFacturasParaDevolver) {
		this.listaFacturasParaDevolver = listaFacturasParaDevolver;
	}

	public int getMostrarStock() {
		return mostrarStock;
	}

	public void setMostrarStock(int mostrarStock) {
		this.mostrarStock = mostrarStock;
	}

	public Cliente getInformacionCliente() {
		return informacionCliente;
	}

	public void setInformacionCliente(Cliente informacionCliente) {
		this.informacionCliente = informacionCliente;
	}

	public String getDescripcionProducto() {
		return descripcionProducto;
	}

	public void setDescripcionProducto(String descripcionProducto) {
		this.descripcionProducto = descripcionProducto;
	}

	public Date getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public Date getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}

	public Producto getProductoSeleccionado() {
		return productoSeleccionado;
	}

	public void setProductoSeleccionado(Producto productoSeleccionado) {
		this.productoSeleccionado = productoSeleccionado;
	}

	public Proforma getProforma() {
		return proforma;
	}

	public void setProforma(Proforma proforma) {
		this.proforma = proforma;
	}

	public Proforma getProformVer() {
		return proformVer;
	}

	public void setProformVer(Proforma proformVer) {
		this.proformVer = proformVer;
	}

	public int getCantidadDetProforma() {
		return cantidadDetProforma;
	}

	public void setCantidadDetProforma(int cantidadDetProforma) {
		this.cantidadDetProforma = cantidadDetProforma;
	}

	public List<Proforma> getListaProformas() {
		return listaProformas;
	}

	public void setListaProformas(List<Proforma> listaProformas) {
		this.listaProformas = listaProformas;
	}

	public List<Proforma> getListaProformasPorFecha() {
		return listaProformasPorFecha;
	}

	public void setListaProformasPorFecha(List<Proforma> listaProformasPorFecha) {
		this.listaProformasPorFecha = listaProformasPorFecha;
	}

	public Proforma getProformFacturar() {
		return proformFacturar;
	}

	public void setProformFacturar(Proforma proformFacturar) {
		this.proformFacturar = proformFacturar;
	}

	public List<MetodoPago> getListaMetodoPagos() {
		return listaMetodoPagos;
	}

	public void setListaMetodoPagos(List<MetodoPago> listaMetodoPagos) {
		this.listaMetodoPagos = listaMetodoPagos;
	}

	public int getIdMetodoPago() {
		return idMetodoPago;
	}

	public void setIdMetodoPago(int idMetodoPago) {
		this.idMetodoPago = idMetodoPago;
	}

	public String getDescuentoFactura() {
		return descuentoFactura;
	}

	public void setDescuentoFactura(String descuentoFactura) {
		this.descuentoFactura = descuentoFactura;
	}

	public Date getFechaVencimientoProforma() {
		return fechaVencimientoProforma;
	}

	public void setFechaVencimientoProforma(Date fechaVencimientoProforma) {
		this.fechaVencimientoProforma = fechaVencimientoProforma;
	}

	public String getNumFactura() {
		return numFactura;
	}

	public void setNumFactura(String numFactura) {
		this.numFactura = numFactura;
	}

	public String getNumProforma() {
		return numProforma;
	}

	public void setNumProforma(String numProforma) {
		this.numProforma = numProforma;
	}

	public String actionReporteCliente() {
		Map<String, Object> parametros = new HashMap<String, Object>();
		/*
		 * parametros.put("p_titulo_principal",p_titulo_principal);
		 * parametros.put("p_titulo",p_titulo);
		 */
		FacesContext context = FacesContext.getCurrentInstance();
		ServletContext servletContext = (ServletContext) context.getExternalContext().getContext();
		String ruta = servletContext.getRealPath("ventas/vendedor/ClienteRR.jasper");
		System.out.println(ruta);
		HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
		response.addHeader("Content-disposition", "attachment;filename=reporte_cliente.pdf");
		response.setContentType("application/pdf");
		try {
			Class.forName("org.postgresql.Driver");
			Connection connection = null;
			connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/ferreteriakyms", "postgres",
					"2021");
			JasperPrint impresion = JasperFillManager.fillReport(ruta, parametros, connection);
			JasperExportManager.exportReportToPdfStream(impresion, response.getOutputStream());
			context.getApplication().getStateManager().saveView(context);
			System.out.println("Reporte proveedor generado.");
			context.responseComplete();
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
		return "";
	}

	public String actionReporteCliente2() {
		Map<String, Object> parametros = new HashMap<String, Object>();
		/*
		 * parametros.put("p_titulo_principal",p_titulo_principal);
		 * parametros.put("p_titulo",p_titulo);
		 */
		FacesContext context = FacesContext.getCurrentInstance();
		ServletContext servletContext = (ServletContext) context.getExternalContext().getContext();
		String ruta = servletContext.getRealPath("inventario/supervisor/ClienteRR.jasper");
		System.out.println(ruta);
		HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
		response.addHeader("Content-disposition", "attachment;filename=reporte_cliente.pdf");
		response.setContentType("application/pdf");
		try {
			Class.forName("org.postgresql.Driver");
			Connection connection = null;
			connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/ferreteriakyms", "postgres",
					"2021");
			JasperPrint impresion = JasperFillManager.fillReport(ruta, parametros, connection);
			JasperExportManager.exportReportToPdfStream(impresion, response.getOutputStream());
			context.getApplication().getStateManager().saveView(context);
			System.out.println("Reporte CLIENTE generado.");
			context.responseComplete();
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
		return "";
	}

	public String actionReporteVentas() {
		Map<String, Object> parametros = new HashMap<String, Object>();

		FacesContext context = FacesContext.getCurrentInstance();
		ServletContext servletContext = (ServletContext) context.getExternalContext().getContext();
		String ruta = servletContext.getRealPath("inventario/supervisor/venta.jasper");
		System.out.println(ruta);
		HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
		response.addHeader("Content-disposition", "attachment;filename=ventas.pdf");
		response.setContentType("application/pdf");
		try {
			Class.forName("org.postgresql.Driver");
			Connection connection = null;
			connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/ferreteriakyms", "postgres",
					"2021");
			JasperPrint impresion = JasperFillManager.fillReport(ruta, parametros, connection);
			JasperExportManager.exportReportToPdfStream(impresion, response.getOutputStream());
			context.getApplication().getStateManager().saveView(context);
			System.out.println("reporte generado.");
			context.responseComplete();
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
		System.out.print("Hola");
		return "";
	}

	public String actionReporteProformaV() {
		Map<String, Object> parametros = new HashMap<String, Object>();

		FacesContext context = FacesContext.getCurrentInstance();
		ServletContext servletContext = (ServletContext) context.getExternalContext().getContext();
		String ruta = servletContext.getRealPath("inventario/supervisor/proformaV.jasper");
		System.out.println(ruta);
		HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
		response.addHeader("Content-disposition", "attachment;filename=proformas.pdf");
		response.setContentType("application/pdf");
		try {
			Class.forName("org.postgresql.Driver");
			Connection connection = null;
			connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/ferreteriakyms", "postgres",
					"2021");
			JasperPrint impresion = JasperFillManager.fillReport(ruta, parametros, connection);
			JasperExportManager.exportReportToPdfStream(impresion, response.getOutputStream());
			context.getApplication().getStateManager().saveView(context);
			System.out.println("reporte generado.");
			context.responseComplete();
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
		System.out.print("Hola");
		return "";
	}

}
