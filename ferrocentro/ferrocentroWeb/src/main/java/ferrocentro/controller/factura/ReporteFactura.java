package ferreteriakyms.controller.factura;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import ferreteriakyms.model.core.entities.Venta;

public class ReporteFactura {
	
	public void Generar(Venta factura,double subtotal, double iva) throws FileNotFoundException, DocumentException {
		String ruta=System.getProperty("user.home");
		Document documento = new Document();
		try {
		PdfWriter.getInstance(documento, new FileOutputStream(ruta+"/Desktop/rep-fac-"+factura.getNumFactura()+".pdf"));
			documento.open();
			
			//Imagen Header
		    Image header = Image.getInstance(getClass().getResource("header2.jpg"));
	        header.scaleToFit(50, 100);
	        header.scaleAbsolute(1000,150); //Tamnio
	        header.setAbsolutePosition(0, 700);
	        documento.add(header);
	        
	        Paragraph parrafoSaltoDeLinea = new Paragraph();
            parrafoSaltoDeLinea.setAlignment(Paragraph.ALIGN_LEFT);
            parrafoSaltoDeLinea.add("\n\n");
            documento.add(parrafoSaltoDeLinea);
	        
			//Header Factura
            Paragraph parrafoT = new Paragraph();
            parrafoT.setAlignment(Paragraph.ALIGN_LEFT);
            parrafoT.setFont(FontFactory.getFont("Tahoma", 16, Font.BOLD, BaseColor.DARK_GRAY));
            parrafoT.add("                                                        FACTURA                        KYM'S \n\n\n\n");
            documento.add(parrafoT);
            
		    
			Paragraph parrafo1 = new Paragraph();
			parrafo1.setAlignment(Paragraph.ALIGN_LEFT);
			parrafo1.add("     Ferreteria Kym's \n");
			parrafo1.setFont(FontFactory.getFont("Tahoma",20,Font.BOLD,BaseColor.DARK_GRAY));
			documento.add(parrafo1);
			
			Paragraph parrafo2 = new Paragraph();
			parrafo2.setAlignment(Paragraph.ALIGN_LEFT);
			parrafo2.add("     Cotacachi 23-14                                                                          Fecha: "+factura.getFecha()+ " \n");
			parrafo2.setFont(FontFactory.getFont("Tahoma",20,Font.BOLD,BaseColor.DARK_GRAY));
			documento.add(parrafo2);
			
			Paragraph parrafo3 = new Paragraph();
			parrafo3.setAlignment(Paragraph.ALIGN_LEFT);
			parrafo3.add("     ferrykym@gmail.com                                                                  Nro factura: "+factura.getNumFactura()+ "\n");
			parrafo3.setFont(FontFactory.getFont("Tahoma",20,Font.BOLD,BaseColor.DARK_GRAY));
			documento.add(parrafo3);
			
			Paragraph parrafo4 = new Paragraph();
			parrafo4.setAlignment(Paragraph.ALIGN_LEFT);
			parrafo4.add("     Telf: 0985802375 \n");
			parrafo4.setFont(FontFactory.getFont("Tahoma",20,Font.BOLD,BaseColor.DARK_GRAY));
			documento.add(parrafo4);
			
			documento.add(Chunk.NEWLINE);
			
			//CLIENTE
			
			Paragraph parrafoCliente = new Paragraph();
			parrafoCliente.setAlignment(Paragraph.ALIGN_LEFT);
			parrafoCliente.add("     Cliente \n");
			parrafoCliente.setFont(FontFactory.getFont("Tahoma",20,Font.BOLD,BaseColor.DARK_GRAY));
			documento.add(parrafoCliente);
			
			Paragraph parrafoClienteLinea = new Paragraph();
			parrafoClienteLinea.setAlignment(Paragraph.ALIGN_LEFT);
			parrafoClienteLinea.add("     ___________________________ \n");
			parrafoClienteLinea.setFont(FontFactory.getFont("Tahoma",10,Font.BOLD,BaseColor.DARK_GRAY));
			documento.add(parrafoClienteLinea);
			
			Paragraph parrafoCliente1 = new Paragraph();
			parrafoCliente1.setAlignment(Paragraph.ALIGN_LEFT);
			parrafoCliente1.add("     Nombre: "+factura.getCliente().getNombre()+"  "+factura.getCliente().getApellido()+"\n");
			parrafoCliente1.setFont(FontFactory.getFont("Tahoma",20,Font.BOLD,BaseColor.DARK_GRAY));
			documento.add(parrafoCliente1);
			
			Paragraph parrafoCliente2 = new Paragraph();
			parrafoCliente2.setAlignment(Paragraph.ALIGN_LEFT);
			parrafoCliente2.add("     C�dula: "+factura.getCliente().getIdentificacion()+"\n");
			parrafoCliente2.setFont(FontFactory.getFont("Tahoma",20,Font.BOLD,BaseColor.DARK_GRAY));
			documento.add(parrafoCliente2);
			
			Paragraph parrafoCliente3 = new Paragraph();
			parrafoCliente3.setAlignment(Paragraph.ALIGN_LEFT);
			parrafoCliente3.add("     Direcci�n: "+factura.getCliente().getDireccion()+"\n");
			parrafoCliente3.setFont(FontFactory.getFont("Tahoma",20,Font.BOLD,BaseColor.DARK_GRAY));
			documento.add(parrafoCliente3);
			
			Paragraph parrafoCliente4 = new Paragraph();
			parrafoCliente4.setAlignment(Paragraph.ALIGN_LEFT);
			parrafoCliente4.add("     Email: "+factura.getCliente().getCorreo()+"\n\n");
			parrafoCliente4.setFont(FontFactory.getFont("Tahoma",20,Font.BOLD,BaseColor.DARK_GRAY));
			documento.add(parrafoCliente4);
			
			
			//TABLA DE DESCRIPCION
			Paragraph parrafo5 = new Paragraph();
			parrafo5.setAlignment(Paragraph.ALIGN_CENTER);
			parrafo5.add("Detalle de la Factura \n\n");
			parrafo5.setFont(FontFactory.getFont("Tahoma",18,Font.BOLD,BaseColor.DARK_GRAY));
			documento.add(parrafo5);
		
			PdfPTable tabla=new PdfPTable(4);
			tabla.setWidthPercentage(100);
			float[] columnas = new float[]{100f, 50f, 55f, 75f};
			
			Paragraph d1 = new Paragraph();
	        d1.setFont(FontFactory.getFont("Tahoma", 12, Font.BOLD, BaseColor.WHITE));
	        d1.add("DESCRIPCI�N");
		    PdfPCell titulo1 = new PdfPCell(d1);
            titulo1.setBackgroundColor(BaseColor.BLUE);
            titulo1.setFixedHeight(30f);
            titulo1.setVerticalAlignment(Element.ALIGN_MIDDLE);
            titulo1.setHorizontalAlignment(Element.ALIGN_CENTER);
            
        	Paragraph d2 = new Paragraph();
	        d2.setFont(FontFactory.getFont("Tahoma", 12, Font.BOLD, BaseColor.WHITE));
	        d2.add("CANTIDAD");
		    PdfPCell titulo2 = new PdfPCell(d2);
            titulo2.setBackgroundColor(BaseColor.BLUE);
            titulo2.setVerticalAlignment(Element.ALIGN_MIDDLE);
            titulo2.setHorizontalAlignment(Element.ALIGN_CENTER);
            
        	Paragraph d3 = new Paragraph();
	        d3.setFont(FontFactory.getFont("Tahoma", 12, Font.BOLD, BaseColor.WHITE));
	        d3.add("PRECIO UNIT");
		    PdfPCell titulo3 = new PdfPCell(d3);
            titulo3.setBackgroundColor(BaseColor.BLUE);
            titulo3.setVerticalAlignment(Element.ALIGN_MIDDLE);
            titulo3.setHorizontalAlignment(Element.ALIGN_CENTER);
            
        	Paragraph d4 = new Paragraph();
	        d4.setFont(FontFactory.getFont("Tahoma", 12, Font.BOLD, BaseColor.WHITE));
	        d4.add("TOTAL");
		    PdfPCell titulo4 = new PdfPCell(d4);
            titulo4.setBackgroundColor(BaseColor.BLUE);
            titulo4.setVerticalAlignment(Element.ALIGN_MIDDLE);
            titulo4.setHorizontalAlignment(Element.ALIGN_CENTER);
            
            tabla.addCell(titulo1);
            tabla.addCell(titulo2);
            tabla.addCell(titulo3);
            tabla.addCell(titulo4);
            
	        tabla.setWidths(columnas);
			
			for(int i=0;i<factura.getDetalleVentas().size();i++) {
				Paragraph dD1 = new Paragraph();
		        dD1.add(factura.getDetalleVentas().get(i).getProducto().getDescripcion());
			    PdfPCell tituloD1 = new PdfPCell(dD1);
	            tituloD1.setFixedHeight(30f);
	            tituloD1.setVerticalAlignment(Element.ALIGN_MIDDLE);
	            tituloD1.setHorizontalAlignment(Element.ALIGN_CENTER);
	            
	        	Paragraph dD2 = new Paragraph();
		        dD2.add(factura.getDetalleVentas().get(i).getCantidad()+" "+factura.getDetalleVentas().get(i).getProducto().getUnidadMedida().getDescripcion());
			    PdfPCell tituloD2 = new PdfPCell(dD2);
	            tituloD2.setVerticalAlignment(Element.ALIGN_MIDDLE);
	            tituloD2.setHorizontalAlignment(Element.ALIGN_CENTER);
	            
	        	Paragraph dD3 = new Paragraph();
		        dD3.add(factura.getDetalleVentas().get(i).getProducto().getPrecioVenta()+"");
			    PdfPCell tituloD3 = new PdfPCell(dD3);
	            tituloD3.setVerticalAlignment(Element.ALIGN_MIDDLE);
	            tituloD3.setHorizontalAlignment(Element.ALIGN_CENTER);
	            
	        	Paragraph dD4 = new Paragraph();
		        dD4.add(factura.getDetalleVentas().get(i).getPrecio()+"");
			    PdfPCell tituloD4 = new PdfPCell(dD4);
	            tituloD4.setVerticalAlignment(Element.ALIGN_MIDDLE);
	            tituloD4.setHorizontalAlignment(Element.ALIGN_CENTER);
	            
	            tabla.addCell(tituloD1);
	            tabla.addCell(tituloD2);
	            tabla.addCell(tituloD3);
	            tabla.addCell(tituloD4);
			}
			
			documento.add(tabla);
			documento.add(Chunk.NEWLINE);
			
			Paragraph parrafo6 = new Paragraph();
			parrafo6.setAlignment(Paragraph.ALIGN_CENTER);
			parrafo6.add("                                                                                              Descuento : "+factura.getImpuesto()+"\n");
			parrafo6.setFont(FontFactory.getFont("Tahoma",20,Font.BOLD,BaseColor.DARK_GRAY));
			documento.add(parrafo6);
			
			Paragraph parrafo7 = new Paragraph();
			parrafo7.setAlignment(Paragraph.ALIGN_CENTER);
			parrafo7.add("                                                                                           Subtotal : "+subtotal+"\n");
			parrafo7.setFont(FontFactory.getFont("Tahoma",20,Font.BOLD,BaseColor.DARK_GRAY));
			documento.add(parrafo7);
			
			Paragraph parrafo8 = new Paragraph();
			parrafo8.setAlignment(Paragraph.ALIGN_CENTER);
			parrafo8.add("                                                                                   IVA : "+iva+"\n");
			parrafo8.setFont(FontFactory.getFont("Tahoma",20,Font.BOLD,BaseColor.DARK_GRAY));
			documento.add(parrafo8);
			
			Paragraph parrafo9 = new Paragraph();
			parrafo9.setAlignment(Paragraph.ALIGN_CENTER);
			parrafo9.add("                                                                                           TOTAL : "+factura.getTotal()+"\n\n\n");
		    parrafo9.setFont(FontFactory.getFont("Tahoma", 20, Font.BOLD, BaseColor.RED));
			documento.add(parrafo9);
			
			
			//Descripcion de factura
			Paragraph parrafoDes = new Paragraph();
			parrafoDes.setAlignment(Paragraph.ALIGN_LEFT);
			parrafoDes.add("       Obserservaciones/Instrucciones de pago\n");
			parrafoDes.setFont(FontFactory.getFont("Tahoma",15,Font.BOLD,BaseColor.DARK_GRAY));
			documento.add(parrafoDes);
		
		
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("error de pdf: "+e);
			e.printStackTrace();
		}
		documento.close();
	}
}
