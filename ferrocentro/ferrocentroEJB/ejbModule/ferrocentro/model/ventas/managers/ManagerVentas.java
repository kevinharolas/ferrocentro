package ferreteriakyms.model.ventas.managers;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import ferreteriakyms.model.auditoria.managers.ManagerAuditoria;
import ferreteriakyms.model.core.entities.Cliente;
import ferreteriakyms.model.core.entities.DetalleDevolucionVenta;
import ferreteriakyms.model.core.entities.DetalleProforma;
import ferreteriakyms.model.core.entities.DetalleVenta;
import ferreteriakyms.model.core.entities.DevolucionVenta;
import ferreteriakyms.model.core.entities.Entrada;
import ferreteriakyms.model.core.entities.Marca;
import ferreteriakyms.model.core.entities.MetodoPago;
import ferreteriakyms.model.core.entities.Producto;
import ferreteriakyms.model.core.entities.Proforma;
import ferreteriakyms.model.core.entities.SegUsuario;
import ferreteriakyms.model.core.entities.ThmEmpleado;
import ferreteriakyms.model.core.entities.TipoPago;
import ferreteriakyms.model.core.entities.UnidadMedida;
import ferreteriakyms.model.core.entities.Venta;
import ferreteriakyms.model.core.managers.ManagerDAO;
import ferreteriakyms.model.seguridades.dtos.LoginDTO;

/**
 * Session Bean implementation class ManagerVentas
 */
@Stateless
@LocalBean
public class ManagerVentas {

	@EJB
	private ManagerDAO mDAO;
	@EJB
	private ManagerAuditoria mAuditoria;

	public ManagerVentas() {
		// TODO Auto-generated constructor stub
	}

	public List<Cliente> allClientes() {
		String sentenciaJPQL = "o.estado=true";
		return mDAO.findWhere(Cliente.class, sentenciaJPQL);
	}

	public List<TipoPago> allPagos() {
		String sentenciaJPQL = "o.estado=true";
		return mDAO.findWhere(TipoPago.class, sentenciaJPQL);
	}

	public List<MetodoPago> allMetodoPagos() {
		String sentenciaJPQL = "o.estado=true";
		return mDAO.findWhere(MetodoPago.class, sentenciaJPQL);
	}

	public List<Producto> allProductos() {
		String sentenciaJPQL = "o.estado=true";
		return mDAO.findWhere(Producto.class, sentenciaJPQL);
	}

	public List<Venta> allFacturas() {
		String sentenciaJPQL = "o.estado=true";
		return mDAO.findWhere(Venta.class, sentenciaJPQL);
	}

	public List<Proforma> allProformas() {
		String sentenciaJPQL = "o.estado=true";
		return mDAO.findWhere(Proforma.class, sentenciaJPQL);
	}

	public List<Venta> findFacturaByEstadoVenta() {
		String sentenciaJPQL = "o.estado=true AND o.estadoVenta='P'";
		return mDAO.findWhere(Venta.class, sentenciaJPQL);
	}

	public List<Venta> findFacturaByFechas(Date fechaInicio, Date fechaFin) throws Exception {
		String sql = "SELECT v from Venta v WHERE v.fecha BETWEEN :fechaInicio AND :fechaFin AND v.estado=true ORDER BY v.fecha";
		return mDAO.findFacturaByRangoFecha(Venta.class, sql, fechaInicio, fechaFin);
	}

	public List<Proforma> findProformaByFechas(Date fechaInicio, Date fechaFin) throws Exception {
		String sql = "SELECT v from Proforma v WHERE v.fecha BETWEEN :fechaInicio AND :fechaFin AND v.estado=true ORDER BY v.fecha";
		return mDAO.findFacturaByRangoFecha(Proforma.class, sql, fechaInicio, fechaFin);
	}

	public List<Venta> findFacturaByCliente(int idCliente) {
		return mDAO.findFacturaByCliente(Venta.class, idCliente);
	}

	public List<Venta> findFacturaDevolverByEstadoDevolver(boolean devuelto) {
		String sentenciaJPQL = "o.estado=true AND o.ventaDevuelto=" + false;
		return mDAO.findWhere(Venta.class, sentenciaJPQL);

	}

	public Cliente findOneClient(String identificacionCliente) throws Exception {
		return (Cliente) mDAO.findWhithQueryName(Cliente.class, "Cliente.findByIdentificacion", "identificacion",
				identificacionCliente);

	}

	public Producto findOneProducto(String descripcionProducto) throws Exception {
		return (Producto) mDAO.findWhithQueryName(Producto.class, "Producto.findByDescripcion", "descripcion",
				descripcionProducto);

	}

	// ============================== Cliente ==============================
	public void insertarCliente(LoginDTO loginDTO, Cliente nuevoCliente) throws Exception {
		if (nuevoCliente == null)
			throw new Exception("Debe ingresar los datos del cliente");

		SegUsuario usuario = (SegUsuario) mDAO.findById(SegUsuario.class, loginDTO.getIdSegUsuario());
		nuevoCliente.setSegUsuario(usuario);
		nuevoCliente.setEstado(true);
		mDAO.insertar(nuevoCliente);
		mAuditoria.mostrarLog(loginDTO, getClass(), "Cliente insertado",
				"Cliente: " + nuevoCliente.getCorreo() + " insertado");
	}

	public void eliminarCliente(Cliente cli) throws Exception {
		Cliente cliente = (Cliente) mDAO.findById(Cliente.class, cli.getIdCliente());
		cliente.setEstado(false);
		mDAO.actualizar(cliente);
		mAuditoria.mostrarLog(getClass(), "Cliente eliminado", "Cliente: " + cliente.getCorreo() + " eliminado");
	}

	public void actualizarCliente(Cliente cli) throws Exception {
		Cliente cliente = (Cliente) mDAO.findById(Cliente.class, cli.getIdCliente());
		cliente.setNombre(cli.getNombre());
		cliente.setApellido(cli.getApellido());
		cliente.setTelefono(cli.getTelefono());
		cliente.setCiudad(cli.getCiudad());
		cliente.setDireccion(cli.getDireccion());
		cliente.setCorreo(cli.getCorreo());
		mDAO.actualizar(cliente);
		mAuditoria.mostrarLog(getClass(), "Cliente actualizado", "Cliente: " + cliente.getCorreo() + " actualizado");
	}

	// ============================== Facturacion ==============================
	public Venta adicionarProducto(Venta factura, int cantidad, Producto prodSeleccionado) throws Exception {
		// comprobar si es la primera vez
		if (factura == null) {
			factura = new Venta();
			factura.setDetalleVentas(new ArrayList<DetalleVenta>());
			factura.setFecha(new Date());
			factura.setTotal(new BigDecimal(0));
			factura.setEstado(true);
		}

		// adicionamos el detalle:
		Producto prod = (Producto) mDAO.findById(Producto.class, prodSeleccionado.getIdProducto());
		// calculos
		double subtotal = prod.getPrecioVenta().doubleValue() * cantidad;
		double IVA = subtotal * prod.getImpuesto().doubleValue();
		double total = subtotal + IVA;

		DetalleVenta detalle = new DetalleVenta();
		detalle.setPrecio(new BigDecimal(subtotal).setScale(2, RoundingMode.HALF_UP));
		detalle.setVenta(factura);
		detalle.setCantidad(cantidad);
		detalle.setProducto(prod);
		factura.setTotal(factura.getTotal().add(new BigDecimal(total).setScale(2, RoundingMode.HALF_UP)));
		factura.getDetalleVentas().add(detalle);
		return factura;
	}

	public void registrarFactura(LoginDTO loginDTO, Venta factura, String identificacionCliente, int idPago,
			boolean tipoPago, Date fechaPago, int idMetodoPago, double descuentoFactura) throws Exception {
		if (factura == null || factura.getDetalleVentas() == null || factura.getDetalleVentas().size() == 0)
			throw new Exception("Debe seleccionar al menos un producto");

		Cliente cliente = (Cliente) mDAO.findWhithQueryName(Cliente.class, "Cliente.findByIdentificacion",
				"identificacion", identificacionCliente);
		TipoPago pago = (TipoPago) mDAO.findById(TipoPago.class, idPago);
		MetodoPago Mpago = (MetodoPago) mDAO.findById(MetodoPago.class, idMetodoPago);
		SegUsuario usuario = (SegUsuario) mDAO.findById(SegUsuario.class, loginDTO.getIdSegUsuario());
		factura.setCliente(cliente);
		factura.setTipoPago(pago);
		factura.setSegUsuario(usuario);
		factura.setMetodoPago(Mpago);
		factura.setVentaDevuelto(false);
		factura.setNumFactura(numeroFactura(Venta.class, "idVenta"));

		double descuento = descuentoFactura / 100;
		factura.setImpuesto(new BigDecimal(descuento).setScale(2, RoundingMode.HALF_UP));
		double totalFactura = factura.getTotal().doubleValue();
		factura.setTotal(new BigDecimal(totalFactura * (1 - descuento)).setScale(2, RoundingMode.HALF_UP));

		if (!tipoPago && fechaPago != null) {
			factura.setFechaPago(fechaPago);
			factura.setEstadoVenta("P"); // Pendiente
			factura.setDeudaParcial(factura.getTotal());
		} else {
			factura.setFechaPago(new Date());
			factura.setEstadoVenta("C"); // Cancelado
			factura.setDeudaParcial(new BigDecimal(0).setScale(2, RoundingMode.HALF_UP));
		}

		for (int i = 0; i < factura.getDetalleVentas().size(); i++) { // Reduce el stock de productos
			int idProducto = factura.getDetalleVentas().get(i).getProducto().getIdProducto();
			Producto prod = (Producto) mDAO.findById(Producto.class, idProducto);
			int cant = factura.getDetalleVentas().get(i).getCantidad();
			int stock = prod.getStock();
			int resta = stock - cant;
			prod.setStock(resta);
			mDAO.actualizar(prod);
		}

		mDAO.insertar(factura);
		mAuditoria.mostrarLog(loginDTO, getClass(), "Factura registrada",
				"Factura: " + factura.getFecha() + " registrada");
	}

	public void anularFactura(Venta fac) throws Exception {
		Venta factura = (Venta) mDAO.findById(Venta.class, fac.getIdVenta());
		factura.setEstado(false);
		mDAO.actualizar(factura);
		mAuditoria.mostrarLog(getClass(), "Factura anulada", "Factura:" + factura.getIdVenta() + "anulada");
	}

	public void pagarFactura(Venta fac, double valorPagarFactura) throws Exception {
		Venta factura = (Venta) mDAO.findById(Venta.class, fac.getIdVenta());
		double nuevoValor = factura.getDeudaParcial().doubleValue() - valorPagarFactura;
		factura.setDeudaParcial(new BigDecimal(nuevoValor).setScale(2, RoundingMode.HALF_UP));

		if (factura.getDeudaParcial().doubleValue() <= 0) {
			factura.setEstadoVenta("C");
		}
		mDAO.actualizar(factura);
	}

	// =======================DEVOLUCION
	// ======================================================================
	public void registarDevolucion(LoginDTO loginDTO, Venta fac, String descripcion, List<DetalleVenta> detVenta,
			boolean devolverTodo) throws Exception {
		Venta factura = (Venta) mDAO.findById(Venta.class, fac.getIdVenta());
		SegUsuario usuario = (SegUsuario) mDAO.findById(SegUsuario.class, loginDTO.getIdSegUsuario());

		DevolucionVenta devolucion = new DevolucionVenta();
		devolucion.setVenta(factura);
		devolucion.setSegUsuario(usuario);
		devolucion.setFechaDevolucion(new Date());
		devolucion.setDescripcion(descripcion);
		devolucion.setEstado(true);
		devolucion.setSaldoDevolver(new BigDecimal(0).setScale(2, RoundingMode.HALF_UP));
		factura.setVentaDevuelto(true);

		if (devolverTodo) {
			devolucion.setTipoDevolucion("Devolución Total");
		} else {
			devolucion.setTipoDevolucion("Devolución Parcial");
		}
		mDAO.insertar(devolucion);

		int count = (int) mDAO.count(DevolucionVenta.class, "idDevolucionVenta");
		DevolucionVenta sameDevolucion = (DevolucionVenta) mDAO.findById(DevolucionVenta.class,
				loginDTO.getIdSegUsuario());

		if (devolverTodo) {
			factura.setEstado(false);
			sameDevolucion.setSaldoDevolver(
					new BigDecimal(factura.getTotal().doubleValue()).setScale(2, RoundingMode.HALF_UP));

			for (int i = 0; i < fac.getDetalleVentas().size(); i++) {
				// Aumentar Stock;
				Producto prod = (Producto) mDAO.findById(Producto.class,
						fac.getDetalleVentas().get(i).getProducto().getIdProducto());
				double cant = fac.getDetalleVentas().get(i).getCantidad();
				int stock = prod.getStock();
				prod.setStock((int) (stock + cant));
				mDAO.actualizar(prod);
			}
			mDAO.actualizar(sameDevolucion);

		} else {
			double nuevoTotal = 0;
			for (int i = 0; i < detVenta.size(); i++) {
				DetalleDevolucionVenta detalleDevolucion = new DetalleDevolucionVenta();
				DetalleVenta detalleVenta = (DetalleVenta) mDAO.findById(DetalleVenta.class,
						detVenta.get(i).getIdDetalleVenta());
				detalleDevolucion.setDevolucionVenta(sameDevolucion);
				detalleDevolucion.setProducto(detVenta.get(i).getProducto());
				detalleDevolucion.setCantidadDevuelto(detVenta.get(i).getCantidad());

				// Calculo para devolver saldo
				double cantidadRestante = detalleVenta.getCantidad().doubleValue()
						- detVenta.get(i).getCantidad().doubleValue();
				double subtotal = detVenta.get(i).getProducto().getPrecio().doubleValue() * cantidadRestante;
				double IVA = subtotal * detVenta.get(i).getProducto().getImpuesto().doubleValue();
				;
				nuevoTotal += subtotal + IVA;
				mDAO.insertar(detalleDevolucion);

				// Aumentar Stock;
				Producto prod = (Producto) mDAO.findById(Producto.class, detVenta.get(i).getProducto().getIdProducto());
				int cant = detVenta.get(i).getCantidad();
				int stock = prod.getStock();
				prod.setStock((stock + cant));
				mDAO.actualizar(prod);
			}

			// Continuacion de entidad devolucion
			double totalFactura = fac.getTotal().doubleValue();
			sameDevolucion
					.setSaldoDevolver(new BigDecimal((totalFactura - nuevoTotal)).setScale(2, RoundingMode.HALF_UP));
			mDAO.actualizar(sameDevolucion);
		}

		mDAO.actualizar(factura);
		mAuditoria.mostrarLog(loginDTO, getClass(), "Factura Devuelto",
				"Devolución: " + devolucion.getDescripcion() + " registrada");
	}

	// ============================== PROFORMA==============================
	public Proforma adicionarProductoProforma(Proforma proforma, int cantidad, Producto prodSeleccionado)
			throws Exception {
		// comprobar si es la primera vez
		if (proforma == null) {
			proforma = new Proforma();
			proforma.setDetalleProformas(new ArrayList<DetalleProforma>());
			proforma.setFecha(new Date());
			proforma.setTotal(new BigDecimal(0));
			proforma.setEstado(true);
		}

		// adicionamos el detalle:
		Producto prod = (Producto) mDAO.findById(Producto.class, prodSeleccionado.getIdProducto());
		;
		// calculos
		double subtotal = prod.getPrecioVenta().doubleValue() * cantidad;
		double IVA = subtotal * prod.getImpuesto().doubleValue();
		double total = subtotal + IVA;

		DetalleProforma detalle = new DetalleProforma();
		detalle.setCantidad(cantidad);
		detalle.setPrecio(new BigDecimal(subtotal).setScale(2, RoundingMode.HALF_UP));
		detalle.setProforma(proforma);
		detalle.setCantidad(detalle.getCantidad());
		detalle.setProducto(prod);
		proforma.setTotal(proforma.getTotal().add(new BigDecimal(total).setScale(2, RoundingMode.HALF_UP)));
		proforma.getDetalleProformas().add(detalle);
		return proforma;
	}

	public void registrarProforma(LoginDTO loginDTO, Proforma proforma, String identificacionCliente,
			Date fechaVencimientoProforma, double descuentoFactura) throws Exception {
		if (proforma == null || proforma.getDetalleProformas() == null || proforma.getDetalleProformas().size() == 0)
			throw new Exception("Debe seleccionar al menos un producto");
		SegUsuario usuario = (SegUsuario) mDAO.findById(SegUsuario.class, loginDTO.getIdSegUsuario());
		Cliente cliente = (Cliente) mDAO.findWhithQueryName(Cliente.class, "Cliente.findByIdentificacion",
				"identificacion", identificacionCliente);
		proforma.setSegUsuario(usuario);
		proforma.setCliente(cliente);
		proforma.setFecha(new Date());
		proforma.setFechaVencimiento(fechaVencimientoProforma);
		proforma.setNumProforma(numeroFactura(Proforma.class, "idProforma"));

		double descuento = descuentoFactura / 100;
		proforma.setImpuesto(new BigDecimal(descuento).setScale(2, RoundingMode.HALF_UP));
		double totalProforma = proforma.getTotal().doubleValue();
		proforma.setTotal(new BigDecimal(totalProforma * (1 - descuento)).setScale(2, RoundingMode.HALF_UP));

		mDAO.insertar(proforma);
		mAuditoria.mostrarLog(loginDTO, getClass(), "Proforma registrada",
				"Proforma: " + proforma.getFecha() + " registrada");
	}

	public void anularProforma(Proforma proform) throws Exception {
		Proforma proforma = (Proforma) mDAO.findById(Proforma.class, proform.getIdProforma());
		proforma.setEstado(false);
		mDAO.actualizar(proforma);
		mAuditoria.mostrarLog(getClass(), "Proforma anulada", "Proforma:" + proforma.getIdProforma() + "anulada");
	}

	public void registrarProformaFactura(LoginDTO loginDTO, Venta factura, int idPago, boolean tipoPago, Date fechaPago,
			int idMetodoPago) throws Exception {
		if (factura == null || factura.getDetalleVentas() == null || factura.getDetalleVentas().size() == 0)
			throw new Exception("Debe seleccionar al menos un producto");

		TipoPago pago = (TipoPago) mDAO.findById(TipoPago.class, idPago);
		MetodoPago Mpago = (MetodoPago) mDAO.findById(MetodoPago.class, idMetodoPago);
		SegUsuario usuario = (SegUsuario) mDAO.findById(SegUsuario.class, loginDTO.getIdSegUsuario());
		factura.setTipoPago(pago);
		factura.setSegUsuario(usuario);
		factura.setMetodoPago(Mpago);
		factura.setVentaDevuelto(false);
		factura.setNumFactura(numeroFactura(Venta.class, "idVenta"));

		if (!tipoPago && fechaPago != null) {
			factura.setFechaPago(fechaPago);
			factura.setEstadoVenta("P"); // Pendiente
			factura.setDeudaParcial(factura.getTotal());
		} else {
			factura.setFechaPago(new Date());
			factura.setEstadoVenta("C"); // Cancelado
			factura.setDeudaParcial(new BigDecimal(0).setScale(2, RoundingMode.HALF_UP));
		}

		for (int i = 0; i < factura.getDetalleVentas().size(); i++) { // Reduce el stock de productos
			int idProducto = factura.getDetalleVentas().get(i).getProducto().getIdProducto();
			Producto prod = (Producto) mDAO.findById(Producto.class, idProducto);
			double cant = factura.getDetalleVentas().get(i).getCantidad().doubleValue(); // modificar esto a entero
			int stock = prod.getStock();
			prod.setStock((int) (stock - cant)); // cambiar esto a entero
			mDAO.actualizar(prod);
		}

		mDAO.insertar(factura);
		mAuditoria.mostrarLog(loginDTO, getClass(), "Factura registrada",
				"Factura: " + factura.getFecha() + " registrada");
	}

	// Metodo extra

	public String numeroFactura(Class clase, String identificador) {
		String numFactura = "";
		int count = 0;

		try {
			count = (int) mDAO.count(clase, identificador);
			if (count == 0) {
				numFactura = "FK-000000" + (count + 1);
				System.out.println("num: " + count + 1);
			} else {
				System.out.println("num: " + count + 1);
				if (count < 10) {
					numFactura = "FK-00000" + (count + 1);
				} else if (count < 100 && count >= 10) {
					numFactura = "FK-0000" + (count + 1);

				} else if (count < 1000 && count >= 100) {
					numFactura = "FK-000" + (count + 1);

				} else if (count < 10000 && count >= 1000) {
					numFactura = "FK-00" + (count + 1);

				} else if (count < 100000 && count >= 10000) {
					numFactura = "FK-0" + (count + 1);
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return numFactura;
	}

}
