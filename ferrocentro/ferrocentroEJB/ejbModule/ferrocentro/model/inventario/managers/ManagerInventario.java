package ferreteriakyms.model.inventario.managers;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import ferreteriakyms.model.auditoria.managers.ManagerAuditoria;
import ferreteriakyms.model.core.entities.DevolucionVenta;
import ferreteriakyms.model.core.entities.Venta;
import ferreteriakyms.model.core.managers.ManagerDAO;


@Stateless
@LocalBean
public class ManagerInventario {
	@EJB
	private ManagerDAO mDAO;
	@EJB
	private ManagerAuditoria mAuditoria;
	
    public ManagerInventario() {
        // TODO Auto-generated constructor stub
    }

    
    public List<DevolucionVenta> findAllDevolucionesTotales(){
      	 String sentenciaJPQL = "o.estado=true AND o.tipoDevolucion='T'";
      	 return mDAO.findAllListaDevoluciones(DevolucionVenta.class,sentenciaJPQL);
      }
    
    public List<DevolucionVenta> findAllDevolucionesParciales(){
      	 String sentenciaJPQL = "o.estado=true AND o.tipoDevolucion='P'";
      	 return mDAO.findAllListaDevoluciones(DevolucionVenta.class,sentenciaJPQL);
      }
}
