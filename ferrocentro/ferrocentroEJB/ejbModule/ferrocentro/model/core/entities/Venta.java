package ferreteriakyms.model.core.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the venta database table.
 * 
 */
@Entity
@NamedQuery(name="Venta.findAll", query="SELECT v FROM Venta v")
public class Venta implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_venta")
	private Integer idVenta;

	@Column(name="deuda_parcial")
	private BigDecimal deudaParcial;

	private Boolean estado;

	@Column(name="estado_venta")
	private String estadoVenta;

	@Temporal(TemporalType.DATE)
	private Date fecha;

	@Temporal(TemporalType.DATE)
	@Column(name="fecha_pago")
	private Date fechaPago;

	private BigDecimal impuesto;

	@Column(name="num_factura")
	private String numFactura;

	private BigDecimal total;

	@Column(name="venta_devuelto")
	private Boolean ventaDevuelto;

	//bi-directional many-to-one association to DetalleVenta
	@OneToMany(mappedBy="venta")
	private List<DetalleVenta> detalleVentas;

	//bi-directional many-to-one association to DevolucionVenta
	@OneToMany(mappedBy="venta")
	private List<DevolucionVenta> devolucionVentas;

	//bi-directional many-to-one association to Cliente
	@ManyToOne
	@JoinColumn(name="id_cliente")
	private Cliente cliente;

	//bi-directional many-to-one association to MetodoPago
	@ManyToOne
	@JoinColumn(name="id_metodo_pago")
	private MetodoPago metodoPago;

	//bi-directional many-to-one association to SegUsuario
	@ManyToOne
	@JoinColumn(name="id_seg_usuario")
	private SegUsuario segUsuario;

	//bi-directional many-to-one association to TipoPago
	@ManyToOne
	@JoinColumn(name="id_tipo_pago")
	private TipoPago tipoPago;

	public Venta() {
	}

	public Integer getIdVenta() {
		return this.idVenta;
	}

	public void setIdVenta(Integer idVenta) {
		this.idVenta = idVenta;
	}

	public BigDecimal getDeudaParcial() {
		return this.deudaParcial;
	}

	public void setDeudaParcial(BigDecimal deudaParcial) {
		this.deudaParcial = deudaParcial;
	}

	public Boolean getEstado() {
		return this.estado;
	}

	public void setEstado(Boolean estado) {
		this.estado = estado;
	}

	public String getEstadoVenta() {
		return this.estadoVenta;
	}

	public void setEstadoVenta(String estadoVenta) {
		this.estadoVenta = estadoVenta;
	}

	public Date getFecha() {
		return this.fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public Date getFechaPago() {
		return this.fechaPago;
	}

	public void setFechaPago(Date fechaPago) {
		this.fechaPago = fechaPago;
	}

	public BigDecimal getImpuesto() {
		return this.impuesto;
	}

	public void setImpuesto(BigDecimal impuesto) {
		this.impuesto = impuesto;
	}

	public String getNumFactura() {
		return this.numFactura;
	}

	public void setNumFactura(String numFactura) {
		this.numFactura = numFactura;
	}

	public BigDecimal getTotal() {
		return this.total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public Boolean getVentaDevuelto() {
		return this.ventaDevuelto;
	}

	public void setVentaDevuelto(Boolean ventaDevuelto) {
		this.ventaDevuelto = ventaDevuelto;
	}

	public List<DetalleVenta> getDetalleVentas() {
		return this.detalleVentas;
	}

	public void setDetalleVentas(List<DetalleVenta> detalleVentas) {
		this.detalleVentas = detalleVentas;
	}

	public DetalleVenta addDetalleVenta(DetalleVenta detalleVenta) {
		getDetalleVentas().add(detalleVenta);
		detalleVenta.setVenta(this);

		return detalleVenta;
	}

	public DetalleVenta removeDetalleVenta(DetalleVenta detalleVenta) {
		getDetalleVentas().remove(detalleVenta);
		detalleVenta.setVenta(null);

		return detalleVenta;
	}

	public List<DevolucionVenta> getDevolucionVentas() {
		return this.devolucionVentas;
	}

	public void setDevolucionVentas(List<DevolucionVenta> devolucionVentas) {
		this.devolucionVentas = devolucionVentas;
	}

	public DevolucionVenta addDevolucionVenta(DevolucionVenta devolucionVenta) {
		getDevolucionVentas().add(devolucionVenta);
		devolucionVenta.setVenta(this);

		return devolucionVenta;
	}

	public DevolucionVenta removeDevolucionVenta(DevolucionVenta devolucionVenta) {
		getDevolucionVentas().remove(devolucionVenta);
		devolucionVenta.setVenta(null);

		return devolucionVenta;
	}

	public Cliente getCliente() {
		return this.cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public MetodoPago getMetodoPago() {
		return this.metodoPago;
	}

	public void setMetodoPago(MetodoPago metodoPago) {
		this.metodoPago = metodoPago;
	}

	public SegUsuario getSegUsuario() {
		return this.segUsuario;
	}

	public void setSegUsuario(SegUsuario segUsuario) {
		this.segUsuario = segUsuario;
	}

	public TipoPago getTipoPago() {
		return this.tipoPago;
	}

	public void setTipoPago(TipoPago tipoPago) {
		this.tipoPago = tipoPago;
	}

}