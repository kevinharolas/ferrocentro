package ferreteriakyms.model.core.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the producto database table.
 * 
 */
@Entity
@NamedQuery(name="Producto.findAll", query="SELECT p FROM Producto p")
public class Producto implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_producto")
	private Integer idProducto;

	private String descripcion;

	private BigDecimal descuento;

	private Boolean estado;

	@Temporal(TemporalType.DATE)
	@Column(name="fecha_caducidad")
	private Date fechaCaducidad;

	private BigDecimal impuesto;

	private BigDecimal precio;

	@Column(name="precio_venta")
	private BigDecimal precioVenta;

	private Integer stock;

	@Column(name="stock_maximo")
	private Integer stockMaximo;

	@Column(name="stock_minimo")
	private Integer stockMinimo;

	private BigDecimal utilidad;

	//bi-directional many-to-one association to DetalleControl
	@OneToMany(mappedBy="producto")
	private List<DetalleControl> detalleControls;

	//bi-directional many-to-one association to DetalleDevolucionEntrada
	@OneToMany(mappedBy="producto")
	private List<DetalleDevolucionEntrada> detalleDevolucionEntradas;

	//bi-directional many-to-one association to DetalleDevolucionVenta
	@OneToMany(mappedBy="producto")
	private List<DetalleDevolucionVenta> detalleDevolucionVentas;

	//bi-directional many-to-one association to DetalleEntrada
	@OneToMany(mappedBy="producto")
	private List<DetalleEntrada> detalleEntradas;

	//bi-directional many-to-one association to DetalleOrdenCompra
	@OneToMany(mappedBy="producto")
	private List<DetalleOrdenCompra> detalleOrdenCompras;

	//bi-directional many-to-one association to DetalleProforma
	@OneToMany(mappedBy="producto")
	private List<DetalleProforma> detalleProformas;

	//bi-directional many-to-one association to DetalleVenta
	@OneToMany(mappedBy="producto")
	private List<DetalleVenta> detalleVentas;

	//bi-directional many-to-one association to Clasificacion
	@ManyToOne
	@JoinColumn(name="id_clasificacion")
	private Clasificacion clasificacion;

	//bi-directional many-to-one association to ListaPrecio
	@ManyToOne
	@JoinColumn(name="id_lista_precio")
	private ListaPrecio listaPrecio;

	//bi-directional many-to-one association to Marca
	@ManyToOne
	@JoinColumn(name="id_marca")
	private Marca marca;

	//bi-directional many-to-one association to SegUsuario
	@ManyToOne
	@JoinColumn(name="id_seg_usuario")
	private SegUsuario segUsuario;

	//bi-directional many-to-one association to TipoProducto
	@ManyToOne
	@JoinColumn(name="id_tipo_producto")
	private TipoProducto tipoProducto;

	//bi-directional many-to-one association to UnidadMedida
	@ManyToOne
	@JoinColumn(name="id_unidad_medida")
	private UnidadMedida unidadMedida;

	public Producto() {
	}

	public Integer getIdProducto() {
		return this.idProducto;
	}

	public void setIdProducto(Integer idProducto) {
		this.idProducto = idProducto;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public BigDecimal getDescuento() {
		return this.descuento;
	}

	public void setDescuento(BigDecimal descuento) {
		this.descuento = descuento;
	}

	public Boolean getEstado() {
		return this.estado;
	}

	public void setEstado(Boolean estado) {
		this.estado = estado;
	}

	public Date getFechaCaducidad() {
		return this.fechaCaducidad;
	}

	public void setFechaCaducidad(Date fechaCaducidad) {
		this.fechaCaducidad = fechaCaducidad;
	}

	public BigDecimal getImpuesto() {
		return this.impuesto;
	}

	public void setImpuesto(BigDecimal impuesto) {
		this.impuesto = impuesto;
	}

	public BigDecimal getPrecio() {
		return this.precio;
	}

	public void setPrecio(BigDecimal precio) {
		this.precio = precio;
	}

	public BigDecimal getPrecioVenta() {
		return this.precioVenta;
	}

	public void setPrecioVenta(BigDecimal precioVenta) {
		this.precioVenta = precioVenta;
	}

	public Integer getStock() {
		return this.stock;
	}

	public void setStock(Integer stock) {
		this.stock = stock;
	}

	public Integer getStockMaximo() {
		return this.stockMaximo;
	}

	public void setStockMaximo(Integer stockMaximo) {
		this.stockMaximo = stockMaximo;
	}

	public Integer getStockMinimo() {
		return this.stockMinimo;
	}

	public void setStockMinimo(Integer stockMinimo) {
		this.stockMinimo = stockMinimo;
	}

	public BigDecimal getUtilidad() {
		return this.utilidad;
	}

	public void setUtilidad(BigDecimal utilidad) {
		this.utilidad = utilidad;
	}

	public List<DetalleControl> getDetalleControls() {
		return this.detalleControls;
	}

	public void setDetalleControls(List<DetalleControl> detalleControls) {
		this.detalleControls = detalleControls;
	}

	public DetalleControl addDetalleControl(DetalleControl detalleControl) {
		getDetalleControls().add(detalleControl);
		detalleControl.setProducto(this);

		return detalleControl;
	}

	public DetalleControl removeDetalleControl(DetalleControl detalleControl) {
		getDetalleControls().remove(detalleControl);
		detalleControl.setProducto(null);

		return detalleControl;
	}

	public List<DetalleDevolucionEntrada> getDetalleDevolucionEntradas() {
		return this.detalleDevolucionEntradas;
	}

	public void setDetalleDevolucionEntradas(List<DetalleDevolucionEntrada> detalleDevolucionEntradas) {
		this.detalleDevolucionEntradas = detalleDevolucionEntradas;
	}

	public DetalleDevolucionEntrada addDetalleDevolucionEntrada(DetalleDevolucionEntrada detalleDevolucionEntrada) {
		getDetalleDevolucionEntradas().add(detalleDevolucionEntrada);
		detalleDevolucionEntrada.setProducto(this);

		return detalleDevolucionEntrada;
	}

	public DetalleDevolucionEntrada removeDetalleDevolucionEntrada(DetalleDevolucionEntrada detalleDevolucionEntrada) {
		getDetalleDevolucionEntradas().remove(detalleDevolucionEntrada);
		detalleDevolucionEntrada.setProducto(null);

		return detalleDevolucionEntrada;
	}

	public List<DetalleDevolucionVenta> getDetalleDevolucionVentas() {
		return this.detalleDevolucionVentas;
	}

	public void setDetalleDevolucionVentas(List<DetalleDevolucionVenta> detalleDevolucionVentas) {
		this.detalleDevolucionVentas = detalleDevolucionVentas;
	}

	public DetalleDevolucionVenta addDetalleDevolucionVenta(DetalleDevolucionVenta detalleDevolucionVenta) {
		getDetalleDevolucionVentas().add(detalleDevolucionVenta);
		detalleDevolucionVenta.setProducto(this);

		return detalleDevolucionVenta;
	}

	public DetalleDevolucionVenta removeDetalleDevolucionVenta(DetalleDevolucionVenta detalleDevolucionVenta) {
		getDetalleDevolucionVentas().remove(detalleDevolucionVenta);
		detalleDevolucionVenta.setProducto(null);

		return detalleDevolucionVenta;
	}

	public List<DetalleEntrada> getDetalleEntradas() {
		return this.detalleEntradas;
	}

	public void setDetalleEntradas(List<DetalleEntrada> detalleEntradas) {
		this.detalleEntradas = detalleEntradas;
	}

	public DetalleEntrada addDetalleEntrada(DetalleEntrada detalleEntrada) {
		getDetalleEntradas().add(detalleEntrada);
		detalleEntrada.setProducto(this);

		return detalleEntrada;
	}

	public DetalleEntrada removeDetalleEntrada(DetalleEntrada detalleEntrada) {
		getDetalleEntradas().remove(detalleEntrada);
		detalleEntrada.setProducto(null);

		return detalleEntrada;
	}

	public List<DetalleOrdenCompra> getDetalleOrdenCompras() {
		return this.detalleOrdenCompras;
	}

	public void setDetalleOrdenCompras(List<DetalleOrdenCompra> detalleOrdenCompras) {
		this.detalleOrdenCompras = detalleOrdenCompras;
	}

	public DetalleOrdenCompra addDetalleOrdenCompra(DetalleOrdenCompra detalleOrdenCompra) {
		getDetalleOrdenCompras().add(detalleOrdenCompra);
		detalleOrdenCompra.setProducto(this);

		return detalleOrdenCompra;
	}

	public DetalleOrdenCompra removeDetalleOrdenCompra(DetalleOrdenCompra detalleOrdenCompra) {
		getDetalleOrdenCompras().remove(detalleOrdenCompra);
		detalleOrdenCompra.setProducto(null);

		return detalleOrdenCompra;
	}

	public List<DetalleProforma> getDetalleProformas() {
		return this.detalleProformas;
	}

	public void setDetalleProformas(List<DetalleProforma> detalleProformas) {
		this.detalleProformas = detalleProformas;
	}

	public DetalleProforma addDetalleProforma(DetalleProforma detalleProforma) {
		getDetalleProformas().add(detalleProforma);
		detalleProforma.setProducto(this);

		return detalleProforma;
	}

	public DetalleProforma removeDetalleProforma(DetalleProforma detalleProforma) {
		getDetalleProformas().remove(detalleProforma);
		detalleProforma.setProducto(null);

		return detalleProforma;
	}

	public List<DetalleVenta> getDetalleVentas() {
		return this.detalleVentas;
	}

	public void setDetalleVentas(List<DetalleVenta> detalleVentas) {
		this.detalleVentas = detalleVentas;
	}

	public DetalleVenta addDetalleVenta(DetalleVenta detalleVenta) {
		getDetalleVentas().add(detalleVenta);
		detalleVenta.setProducto(this);

		return detalleVenta;
	}

	public DetalleVenta removeDetalleVenta(DetalleVenta detalleVenta) {
		getDetalleVentas().remove(detalleVenta);
		detalleVenta.setProducto(null);

		return detalleVenta;
	}

	public Clasificacion getClasificacion() {
		return this.clasificacion;
	}

	public void setClasificacion(Clasificacion clasificacion) {
		this.clasificacion = clasificacion;
	}

	public ListaPrecio getListaPrecio() {
		return this.listaPrecio;
	}

	public void setListaPrecio(ListaPrecio listaPrecio) {
		this.listaPrecio = listaPrecio;
	}

	public Marca getMarca() {
		return this.marca;
	}

	public void setMarca(Marca marca) {
		this.marca = marca;
	}

	public SegUsuario getSegUsuario() {
		return this.segUsuario;
	}

	public void setSegUsuario(SegUsuario segUsuario) {
		this.segUsuario = segUsuario;
	}

	public TipoProducto getTipoProducto() {
		return this.tipoProducto;
	}

	public void setTipoProducto(TipoProducto tipoProducto) {
		this.tipoProducto = tipoProducto;
	}

	public UnidadMedida getUnidadMedida() {
		return this.unidadMedida;
	}

	public void setUnidadMedida(UnidadMedida unidadMedida) {
		this.unidadMedida = unidadMedida;
	}

}