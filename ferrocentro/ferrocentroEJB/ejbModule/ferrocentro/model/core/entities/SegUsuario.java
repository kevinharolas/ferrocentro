package ferreteriakyms.model.core.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the seg_usuario database table.
 * 
 */
@Entity
@Table(name="seg_usuario")
@NamedQuery(name="SegUsuario.findAll", query="SELECT s FROM SegUsuario s")
public class SegUsuario implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_seg_usuario")
	private Integer idSegUsuario;

	private Boolean activo;

	private String apellidos;

	private String clave;

	private String codigo;

	private String correo;

	private String nombres;

	//bi-directional many-to-one association to Categoria
	@OneToMany(mappedBy="segUsuario")
	private List<Categoria> categorias;

	//bi-directional many-to-one association to Clasificacion
	@OneToMany(mappedBy="segUsuario")
	private List<Clasificacion> clasificacions;

	//bi-directional many-to-one association to Cliente
	@OneToMany(mappedBy="segUsuario")
	private List<Cliente> clientes;

	//bi-directional many-to-one association to Control
	@OneToMany(mappedBy="segUsuario")
	private List<Control> controls;

	//bi-directional many-to-one association to DevolucionEntrada
	@OneToMany(mappedBy="segUsuario")
	private List<DevolucionEntrada> devolucionEntradas;

	//bi-directional many-to-one association to DevolucionVenta
	@OneToMany(mappedBy="segUsuario")
	private List<DevolucionVenta> devolucionVentas;

	//bi-directional many-to-one association to Entrada
	@OneToMany(mappedBy="segUsuario")
	private List<Entrada> entradas;

	//bi-directional many-to-one association to ListaPrecio
	@OneToMany(mappedBy="segUsuario")
	private List<ListaPrecio> listaPrecios;

	//bi-directional many-to-one association to Marca
	@OneToMany(mappedBy="segUsuario")
	private List<Marca> marcas;

	//bi-directional many-to-one association to MetodoPago
	@OneToMany(mappedBy="segUsuario")
	private List<MetodoPago> metodoPagos;

	//bi-directional many-to-one association to OrdenCompra
	@OneToMany(mappedBy="segUsuario")
	private List<OrdenCompra> ordenCompras;

	//bi-directional many-to-one association to Producto
	@OneToMany(mappedBy="segUsuario")
	private List<Producto> productos;

	//bi-directional many-to-one association to Proforma
	@OneToMany(mappedBy="segUsuario")
	private List<Proforma> proformas;

	//bi-directional many-to-one association to Proveedor
	@OneToMany(mappedBy="segUsuario")
	private List<Proveedor> proveedors;

	//bi-directional many-to-one association to PryTarea
	@OneToMany(mappedBy="segUsuario")
	private List<PryTarea> pryTareas;

	//bi-directional many-to-one association to SegAsignacion
	@OneToMany(mappedBy="segUsuario")
	private List<SegAsignacion> segAsignacions;

	//bi-directional many-to-one association to Subcategoria
	@OneToMany(mappedBy="segUsuario")
	private List<Subcategoria> subcategorias;

	//bi-directional many-to-one association to ThmEmpleado
	@OneToMany(mappedBy="segUsuario")
	private List<ThmEmpleado> thmEmpleados;

	//bi-directional many-to-one association to TipoPago
	@OneToMany(mappedBy="segUsuario")
	private List<TipoPago> tipoPagos;

	//bi-directional many-to-one association to TipoProducto
	@OneToMany(mappedBy="segUsuario")
	private List<TipoProducto> tipoProductos;

	//bi-directional many-to-one association to UnidadMedida
	@OneToMany(mappedBy="segUsuario")
	private List<UnidadMedida> unidadMedidas;

	//bi-directional many-to-one association to Venta
	@OneToMany(mappedBy="segUsuario")
	private List<Venta> ventas;

	public SegUsuario() {
	}

	public Integer getIdSegUsuario() {
		return this.idSegUsuario;
	}

	public void setIdSegUsuario(Integer idSegUsuario) {
		this.idSegUsuario = idSegUsuario;
	}

	public Boolean getActivo() {
		return this.activo;
	}

	public void setActivo(Boolean activo) {
		this.activo = activo;
	}

	public String getApellidos() {
		return this.apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getClave() {
		return this.clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

	public String getCodigo() {
		return this.codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getCorreo() {
		return this.correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getNombres() {
		return this.nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public List<Categoria> getCategorias() {
		return this.categorias;
	}

	public void setCategorias(List<Categoria> categorias) {
		this.categorias = categorias;
	}

	public Categoria addCategoria(Categoria categoria) {
		getCategorias().add(categoria);
		categoria.setSegUsuario(this);

		return categoria;
	}

	public Categoria removeCategoria(Categoria categoria) {
		getCategorias().remove(categoria);
		categoria.setSegUsuario(null);

		return categoria;
	}

	public List<Clasificacion> getClasificacions() {
		return this.clasificacions;
	}

	public void setClasificacions(List<Clasificacion> clasificacions) {
		this.clasificacions = clasificacions;
	}

	public Clasificacion addClasificacion(Clasificacion clasificacion) {
		getClasificacions().add(clasificacion);
		clasificacion.setSegUsuario(this);

		return clasificacion;
	}

	public Clasificacion removeClasificacion(Clasificacion clasificacion) {
		getClasificacions().remove(clasificacion);
		clasificacion.setSegUsuario(null);

		return clasificacion;
	}

	public List<Cliente> getClientes() {
		return this.clientes;
	}

	public void setClientes(List<Cliente> clientes) {
		this.clientes = clientes;
	}

	public Cliente addCliente(Cliente cliente) {
		getClientes().add(cliente);
		cliente.setSegUsuario(this);

		return cliente;
	}

	public Cliente removeCliente(Cliente cliente) {
		getClientes().remove(cliente);
		cliente.setSegUsuario(null);

		return cliente;
	}

	public List<Control> getControls() {
		return this.controls;
	}

	public void setControls(List<Control> controls) {
		this.controls = controls;
	}

	public Control addControl(Control control) {
		getControls().add(control);
		control.setSegUsuario(this);

		return control;
	}

	public Control removeControl(Control control) {
		getControls().remove(control);
		control.setSegUsuario(null);

		return control;
	}

	public List<DevolucionEntrada> getDevolucionEntradas() {
		return this.devolucionEntradas;
	}

	public void setDevolucionEntradas(List<DevolucionEntrada> devolucionEntradas) {
		this.devolucionEntradas = devolucionEntradas;
	}

	public DevolucionEntrada addDevolucionEntrada(DevolucionEntrada devolucionEntrada) {
		getDevolucionEntradas().add(devolucionEntrada);
		devolucionEntrada.setSegUsuario(this);

		return devolucionEntrada;
	}

	public DevolucionEntrada removeDevolucionEntrada(DevolucionEntrada devolucionEntrada) {
		getDevolucionEntradas().remove(devolucionEntrada);
		devolucionEntrada.setSegUsuario(null);

		return devolucionEntrada;
	}

	public List<DevolucionVenta> getDevolucionVentas() {
		return this.devolucionVentas;
	}

	public void setDevolucionVentas(List<DevolucionVenta> devolucionVentas) {
		this.devolucionVentas = devolucionVentas;
	}

	public DevolucionVenta addDevolucionVenta(DevolucionVenta devolucionVenta) {
		getDevolucionVentas().add(devolucionVenta);
		devolucionVenta.setSegUsuario(this);

		return devolucionVenta;
	}

	public DevolucionVenta removeDevolucionVenta(DevolucionVenta devolucionVenta) {
		getDevolucionVentas().remove(devolucionVenta);
		devolucionVenta.setSegUsuario(null);

		return devolucionVenta;
	}

	public List<Entrada> getEntradas() {
		return this.entradas;
	}

	public void setEntradas(List<Entrada> entradas) {
		this.entradas = entradas;
	}

	public Entrada addEntrada(Entrada entrada) {
		getEntradas().add(entrada);
		entrada.setSegUsuario(this);

		return entrada;
	}

	public Entrada removeEntrada(Entrada entrada) {
		getEntradas().remove(entrada);
		entrada.setSegUsuario(null);

		return entrada;
	}

	public List<ListaPrecio> getListaPrecios() {
		return this.listaPrecios;
	}

	public void setListaPrecios(List<ListaPrecio> listaPrecios) {
		this.listaPrecios = listaPrecios;
	}

	public ListaPrecio addListaPrecio(ListaPrecio listaPrecio) {
		getListaPrecios().add(listaPrecio);
		listaPrecio.setSegUsuario(this);

		return listaPrecio;
	}

	public ListaPrecio removeListaPrecio(ListaPrecio listaPrecio) {
		getListaPrecios().remove(listaPrecio);
		listaPrecio.setSegUsuario(null);

		return listaPrecio;
	}

	public List<Marca> getMarcas() {
		return this.marcas;
	}

	public void setMarcas(List<Marca> marcas) {
		this.marcas = marcas;
	}

	public Marca addMarca(Marca marca) {
		getMarcas().add(marca);
		marca.setSegUsuario(this);

		return marca;
	}

	public Marca removeMarca(Marca marca) {
		getMarcas().remove(marca);
		marca.setSegUsuario(null);

		return marca;
	}

	public List<MetodoPago> getMetodoPagos() {
		return this.metodoPagos;
	}

	public void setMetodoPagos(List<MetodoPago> metodoPagos) {
		this.metodoPagos = metodoPagos;
	}

	public MetodoPago addMetodoPago(MetodoPago metodoPago) {
		getMetodoPagos().add(metodoPago);
		metodoPago.setSegUsuario(this);

		return metodoPago;
	}

	public MetodoPago removeMetodoPago(MetodoPago metodoPago) {
		getMetodoPagos().remove(metodoPago);
		metodoPago.setSegUsuario(null);

		return metodoPago;
	}

	public List<OrdenCompra> getOrdenCompras() {
		return this.ordenCompras;
	}

	public void setOrdenCompras(List<OrdenCompra> ordenCompras) {
		this.ordenCompras = ordenCompras;
	}

	public OrdenCompra addOrdenCompra(OrdenCompra ordenCompra) {
		getOrdenCompras().add(ordenCompra);
		ordenCompra.setSegUsuario(this);

		return ordenCompra;
	}

	public OrdenCompra removeOrdenCompra(OrdenCompra ordenCompra) {
		getOrdenCompras().remove(ordenCompra);
		ordenCompra.setSegUsuario(null);

		return ordenCompra;
	}

	public List<Producto> getProductos() {
		return this.productos;
	}

	public void setProductos(List<Producto> productos) {
		this.productos = productos;
	}

	public Producto addProducto(Producto producto) {
		getProductos().add(producto);
		producto.setSegUsuario(this);

		return producto;
	}

	public Producto removeProducto(Producto producto) {
		getProductos().remove(producto);
		producto.setSegUsuario(null);

		return producto;
	}

	public List<Proforma> getProformas() {
		return this.proformas;
	}

	public void setProformas(List<Proforma> proformas) {
		this.proformas = proformas;
	}

	public Proforma addProforma(Proforma proforma) {
		getProformas().add(proforma);
		proforma.setSegUsuario(this);

		return proforma;
	}

	public Proforma removeProforma(Proforma proforma) {
		getProformas().remove(proforma);
		proforma.setSegUsuario(null);

		return proforma;
	}

	public List<Proveedor> getProveedors() {
		return this.proveedors;
	}

	public void setProveedors(List<Proveedor> proveedors) {
		this.proveedors = proveedors;
	}

	public Proveedor addProveedor(Proveedor proveedor) {
		getProveedors().add(proveedor);
		proveedor.setSegUsuario(this);

		return proveedor;
	}

	public Proveedor removeProveedor(Proveedor proveedor) {
		getProveedors().remove(proveedor);
		proveedor.setSegUsuario(null);

		return proveedor;
	}

	public List<PryTarea> getPryTareas() {
		return this.pryTareas;
	}

	public void setPryTareas(List<PryTarea> pryTareas) {
		this.pryTareas = pryTareas;
	}

	public PryTarea addPryTarea(PryTarea pryTarea) {
		getPryTareas().add(pryTarea);
		pryTarea.setSegUsuario(this);

		return pryTarea;
	}

	public PryTarea removePryTarea(PryTarea pryTarea) {
		getPryTareas().remove(pryTarea);
		pryTarea.setSegUsuario(null);

		return pryTarea;
	}

	public List<SegAsignacion> getSegAsignacions() {
		return this.segAsignacions;
	}

	public void setSegAsignacions(List<SegAsignacion> segAsignacions) {
		this.segAsignacions = segAsignacions;
	}

	public SegAsignacion addSegAsignacion(SegAsignacion segAsignacion) {
		getSegAsignacions().add(segAsignacion);
		segAsignacion.setSegUsuario(this);

		return segAsignacion;
	}

	public SegAsignacion removeSegAsignacion(SegAsignacion segAsignacion) {
		getSegAsignacions().remove(segAsignacion);
		segAsignacion.setSegUsuario(null);

		return segAsignacion;
	}

	public List<Subcategoria> getSubcategorias() {
		return this.subcategorias;
	}

	public void setSubcategorias(List<Subcategoria> subcategorias) {
		this.subcategorias = subcategorias;
	}

	public Subcategoria addSubcategoria(Subcategoria subcategoria) {
		getSubcategorias().add(subcategoria);
		subcategoria.setSegUsuario(this);

		return subcategoria;
	}

	public Subcategoria removeSubcategoria(Subcategoria subcategoria) {
		getSubcategorias().remove(subcategoria);
		subcategoria.setSegUsuario(null);

		return subcategoria;
	}

	public List<ThmEmpleado> getThmEmpleados() {
		return this.thmEmpleados;
	}

	public void setThmEmpleados(List<ThmEmpleado> thmEmpleados) {
		this.thmEmpleados = thmEmpleados;
	}

	public ThmEmpleado addThmEmpleado(ThmEmpleado thmEmpleado) {
		getThmEmpleados().add(thmEmpleado);
		thmEmpleado.setSegUsuario(this);

		return thmEmpleado;
	}

	public ThmEmpleado removeThmEmpleado(ThmEmpleado thmEmpleado) {
		getThmEmpleados().remove(thmEmpleado);
		thmEmpleado.setSegUsuario(null);

		return thmEmpleado;
	}

	public List<TipoPago> getTipoPagos() {
		return this.tipoPagos;
	}

	public void setTipoPagos(List<TipoPago> tipoPagos) {
		this.tipoPagos = tipoPagos;
	}

	public TipoPago addTipoPago(TipoPago tipoPago) {
		getTipoPagos().add(tipoPago);
		tipoPago.setSegUsuario(this);

		return tipoPago;
	}

	public TipoPago removeTipoPago(TipoPago tipoPago) {
		getTipoPagos().remove(tipoPago);
		tipoPago.setSegUsuario(null);

		return tipoPago;
	}

	public List<TipoProducto> getTipoProductos() {
		return this.tipoProductos;
	}

	public void setTipoProductos(List<TipoProducto> tipoProductos) {
		this.tipoProductos = tipoProductos;
	}

	public TipoProducto addTipoProducto(TipoProducto tipoProducto) {
		getTipoProductos().add(tipoProducto);
		tipoProducto.setSegUsuario(this);

		return tipoProducto;
	}

	public TipoProducto removeTipoProducto(TipoProducto tipoProducto) {
		getTipoProductos().remove(tipoProducto);
		tipoProducto.setSegUsuario(null);

		return tipoProducto;
	}

	public List<UnidadMedida> getUnidadMedidas() {
		return this.unidadMedidas;
	}

	public void setUnidadMedidas(List<UnidadMedida> unidadMedidas) {
		this.unidadMedidas = unidadMedidas;
	}

	public UnidadMedida addUnidadMedida(UnidadMedida unidadMedida) {
		getUnidadMedidas().add(unidadMedida);
		unidadMedida.setSegUsuario(this);

		return unidadMedida;
	}

	public UnidadMedida removeUnidadMedida(UnidadMedida unidadMedida) {
		getUnidadMedidas().remove(unidadMedida);
		unidadMedida.setSegUsuario(null);

		return unidadMedida;
	}

	public List<Venta> getVentas() {
		return this.ventas;
	}

	public void setVentas(List<Venta> ventas) {
		this.ventas = ventas;
	}

	public Venta addVenta(Venta venta) {
		getVentas().add(venta);
		venta.setSegUsuario(this);

		return venta;
	}

	public Venta removeVenta(Venta venta) {
		getVentas().remove(venta);
		venta.setSegUsuario(null);

		return venta;
	}

}