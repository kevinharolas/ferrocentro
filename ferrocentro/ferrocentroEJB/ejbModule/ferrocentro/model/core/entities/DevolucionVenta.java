package ferreteriakyms.model.core.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the devolucion_venta database table.
 * 
 */
@Entity
@Table(name="devolucion_venta")
@NamedQuery(name="DevolucionVenta.findAll", query="SELECT d FROM DevolucionVenta d")
public class DevolucionVenta implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_devolucion_venta")
	private Integer idDevolucionVenta;

	private String descripcion;

	private Boolean estado;

	@Temporal(TemporalType.DATE)
	@Column(name="fecha_devolucion")
	private Date fechaDevolucion;

	@Column(name="saldo_devolver")
	private BigDecimal saldoDevolver;

	@Column(name="tipo_devolucion")
	private String tipoDevolucion;

	//bi-directional many-to-one association to DetalleDevolucionVenta
	@OneToMany(mappedBy="devolucionVenta")
	private List<DetalleDevolucionVenta> detalleDevolucionVentas;

	//bi-directional many-to-one association to SegUsuario
	@ManyToOne
	@JoinColumn(name="id_seg_usuario")
	private SegUsuario segUsuario;

	//bi-directional many-to-one association to Venta
	@ManyToOne
	@JoinColumn(name="id_venta")
	private Venta venta;

	public DevolucionVenta() {
	}

	public Integer getIdDevolucionVenta() {
		return this.idDevolucionVenta;
	}

	public void setIdDevolucionVenta(Integer idDevolucionVenta) {
		this.idDevolucionVenta = idDevolucionVenta;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Boolean getEstado() {
		return this.estado;
	}

	public void setEstado(Boolean estado) {
		this.estado = estado;
	}

	public Date getFechaDevolucion() {
		return this.fechaDevolucion;
	}

	public void setFechaDevolucion(Date fechaDevolucion) {
		this.fechaDevolucion = fechaDevolucion;
	}

	public BigDecimal getSaldoDevolver() {
		return this.saldoDevolver;
	}

	public void setSaldoDevolver(BigDecimal saldoDevolver) {
		this.saldoDevolver = saldoDevolver;
	}

	public String getTipoDevolucion() {
		return this.tipoDevolucion;
	}

	public void setTipoDevolucion(String tipoDevolucion) {
		this.tipoDevolucion = tipoDevolucion;
	}

	public List<DetalleDevolucionVenta> getDetalleDevolucionVentas() {
		return this.detalleDevolucionVentas;
	}

	public void setDetalleDevolucionVentas(List<DetalleDevolucionVenta> detalleDevolucionVentas) {
		this.detalleDevolucionVentas = detalleDevolucionVentas;
	}

	public DetalleDevolucionVenta addDetalleDevolucionVenta(DetalleDevolucionVenta detalleDevolucionVenta) {
		getDetalleDevolucionVentas().add(detalleDevolucionVenta);
		detalleDevolucionVenta.setDevolucionVenta(this);

		return detalleDevolucionVenta;
	}

	public DetalleDevolucionVenta removeDetalleDevolucionVenta(DetalleDevolucionVenta detalleDevolucionVenta) {
		getDetalleDevolucionVentas().remove(detalleDevolucionVenta);
		detalleDevolucionVenta.setDevolucionVenta(null);

		return detalleDevolucionVenta;
	}

	public SegUsuario getSegUsuario() {
		return this.segUsuario;
	}

	public void setSegUsuario(SegUsuario segUsuario) {
		this.segUsuario = segUsuario;
	}

	public Venta getVenta() {
		return this.venta;
	}

	public void setVenta(Venta venta) {
		this.venta = venta;
	}

}