package ferreteriakyms.model.core.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the detalle_devolucion_venta database table.
 * 
 */
@Entity
@Table(name="detalle_devolucion_venta")
@NamedQuery(name="DetalleDevolucionVenta.findAll", query="SELECT d FROM DetalleDevolucionVenta d")
public class DetalleDevolucionVenta implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_detalle_devolucion_venta")
	private Integer idDetalleDevolucionVenta;

	@Column(name="cantidad_devuelto")
	private Integer cantidadDevuelto;

	//bi-directional many-to-one association to DevolucionVenta
	@ManyToOne
	@JoinColumn(name="id_devolucion_venta")
	private DevolucionVenta devolucionVenta;

	//bi-directional many-to-one association to Producto
	@ManyToOne
	@JoinColumn(name="id_producto")
	private Producto producto;

	public DetalleDevolucionVenta() {
	}

	public Integer getIdDetalleDevolucionVenta() {
		return this.idDetalleDevolucionVenta;
	}

	public void setIdDetalleDevolucionVenta(Integer idDetalleDevolucionVenta) {
		this.idDetalleDevolucionVenta = idDetalleDevolucionVenta;
	}

	public Integer getCantidadDevuelto() {
		return this.cantidadDevuelto;
	}

	public void setCantidadDevuelto(Integer cantidadDevuelto) {
		this.cantidadDevuelto = cantidadDevuelto;
	}

	public DevolucionVenta getDevolucionVenta() {
		return this.devolucionVenta;
	}

	public void setDevolucionVenta(DevolucionVenta devolucionVenta) {
		this.devolucionVenta = devolucionVenta;
	}

	public Producto getProducto() {
		return this.producto;
	}

	public void setProducto(Producto producto) {
		this.producto = producto;
	}

}