package ferreteriakyms.model.core.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the detalle_devolucion_entrada database table.
 * 
 */
@Entity
@Table(name="detalle_devolucion_entrada")
@NamedQuery(name="DetalleDevolucionEntrada.findAll", query="SELECT d FROM DetalleDevolucionEntrada d")
public class DetalleDevolucionEntrada implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_detalle_devolucion_entrada")
	private Integer idDetalleDevolucionEntrada;

	@Column(name="cantidad_devuelto")
	private Integer cantidadDevuelto;

	//bi-directional many-to-one association to DevolucionEntrada
	@ManyToOne
	@JoinColumn(name="id_devolucion_entrada")
	private DevolucionEntrada devolucionEntrada;

	//bi-directional many-to-one association to Producto
	@ManyToOne
	@JoinColumn(name="id_producto")
	private Producto producto;

	public DetalleDevolucionEntrada() {
	}

	public Integer getIdDetalleDevolucionEntrada() {
		return this.idDetalleDevolucionEntrada;
	}

	public void setIdDetalleDevolucionEntrada(Integer idDetalleDevolucionEntrada) {
		this.idDetalleDevolucionEntrada = idDetalleDevolucionEntrada;
	}

	public Integer getCantidadDevuelto() {
		return this.cantidadDevuelto;
	}

	public void setCantidadDevuelto(Integer cantidadDevuelto) {
		this.cantidadDevuelto = cantidadDevuelto;
	}

	public DevolucionEntrada getDevolucionEntrada() {
		return this.devolucionEntrada;
	}

	public void setDevolucionEntrada(DevolucionEntrada devolucionEntrada) {
		this.devolucionEntrada = devolucionEntrada;
	}

	public Producto getProducto() {
		return this.producto;
	}

	public void setProducto(Producto producto) {
		this.producto = producto;
	}

}