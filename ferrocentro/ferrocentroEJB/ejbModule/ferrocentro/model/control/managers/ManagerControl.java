package ferreteriakyms.model.control.managers;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import ferreteriakyms.model.auditoria.managers.ManagerAuditoria;
import ferreteriakyms.model.core.entities.Cliente;
import ferreteriakyms.model.core.entities.Control;
import ferreteriakyms.model.core.entities.DetalleControl;
import ferreteriakyms.model.core.entities.DetalleVenta;
import ferreteriakyms.model.core.entities.Producto;
import ferreteriakyms.model.core.entities.SegUsuario;
import ferreteriakyms.model.core.entities.TipoPago;
import ferreteriakyms.model.core.entities.Venta;
import ferreteriakyms.model.core.managers.ManagerDAO;
import ferreteriakyms.model.seguridades.dtos.LoginDTO;

/**
 * Session Bean implementation class ManagerControl
 */
@Stateless
@LocalBean
public class ManagerControl {
	@EJB
	private ManagerDAO mDAO;
	@EJB
	private ManagerAuditoria mAuditoria;

	public ManagerControl() {
		// TODO Auto-generated constructor stub
	}

	public List<Control> allControles() {
		String sentenciaJPQL = "o.estado=true";
		return mDAO.findWhere(Control.class, sentenciaJPQL);
	}

	public List<TipoPago> allTipoPago() {
		String sentenciaJPQL = "o.estado=true";
		return mDAO.findWhere(TipoPago.class, sentenciaJPQL);
	}

	public List<Producto> allProductos() {
		return mDAO.findAll(Producto.class);
	}

//===================== TIPO DE PAGO ======================================
	public void insertarPago(LoginDTO loginDTO, TipoPago nuevoPago) throws Exception {
		if (nuevoPago == null)
			throw new Exception("Debe ingresar los datos del pago");

		nuevoPago.setEstado(true);
		mDAO.insertar(nuevoPago);
		mAuditoria.mostrarLog(loginDTO, getClass(), "Pago insertado",
				"Pago: " + nuevoPago.getDescripcion() + " insertado");
	}

	public void eliminarPago(LoginDTO loginDTO, TipoPago pag) throws Exception {
		TipoPago pago = (TipoPago) mDAO.findById(TipoPago.class, pag.getIdTipoPago());
		pago.setEstado(false);
		mDAO.actualizar(pago);
		mAuditoria.mostrarLog(loginDTO, getClass(), "Pago eliminado", "Pago: " + pago.getDescripcion() + " eliminado");
	}

	public void actualizarPago(LoginDTO loginDTO, TipoPago pag) throws Exception {
		TipoPago pago = (TipoPago) mDAO.findById(TipoPago.class, pag.getIdTipoPago());
		pago.setDescripcion(pago.getDescripcion());
		mDAO.actualizar(pago);
		mAuditoria.mostrarLog(loginDTO, getClass(), "Pago actualizado",
				"Pago: " + pago.getDescripcion() + " actualizado");
	}

	// ============================== Modificar Stock Por Da�os o Situaciones
	// ==============================

	public void modificarStockProducto(LoginDTO loginDTO, Producto pro, int cantidad, boolean tipoAltaBaja)
			throws Exception {
		Producto producto = (Producto) mDAO.findById(Producto.class, pro.getIdProducto());
		int stock = producto.getStock();

		if (tipoAltaBaja) {
			producto.setStock(stock + cantidad);
		} else {
			producto.setStock(stock - cantidad);
		}

		mDAO.actualizar(producto);
		mAuditoria.mostrarLog(loginDTO, getClass(), "Stock Modificado",
				"Stock de Producto: " + producto.getDescripcion() + " modificado");
	}

	// ============================== Ajustar Stock Minimo o Maximo
	// ==============================
	public void AjustarStockProductoMinMax(LoginDTO loginDTO, Producto pro, int cantidad, boolean tipoMinMax)
			throws Exception {
		Producto producto = (Producto) mDAO.findById(Producto.class, pro.getIdProducto());

		if (tipoMinMax) {
			producto.setStockMaximo(cantidad);
			mAuditoria.mostrarLog(loginDTO, getClass(), "Stock M�ximo Modificado",
					"Stock de Producto: " + producto.getDescripcion() + " modificado");
		} else {
			producto.setStockMinimo(cantidad);
			mAuditoria.mostrarLog(loginDTO, getClass(), "Stock M�nimo Modificado",
					"Stock de Producto: " + producto.getDescripcion() + " modificado");
		}

		mDAO.actualizar(producto);
	}

	// =================Modificar Precio de Producto =================
	public void modificarPrecioProducto(LoginDTO loginDTO, Producto pro, double valor) throws Exception {
		System.out.println("valor modificar: " + valor);
		Producto prod = (Producto) mDAO.findById(Producto.class, pro.getIdProducto());
		prod.setPrecioVenta(new BigDecimal(valor).setScale(2, RoundingMode.HALF_UP));
		// mDAO.actualizar(prod);
		// mAuditoria.mostrarLog(loginDTO, getClass(), "Precio modificado","Precio :" +
		// prod.getDescripcion() + " Modificado");
	}

	// =================Ajustar Utilidad por Producto =================
	public void ajustarPorcetajeUtilidad(LoginDTO loginDTO, Producto pro, double porcentaje) throws Exception {
		Producto producto = (Producto) mDAO.findById(Producto.class, pro.getIdProducto());

		double valorPorcentaje = porcentaje / 100;
		double precioVenta = producto.getPrecio().doubleValue() / (1 - valorPorcentaje);
		producto.setUtilidad(new BigDecimal(valorPorcentaje).setScale(2, RoundingMode.HALF_UP));
		producto.setPrecioVenta(new BigDecimal(precioVenta).setScale(2, RoundingMode.HALF_UP));
		mDAO.actualizar(producto);
		mAuditoria.mostrarLog(loginDTO, getClass(), "Utilidad agregada",
				"Utilidad de :" + producto.getDescripcion() + " agregado");
	}

	// =================Ajustar Descuento por Producto =================
	public void ajustarPorcetajeDescuento(LoginDTO loginDTO, Producto pro, double porcentaje) throws Exception {
		Producto producto = (Producto) mDAO.findById(Producto.class, pro.getIdProducto());

		double valorPorcentaje = porcentaje / 100;
		double precioVenta = producto.getPrecioVenta().doubleValue() * (1 - valorPorcentaje);
		producto.setDescuento(new BigDecimal(valorPorcentaje).setScale(2, RoundingMode.HALF_UP));
		producto.setPrecioVenta(new BigDecimal(precioVenta).setScale(2, RoundingMode.HALF_UP));
		mDAO.actualizar(producto);
		mAuditoria.mostrarLog(loginDTO, getClass(), "Descuento agregada",
				"Descuento de :" + producto.getDescripcion() + " agregado");
	}

	/* ============================================================== */
	/* CONTROL */
	/* ============================================================== */

	public List<Control> allFacturas() {
		String sentenciaJPQL = "o.estado=true";
		return mDAO.findWhere(Control.class, sentenciaJPQL);
	}

	public Control adicionarProducto(Control control, Producto prodSeleccionado, int cantidadd, int existenciaf,
			int diferencia) throws Exception {
		// comprobar si es la primera vez
		if (control == null) {
			control = new Control(); // cabecera factura
			control.setDetalleControls(new ArrayList<DetalleControl>()); // detalle de la factura
			control.setFecha(new Date());
			control.setObservacion("");
			control.setEstado(true);
		}

		// adicionamos el detalle:

		Producto prod = (Producto) mDAO.findById(Producto.class, prodSeleccionado.getIdProducto());
		// calculos

		DetalleControl detalle = new DetalleControl();
		detalle.setControl(control);
		detalle.setProducto(prod);
		detalle.setCantidadDeteriorado(cantidadd);
		detalle.setExistenciaFisica(existenciaf);
		detalle.setDiferencia(diferencia);

		control.getDetalleControls().add(detalle);
		return control;
	}

	// public Control adicionarProducto(Control control, Producto prodSeleccionado,
	// int cantidadd, int existenciaf , int diferencia ) throws Exception {

	public void registrarControl(LoginDTO loginDTO, Control control, int cantidadd, int existenciaf, int diferencia)
			throws Exception {
		if (control == null || control.getDetalleControls() == null || control.getDetalleControls().size() == 0)
			throw new Exception("Debe seleccionar al menos un producto");

		SegUsuario usuario = (SegUsuario) mDAO.findById(SegUsuario.class, loginDTO.getIdSegUsuario());

		control.setSegUsuario(usuario);

		// control.set(numeroFactura(Control.class, "idControl"));

		mDAO.insertar(control);
		mAuditoria.mostrarLog(loginDTO, getClass(), "Control  registrado",
				"Control: " + control.getFecha() + " registrado");
	}



	public void anularFactura(Control fac) throws Exception {
		Control control = (Control) mDAO.findById(Control.class, fac.getIdControl());
		control.setEstado(false);
		mDAO.actualizar(control);
		mAuditoria.mostrarLog(getClass(), "Registro de control anulada",
				"Factura:" + control.getIdControl() + "anulada");
	}

	// Metodo extra

	public String numeroFactura(Class clase, String identificador) {
		String numFactura = "";
		int count = 0;

		try {
			count = (int) mDAO.count(clase, identificador);
			if (count == 0) {
				numFactura = "FK-000000" + (count + 1);
				System.out.println("num: " + count + 1);
			} else {
				System.out.println("num: " + count + 1);
				if (count < 10) {
					numFactura = "FK-00000" + (count + 1);
				} else if (count < 100 && count >= 10) {
					numFactura = "FK-0000" + (count + 1);

				} else if (count < 1000 && count >= 100) {
					numFactura = "FK-000" + (count + 1);

				} else if (count < 10000 && count >= 1000) {
					numFactura = "FK-00" + (count + 1);

				} else if (count < 100000 && count >= 10000) {
					numFactura = "FK-0" + (count + 1);
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return numFactura;
	}

}
