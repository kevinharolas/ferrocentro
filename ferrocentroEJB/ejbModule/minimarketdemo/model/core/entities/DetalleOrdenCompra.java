package minimarketdemo.model.core.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the detalle_orden_compra database table.
 * 
 */
@Entity
@Table(name="detalle_orden_compra")
@NamedQuery(name="DetalleOrdenCompra.findAll", query="SELECT d FROM DetalleOrdenCompra d")
public class DetalleOrdenCompra implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_detalle_proforma", unique=true, nullable=false)
	private Integer idDetalleProforma;

	@Column(nullable=false)
	private Integer cantidad;

	@Column(nullable=false, precision=10, scale=2)
	private BigDecimal precio;

	//bi-directional many-to-one association to OrdenCompra
	@ManyToOne
	@JoinColumn(name="id_orden_compra", nullable=false)
	private OrdenCompra ordenCompra;

	//bi-directional many-to-one association to Producto
	@ManyToOne
	@JoinColumn(name="id_producto", nullable=false)
	private Producto producto;

	public DetalleOrdenCompra() {
	}

	public Integer getIdDetalleProforma() {
		return this.idDetalleProforma;
	}

	public void setIdDetalleProforma(Integer idDetalleProforma) {
		this.idDetalleProforma = idDetalleProforma;
	}

	public Integer getCantidad() {
		return this.cantidad;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}

	public BigDecimal getPrecio() {
		return this.precio;
	}

	public void setPrecio(BigDecimal precio) {
		this.precio = precio;
	}

	public OrdenCompra getOrdenCompra() {
		return this.ordenCompra;
	}

	public void setOrdenCompra(OrdenCompra ordenCompra) {
		this.ordenCompra = ordenCompra;
	}

	public Producto getProducto() {
		return this.producto;
	}

	public void setProducto(Producto producto) {
		this.producto = producto;
	}

}