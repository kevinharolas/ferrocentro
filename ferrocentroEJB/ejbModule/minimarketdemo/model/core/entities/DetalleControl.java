package minimarketdemo.model.core.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the detalle_control database table.
 * 
 */
@Entity
@Table(name="detalle_control")
@NamedQuery(name="DetalleControl.findAll", query="SELECT d FROM DetalleControl d")
public class DetalleControl implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_detalle_control", unique=true, nullable=false)
	private Integer idDetalleControl;

	@Column(name="cantidad_deteriorado", nullable=false)
	private Integer cantidadDeteriorado;

	@Column(nullable=false)
	private Integer diferencia;

	@Column(name="existencia_fisica", nullable=false)
	private Integer existenciaFisica;

	//bi-directional many-to-one association to Control
	@ManyToOne
	@JoinColumn(name="id_control", nullable=false)
	private Control control;

	//bi-directional many-to-one association to Producto
	@ManyToOne
	@JoinColumn(name="id_producto", nullable=false)
	private Producto producto;

	public DetalleControl() {
	}

	public Integer getIdDetalleControl() {
		return this.idDetalleControl;
	}

	public void setIdDetalleControl(Integer idDetalleControl) {
		this.idDetalleControl = idDetalleControl;
	}

	public Integer getCantidadDeteriorado() {
		return this.cantidadDeteriorado;
	}

	public void setCantidadDeteriorado(Integer cantidadDeteriorado) {
		this.cantidadDeteriorado = cantidadDeteriorado;
	}

	public Integer getDiferencia() {
		return this.diferencia;
	}

	public void setDiferencia(Integer diferencia) {
		this.diferencia = diferencia;
	}

	public Integer getExistenciaFisica() {
		return this.existenciaFisica;
	}

	public void setExistenciaFisica(Integer existenciaFisica) {
		this.existenciaFisica = existenciaFisica;
	}

	public Control getControl() {
		return this.control;
	}

	public void setControl(Control control) {
		this.control = control;
	}

	public Producto getProducto() {
		return this.producto;
	}

	public void setProducto(Producto producto) {
		this.producto = producto;
	}

}