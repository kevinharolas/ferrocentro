package minimarketdemo.model.core.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the devolucion_entrada database table.
 * 
 */
@Entity
@Table(name="devolucion_entrada")
@NamedQuery(name="DevolucionEntrada.findAll", query="SELECT d FROM DevolucionEntrada d")
public class DevolucionEntrada implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_devolucion_entrada", unique=true, nullable=false)
	private Integer idDevolucionEntrada;

	@Column(nullable=false, length=2147483647)
	private String descripcion;

	@Column(nullable=false)
	private Boolean estado;

	@Temporal(TemporalType.DATE)
	@Column(name="fecha_devolucion", nullable=false)
	private Date fechaDevolucion;

	@Column(name="saldo_devolver", nullable=false, precision=10, scale=2)
	private BigDecimal saldoDevolver;

	@Column(name="tipo_devolucion", nullable=false, length=20)
	private String tipoDevolucion;

	//bi-directional many-to-one association to DetalleDevolucionEntrada
	@OneToMany(mappedBy="devolucionEntrada")
	private List<DetalleDevolucionEntrada> detalleDevolucionEntradas;

	//bi-directional many-to-one association to Entrada
	@ManyToOne
	@JoinColumn(name="id_entrada", nullable=false)
	private Entrada entrada;

	//bi-directional many-to-one association to SegUsuario
	@ManyToOne
	@JoinColumn(name="id_seg_usuario", nullable=false)
	private SegUsuario segUsuario;

	public DevolucionEntrada() {
	}

	public Integer getIdDevolucionEntrada() {
		return this.idDevolucionEntrada;
	}

	public void setIdDevolucionEntrada(Integer idDevolucionEntrada) {
		this.idDevolucionEntrada = idDevolucionEntrada;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Boolean getEstado() {
		return this.estado;
	}

	public void setEstado(Boolean estado) {
		this.estado = estado;
	}

	public Date getFechaDevolucion() {
		return this.fechaDevolucion;
	}

	public void setFechaDevolucion(Date fechaDevolucion) {
		this.fechaDevolucion = fechaDevolucion;
	}

	public BigDecimal getSaldoDevolver() {
		return this.saldoDevolver;
	}

	public void setSaldoDevolver(BigDecimal saldoDevolver) {
		this.saldoDevolver = saldoDevolver;
	}

	public String getTipoDevolucion() {
		return this.tipoDevolucion;
	}

	public void setTipoDevolucion(String tipoDevolucion) {
		this.tipoDevolucion = tipoDevolucion;
	}

	public List<DetalleDevolucionEntrada> getDetalleDevolucionEntradas() {
		return this.detalleDevolucionEntradas;
	}

	public void setDetalleDevolucionEntradas(List<DetalleDevolucionEntrada> detalleDevolucionEntradas) {
		this.detalleDevolucionEntradas = detalleDevolucionEntradas;
	}

	public DetalleDevolucionEntrada addDetalleDevolucionEntrada(DetalleDevolucionEntrada detalleDevolucionEntrada) {
		getDetalleDevolucionEntradas().add(detalleDevolucionEntrada);
		detalleDevolucionEntrada.setDevolucionEntrada(this);

		return detalleDevolucionEntrada;
	}

	public DetalleDevolucionEntrada removeDetalleDevolucionEntrada(DetalleDevolucionEntrada detalleDevolucionEntrada) {
		getDetalleDevolucionEntradas().remove(detalleDevolucionEntrada);
		detalleDevolucionEntrada.setDevolucionEntrada(null);

		return detalleDevolucionEntrada;
	}

	public Entrada getEntrada() {
		return this.entrada;
	}

	public void setEntrada(Entrada entrada) {
		this.entrada = entrada;
	}

	public SegUsuario getSegUsuario() {
		return this.segUsuario;
	}

	public void setSegUsuario(SegUsuario segUsuario) {
		this.segUsuario = segUsuario;
	}

}