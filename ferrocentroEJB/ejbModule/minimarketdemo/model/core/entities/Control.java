package minimarketdemo.model.core.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the control database table.
 * 
 */
@Entity
@Table(name="control")
@NamedQuery(name="Control.findAll", query="SELECT c FROM Control c")
public class Control implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_control", unique=true, nullable=false)
	private Integer idControl;

	@Column(nullable=false)
	private Boolean estado;

	@Temporal(TemporalType.DATE)
	@Column(nullable=false)
	private Date fecha;

	@Column(nullable=false, length=2147483647)
	private String observacion;

	//bi-directional many-to-one association to SegUsuario
	@ManyToOne
	@JoinColumn(name="id_seg_usuario", nullable=false)
	private SegUsuario segUsuario;

	//bi-directional many-to-one association to DetalleControl
	@OneToMany(mappedBy="control")
	private List<DetalleControl> detalleControls;

	public Control() {
	}

	public Integer getIdControl() {
		return this.idControl;
	}

	public void setIdControl(Integer idControl) {
		this.idControl = idControl;
	}

	public Boolean getEstado() {
		return this.estado;
	}

	public void setEstado(Boolean estado) {
		this.estado = estado;
	}

	public Date getFecha() {
		return this.fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public String getObservacion() {
		return this.observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	public SegUsuario getSegUsuario() {
		return this.segUsuario;
	}

	public void setSegUsuario(SegUsuario segUsuario) {
		this.segUsuario = segUsuario;
	}

	public List<DetalleControl> getDetalleControls() {
		return this.detalleControls;
	}

	public void setDetalleControls(List<DetalleControl> detalleControls) {
		this.detalleControls = detalleControls;
	}

	public DetalleControl addDetalleControl(DetalleControl detalleControl) {
		getDetalleControls().add(detalleControl);
		detalleControl.setControl(this);

		return detalleControl;
	}

	public DetalleControl removeDetalleControl(DetalleControl detalleControl) {
		getDetalleControls().remove(detalleControl);
		detalleControl.setControl(null);

		return detalleControl;
	}

}