package minimarketdemo.model.core.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the detalle_entrada database table.
 * 
 */
@Entity
@Table(name="detalle_entrada")
@NamedQuery(name="DetalleEntrada.findAll", query="SELECT d FROM DetalleEntrada d")
public class DetalleEntrada implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_detalle_entrada", unique=true, nullable=false)
	private Integer idDetalleEntrada;

	@Column(nullable=false)
	private Integer cantidad;

	@Column(nullable=false, precision=10, scale=2)
	private BigDecimal precio;

	//bi-directional many-to-one association to Entrada
	@ManyToOne
	@JoinColumn(name="id_entrada", nullable=false)
	private Entrada entrada;

	//bi-directional many-to-one association to Producto
	@ManyToOne
	@JoinColumn(name="id_producto", nullable=false)
	private Producto producto;

	public DetalleEntrada() {
	}

	public Integer getIdDetalleEntrada() {
		return this.idDetalleEntrada;
	}

	public void setIdDetalleEntrada(Integer idDetalleEntrada) {
		this.idDetalleEntrada = idDetalleEntrada;
	}

	public Integer getCantidad() {
		return this.cantidad;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}

	public BigDecimal getPrecio() {
		return this.precio;
	}

	public void setPrecio(BigDecimal precio) {
		this.precio = precio;
	}

	public Entrada getEntrada() {
		return this.entrada;
	}

	public void setEntrada(Entrada entrada) {
		this.entrada = entrada;
	}

	public Producto getProducto() {
		return this.producto;
	}

	public void setProducto(Producto producto) {
		this.producto = producto;
	}

}