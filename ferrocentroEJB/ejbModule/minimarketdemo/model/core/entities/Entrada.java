package minimarketdemo.model.core.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the entrada database table.
 * 
 */
@Entity
@Table(name="entrada")
@NamedQuery(name="Entrada.findAll", query="SELECT e FROM Entrada e")
public class Entrada implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_entrada", unique=true, nullable=false)
	private Integer idEntrada;

	@Column(name="deuda_parcial", nullable=false, precision=10, scale=2)
	private BigDecimal deudaParcial;

	@Column(name="entrada_devuelta", nullable=false)
	private Boolean entradaDevuelta;

	@Column(nullable=false)
	private Boolean estado;

	@Column(name="estado_entrada", nullable=false, length=1)
	private String estadoEntrada;

	@Temporal(TemporalType.DATE)
	@Column(nullable=false)
	private Date fecha;

	@Temporal(TemporalType.DATE)
	@Column(name="fecha_pago", nullable=false)
	private Date fechaPago;

	@Column(nullable=false, precision=10, scale=2)
	private BigDecimal impuesto;

	@Column(name="num_entrada", nullable=false, length=20)
	private String numEntrada;

	@Column(nullable=false, precision=10, scale=2)
	private BigDecimal total;

	//bi-directional many-to-one association to DetalleEntrada
	@OneToMany(mappedBy = "entrada", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private List<DetalleEntrada> detalleEntradas;

	//bi-directional many-to-one association to DevolucionEntrada
	@OneToMany(mappedBy="entrada")
	private List<DevolucionEntrada> devolucionEntradas;

	//bi-directional many-to-one association to MetodoPago
	@ManyToOne
	@JoinColumn(name="id_metodo_pago", nullable=false)
	private MetodoPago metodoPago;

	//bi-directional many-to-one association to Proveedor
	@ManyToOne
	@JoinColumn(name="id_proveedor", nullable=false)
	private Proveedor proveedor;

	//bi-directional many-to-one association to SegUsuario
	@ManyToOne
	@JoinColumn(name="id_seg_usuario", nullable=false)
	private SegUsuario segUsuario;

	//bi-directional many-to-one association to TipoPago
	@ManyToOne
	@JoinColumn(name="id_tipo_pago", nullable=false)
	private TipoPago tipoPago;

	public Entrada() {
	}

	public Integer getIdEntrada() {
		return this.idEntrada;
	}

	public void setIdEntrada(Integer idEntrada) {
		this.idEntrada = idEntrada;
	}

	public BigDecimal getDeudaParcial() {
		return this.deudaParcial;
	}

	public void setDeudaParcial(BigDecimal deudaParcial) {
		this.deudaParcial = deudaParcial;
	}

	public Boolean getEntradaDevuelta() {
		return this.entradaDevuelta;
	}

	public void setEntradaDevuelta(Boolean entradaDevuelta) {
		this.entradaDevuelta = entradaDevuelta;
	}

	public Boolean getEstado() {
		return this.estado;
	}

	public void setEstado(Boolean estado) {
		this.estado = estado;
	}

	public String getEstadoEntrada() {
		return this.estadoEntrada;
	}

	public void setEstadoEntrada(String estadoEntrada) {
		this.estadoEntrada = estadoEntrada;
	}

	public Date getFecha() {
		return this.fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public Date getFechaPago() {
		return this.fechaPago;
	}

	public void setFechaPago(Date fechaPago) {
		this.fechaPago = fechaPago;
	}

	public BigDecimal getImpuesto() {
		return this.impuesto;
	}

	public void setImpuesto(BigDecimal impuesto) {
		this.impuesto = impuesto;
	}

	public String getNumEntrada() {
		return this.numEntrada;
	}

	public void setNumEntrada(String numEntrada) {
		this.numEntrada = numEntrada;
	}

	public BigDecimal getTotal() {
		return this.total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public List<DetalleEntrada> getDetalleEntradas() {
		return this.detalleEntradas;
	}

	public void setDetalleEntradas(List<DetalleEntrada> detalleEntradas) {
		this.detalleEntradas = detalleEntradas;
	}

	public DetalleEntrada addDetalleEntrada(DetalleEntrada detalleEntrada) {
		getDetalleEntradas().add(detalleEntrada);
		detalleEntrada.setEntrada(this);

		return detalleEntrada;
	}

	public DetalleEntrada removeDetalleEntrada(DetalleEntrada detalleEntrada) {
		getDetalleEntradas().remove(detalleEntrada);
		detalleEntrada.setEntrada(null);

		return detalleEntrada;
	}

	public List<DevolucionEntrada> getDevolucionEntradas() {
		return this.devolucionEntradas;
	}

	public void setDevolucionEntradas(List<DevolucionEntrada> devolucionEntradas) {
		this.devolucionEntradas = devolucionEntradas;
	}

	public DevolucionEntrada addDevolucionEntrada(DevolucionEntrada devolucionEntrada) {
		getDevolucionEntradas().add(devolucionEntrada);
		devolucionEntrada.setEntrada(this);

		return devolucionEntrada;
	}

	public DevolucionEntrada removeDevolucionEntrada(DevolucionEntrada devolucionEntrada) {
		getDevolucionEntradas().remove(devolucionEntrada);
		devolucionEntrada.setEntrada(null);

		return devolucionEntrada;
	}

	public MetodoPago getMetodoPago() {
		return this.metodoPago;
	}

	public void setMetodoPago(MetodoPago metodoPago) {
		this.metodoPago = metodoPago;
	}

	public Proveedor getProveedor() {
		return this.proveedor;
	}

	public void setProveedor(Proveedor proveedor) {
		this.proveedor = proveedor;
	}

	public SegUsuario getSegUsuario() {
		return this.segUsuario;
	}

	public void setSegUsuario(SegUsuario segUsuario) {
		this.segUsuario = segUsuario;
	}

	public TipoPago getTipoPago() {
		return this.tipoPago;
	}

	public void setTipoPago(TipoPago tipoPago) {
		this.tipoPago = tipoPago;
	}

}