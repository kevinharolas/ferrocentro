package minimarketdemo.model.core.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the detalle_proforma database table.
 * 
 */
@Entity
@Table(name="detalle_proforma")
@NamedQuery(name="DetalleProforma.findAll", query="SELECT d FROM DetalleProforma d")
public class DetalleProforma implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_detalle_proforma", unique=true, nullable=false)
	private Integer idDetalleProforma;

	@Column(nullable=false)
	private Integer cantidad;

	@Column(nullable=false, precision=10, scale=2)
	private BigDecimal precio;

	//bi-directional many-to-one association to Producto
	@ManyToOne
	@JoinColumn(name="id_producto", nullable=false)
	private Producto producto;

	//bi-directional many-to-one association to Proforma
	@ManyToOne
	@JoinColumn(name="id_proforma", nullable=false)
	private Proforma proforma;

	public DetalleProforma() {
	}

	public Integer getIdDetalleProforma() {
		return this.idDetalleProforma;
	}

	public void setIdDetalleProforma(Integer idDetalleProforma) {
		this.idDetalleProforma = idDetalleProforma;
	}

	public Integer getCantidad() {
		return this.cantidad;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}

	public BigDecimal getPrecio() {
		return this.precio;
	}

	public void setPrecio(BigDecimal precio) {
		this.precio = precio;
	}

	public Producto getProducto() {
		return this.producto;
	}

	public void setProducto(Producto producto) {
		this.producto = producto;
	}

	public Proforma getProforma() {
		return this.proforma;
	}

	public void setProforma(Proforma proforma) {
		this.proforma = proforma;
	}

}