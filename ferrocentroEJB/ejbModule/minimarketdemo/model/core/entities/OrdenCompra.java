package minimarketdemo.model.core.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the orden_compra database table.
 * 
 */
@Entity
@Table(name="orden_compra")
@NamedQuery(name="OrdenCompra.findAll", query="SELECT o FROM OrdenCompra o")
public class OrdenCompra implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_orden_compra", unique=true, nullable=false)
	private Integer idOrdenCompra;

	@Column(nullable=false)
	private Boolean estado;

	@Temporal(TemporalType.DATE)
	@Column(nullable=false)
	private Date fecha;

	@Temporal(TemporalType.DATE)
	@Column(name="fecha_vencimiento", nullable=false)
	private Date fechaVencimiento;

	@Column(nullable=false, precision=10, scale=2)
	private BigDecimal impuesto;

	@Column(name="num_orden_compra", nullable=false, length=20)
	private String numOrdenCompra;

	@Column(nullable=false, precision=10, scale=2)
	private BigDecimal total;

	//bi-directional many-to-one association to DetalleOrdenCompra
	@OneToMany(mappedBy="ordenCompra")
	private List<DetalleOrdenCompra> detalleOrdenCompras;

	//bi-directional many-to-one association to Proveedor
	@ManyToOne
	@JoinColumn(name="id_proveedor", nullable=false)
	private Proveedor proveedor;

	//bi-directional many-to-one association to SegUsuario
	@ManyToOne
	@JoinColumn(name="id_seg_usuario", nullable=false)
	private SegUsuario segUsuario;

	public OrdenCompra() {
	}

	public Integer getIdOrdenCompra() {
		return this.idOrdenCompra;
	}

	public void setIdOrdenCompra(Integer idOrdenCompra) {
		this.idOrdenCompra = idOrdenCompra;
	}

	public Boolean getEstado() {
		return this.estado;
	}

	public void setEstado(Boolean estado) {
		this.estado = estado;
	}

	public Date getFecha() {
		return this.fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public Date getFechaVencimiento() {
		return this.fechaVencimiento;
	}

	public void setFechaVencimiento(Date fechaVencimiento) {
		this.fechaVencimiento = fechaVencimiento;
	}

	public BigDecimal getImpuesto() {
		return this.impuesto;
	}

	public void setImpuesto(BigDecimal impuesto) {
		this.impuesto = impuesto;
	}

	public String getNumOrdenCompra() {
		return this.numOrdenCompra;
	}

	public void setNumOrdenCompra(String numOrdenCompra) {
		this.numOrdenCompra = numOrdenCompra;
	}

	public BigDecimal getTotal() {
		return this.total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public List<DetalleOrdenCompra> getDetalleOrdenCompras() {
		return this.detalleOrdenCompras;
	}

	public void setDetalleOrdenCompras(List<DetalleOrdenCompra> detalleOrdenCompras) {
		this.detalleOrdenCompras = detalleOrdenCompras;
	}

	public DetalleOrdenCompra addDetalleOrdenCompra(DetalleOrdenCompra detalleOrdenCompra) {
		getDetalleOrdenCompras().add(detalleOrdenCompra);
		detalleOrdenCompra.setOrdenCompra(this);

		return detalleOrdenCompra;
	}

	public DetalleOrdenCompra removeDetalleOrdenCompra(DetalleOrdenCompra detalleOrdenCompra) {
		getDetalleOrdenCompras().remove(detalleOrdenCompra);
		detalleOrdenCompra.setOrdenCompra(null);

		return detalleOrdenCompra;
	}

	public Proveedor getProveedor() {
		return this.proveedor;
	}

	public void setProveedor(Proveedor proveedor) {
		this.proveedor = proveedor;
	}

	public SegUsuario getSegUsuario() {
		return this.segUsuario;
	}

	public void setSegUsuario(SegUsuario segUsuario) {
		this.segUsuario = segUsuario;
	}

}