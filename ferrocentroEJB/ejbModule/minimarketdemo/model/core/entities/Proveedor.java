package minimarketdemo.model.core.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the proveedor database table.
 * 
 */
@Entity
@Table(name="proveedor")
@NamedQuery(name="Proveedor.findAll", query="SELECT p FROM Proveedor p")
@NamedQuery(name = "Proveedor.findByIdentificacion", query = "SELECT p FROM Proveedor p WHERE p.identificacion=:identificacion")
public class Proveedor implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_proveedor", unique=true, nullable=false)
	private Integer idProveedor;

	@Column(nullable=false, length=50)
	private String apellido;

	@Column(nullable=false, length=50)
	private String ciudad;

	@Column(nullable=false, length=30)
	private String correo;

	@Column(nullable=false, length=2147483647)
	private String direccionn;

	@Column(nullable=false)
	private Boolean estado;

	@Column(nullable=false, length=13)
	private String identificacion;

	@Column(nullable=false, length=50)
	private String nombre;

	@Column(name="nombre_comercial", nullable=false, length=50)
	private String nombreComercial;

	@Column(nullable=false, length=2147483647)
	private String telefono;

	//bi-directional many-to-one association to Entrada
	@OneToMany(mappedBy="proveedor")
	private List<Entrada> entradas;

	//bi-directional many-to-one association to OrdenCompra
	@OneToMany(mappedBy="proveedor")
	private List<OrdenCompra> ordenCompras;

	//bi-directional many-to-one association to SegUsuario
	@ManyToOne
	@JoinColumn(name="id_seg_usuario", nullable=false)
	private SegUsuario segUsuario;

	public Proveedor() {
	}

	public Integer getIdProveedor() {
		return this.idProveedor;
	}

	public void setIdProveedor(Integer idProveedor) {
		this.idProveedor = idProveedor;
	}

	public String getApellido() {
		return this.apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getCiudad() {
		return this.ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public String getCorreo() {
		return this.correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getDireccionn() {
		return this.direccionn;
	}

	public void setDireccionn(String direccionn) {
		this.direccionn = direccionn;
	}

	public Boolean getEstado() {
		return this.estado;
	}

	public void setEstado(Boolean estado) {
		this.estado = estado;
	}

	public String getIdentificacion() {
		return this.identificacion;
	}

	public void setIdentificacion(String identificacion) {
		this.identificacion = identificacion;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getNombreComercial() {
		return this.nombreComercial;
	}

	public void setNombreComercial(String nombreComercial) {
		this.nombreComercial = nombreComercial;
	}

	public String getTelefono() {
		return this.telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public List<Entrada> getEntradas() {
		return this.entradas;
	}

	public void setEntradas(List<Entrada> entradas) {
		this.entradas = entradas;
	}

	public Entrada addEntrada(Entrada entrada) {
		getEntradas().add(entrada);
		entrada.setProveedor(this);

		return entrada;
	}

	public Entrada removeEntrada(Entrada entrada) {
		getEntradas().remove(entrada);
		entrada.setProveedor(null);

		return entrada;
	}

	public List<OrdenCompra> getOrdenCompras() {
		return this.ordenCompras;
	}

	public void setOrdenCompras(List<OrdenCompra> ordenCompras) {
		this.ordenCompras = ordenCompras;
	}

	public OrdenCompra addOrdenCompra(OrdenCompra ordenCompra) {
		getOrdenCompras().add(ordenCompra);
		ordenCompra.setProveedor(this);

		return ordenCompra;
	}

	public OrdenCompra removeOrdenCompra(OrdenCompra ordenCompra) {
		getOrdenCompras().remove(ordenCompra);
		ordenCompra.setProveedor(null);

		return ordenCompra;
	}

	public SegUsuario getSegUsuario() {
		return this.segUsuario;
	}

	public void setSegUsuario(SegUsuario segUsuario) {
		this.segUsuario = segUsuario;
	}

}