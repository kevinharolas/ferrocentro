package minimarketdemo.model.core.managers;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NamedQuery;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import minimarketdemo.model.core.entities.Cliente;
import minimarketdemo.model.core.entities.DevolucionVenta;
import minimarketdemo.model.core.entities.Marca;
import minimarketdemo.model.core.entities.Producto;
import minimarketdemo.model.core.entities.SegUsuario;
import minimarketdemo.model.core.entities.UnidadMedida;
import minimarketdemo.model.core.utils.ModelUtil;

/**
 * Objeto que encapsula la logica basica de acceso a datos mediante JPA.
 * 
 * @author mrea
 */
@Stateless
@LocalBean
public class ManagerDAO {

	@PersistenceContext
	private EntityManager em;

	/**
	 * Default constructor.
	 */
	public ManagerDAO() {

	}

	public List findAll(Class clase, String propiedadOrderBy, boolean ascendente) {
		Query q;
		List listado;
		String sentenciaJPQL;
		if (propiedadOrderBy == null || propiedadOrderBy.length() == 0)
			sentenciaJPQL = "SELECT o FROM " + clase.getSimpleName() + " o";
		else {
			if (ascendente)
				sentenciaJPQL = "SELECT o FROM " + clase.getSimpleName() + " o ORDER BY o." + propiedadOrderBy + " asc";
			else
				sentenciaJPQL = "SELECT o FROM " + clase.getSimpleName() + " o ORDER BY o." + propiedadOrderBy
						+ " desc";
		}
		q = em.createQuery(sentenciaJPQL);
		listado = q.getResultList();
		System.out.println("Consultados " + clase.getSimpleName() + ": " + listado.size() + " objetos.");
		return listado;

	}

	public List findAll(Class clase) {
		return findAll(clase, null, false);
	}

	@SuppressWarnings("rawtypes")
	public List findAll(Class clase, String propiedadOrderBy) {
		return findAll(clase, propiedadOrderBy, true);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Object findById(Class clase, Object pID) throws Exception {
		if (pID == null)
			throw new Exception("Debe especificar el codigo para buscar el dato.");
		Object o;
		try {
			o = em.find(clase, pID);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error al buscar la informacion especificada (" + pID + ") : " + e.getMessage());
		}
		return o;
	}

	@SuppressWarnings("rawtypes")
	public List findWhere(Class clase, String pClausulaWhere) {
		Query q;
		List listado;
		String sentenciaJPQL;
		sentenciaJPQL = "SELECT o FROM " + clase.getSimpleName() + " o WHERE " + pClausulaWhere;

		q = em.createQuery(sentenciaJPQL);
		listado = q.getResultList();
		return listado;
	}

	public void insertar(Object pObjeto) throws Exception {
		if (pObjeto == null)
			throw new Exception("No se puede insertar un objeto null.");
		try {
			em.persist(pObjeto);
		} catch (Exception e) {
			throw new Exception("No se pudo insertar el objeto especificado: " + e.getMessage());
		}
	}

	@SuppressWarnings("rawtypes")
	public void eliminar(Class clase, Object pID) throws Exception {
		if (pID == null) {
			throw new Exception("Debe especificar un identificador para eliminar el dato solicitado.");
		}
		Object o = findById(clase, pID);
		try {
			em.remove(o);
		} catch (Exception e) {
			throw new Exception("No se pudo eliminar el dato: " + e.getMessage());
		}
	}

	public void actualizar(Object pObjeto) throws Exception {
		if (pObjeto == null)
			throw new Exception("No se puede actualizar un dato null");
		try {
			em.merge(pObjeto);
		} catch (Exception e) {
			throw new Exception("No se pudo actualizar el dato: " + e.getMessage());
		}
	}

	public EntityManager getEntityManager() {
		return em;
	}

	@SuppressWarnings("rawtypes")
	public List execJPQL(String pClausulaJPQL) {
		Query q;
		List listado;
		q = em.createQuery(pClausulaJPQL);
		listado = q.getResultList();

		return listado;
	}

	public Long obtenerSecuenciaPostgresql(String nombreSecuencia) throws Exception {
		String sentenciaSQL;
		Query q;
		BigInteger valSeq;
		Long valorSeq = null;
		try {
			sentenciaSQL = "SELECT nextval('" + nombreSecuencia + "')";
			q = em.createNativeQuery(sentenciaSQL);
			valSeq = (BigInteger) q.getSingleResult();
			valorSeq = valSeq.longValue();
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error al obtener valor de secuencia (" + nombreSecuencia + ") : " + e.getMessage());
		}
		return valorSeq;
	}

	@SuppressWarnings("rawtypes")
	public long obtenerValorMax(Class clase, String nombrePropiedad) throws Exception {
		Query q;
		String sentenciaSQL;
		BigDecimal valorMax;
		try {
			sentenciaSQL = "SELECT MAX(o." + nombrePropiedad + ") FROM " + clase.getSimpleName() + " o";
			q = em.createQuery(sentenciaSQL);
			valorMax = (BigDecimal) q.getSingleResult();
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error al obtener valor max: " + clase.getCanonicalName() + "[" + nombrePropiedad
					+ "]. " + e.getMessage());
		}
		if (valorMax == null)
			return 0;
		return valorMax.longValue();
	}

	@SuppressWarnings("rawtypes")
	public long obtenerValorMaxWhere(Class clase, String nombrePropiedad, String pClausulaWhere) throws Exception {
		Query q;
		String sentenciaSQL;
		BigDecimal valorMax;
		try {
			sentenciaSQL = "SELECT MAX(o." + nombrePropiedad + ") FROM " + clase.getSimpleName() + " o" + " WHERE "
					+ pClausulaWhere;
			q = em.createQuery(sentenciaSQL);
			valorMax = (BigDecimal) q.getSingleResult();
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error al obtener valor max: " + clase.getCanonicalName() + "[" + nombrePropiedad
					+ "]. " + pClausulaWhere + e.getMessage());
		}
		if (valorMax == null)
			return 0;
		return valorMax.longValue();
	}

	// MetodosExtras
	/* Busqueda de usuario por correo */
	@SuppressWarnings("rawtypes")
	public SegUsuario findWhereCorreo(Class clase, String correo) throws Exception {
		// @NamedQuery(name="SegUsuario.findByCorreo", query="SELECT s FROM SegUsuario s
		// WHERE s.correo=:correo")
		if (correo == null)
			throw new Exception("Debe especificar el correo para buscar el dato.");
		SegUsuario user;
		try {
			user = (SegUsuario) em.createNamedQuery("SegUsuario.findByCorreo").setParameter("correo", correo)
					.getSingleResult();

		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error al buscar la informacion especificada (" + correo + ") : " + e.getMessage());
		}
		return user;
	}

	public List findFacturaByRangoFecha(Class clase, String sql, Date fechaInicio, Date fechaFin) {
		Query q;
		List listado;
		String sentenciaJPQL;
		sentenciaJPQL = sql;
		q = em.createQuery(sentenciaJPQL, clase);
		q.setParameter("fechaInicio", new Timestamp(fechaInicio.getTime()));
		q.setParameter("fechaFin", new Timestamp(fechaFin.getTime()));
		listado = q.getResultList();

		return listado;
	}

	public List findFacturaByCliente(Class clase, int idCliente) {
		Query q;
		List listado;
		String sentenciaJPQL = "SELECT * FROM Venta v INNER JOIN Cliente c on c.id_cliente=v.id_cliente WHERE v.estado=true AND c.id_cliente="
				+ idCliente;

		q = em.createNativeQuery(sentenciaJPQL, clase);
		listado = q.getResultList();
		return listado;
	}

	
	
	
	// Filtrar: Clientes, Producto

	@SuppressWarnings("rawtypes")
	public Object findWhithQueryName(Class clase, String sentence, String parameter, String descripcion)
			throws Exception {
//		 @NamedQuery(name="Producto.findByDescripcion", query="SELECT p FROM Producto p WHERE p.descripcion=:descripcion")
//		 @NamedQuery(name="Cliente.findByIdentificacion", query="SELECT c FROM Cliente c WHERE c.identificacion=:identificacion")
		if (descripcion == null)
			throw new Exception("Debe especificar la descripcion para buscar el dato.");
		Object pro;
		try {
			pro = em.createNamedQuery(sentence).setParameter(parameter, descripcion).getSingleResult();

		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(
					"Error al buscar la informacion especificada (" + descripcion + ") : " + e.getMessage());
		}
		return pro;
	}

	@SuppressWarnings("rawtypes")
	public long count(Class clase, String nombrePropiedad) throws Exception {
		Query q;
		String sentenciaSQL;
		long count;
		try {
			sentenciaSQL = "SELECT COUNT(o." + nombrePropiedad + ") FROM " + clase.getSimpleName() + " o";
			q = em.createQuery(sentenciaSQL);
			count = (long) q.getSingleResult();
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error al obtener valor max: " + clase.getCanonicalName() + "[" + nombrePropiedad
					+ "]. " + e.getMessage());
		}
		return count;
	}

	public List findAllListaDevoluciones(Class clase, String pClausulaWhere) {
		Query q;
		String sentenciaJPQL;
		sentenciaJPQL = "SELECT o FROM " + clase.getSimpleName() + " o WHERE " + pClausulaWhere;

		q = em.createQuery(sentenciaJPQL);

		List<DevolucionVenta> listDev = q.getResultList();

		for (DevolucionVenta d : listDev) {
			if (d.getDetalleDevolucionVentas() != null) {
				d.getDetalleDevolucionVentas().size();
			}
		}
		return listDev;
	}
	
	// Aumento EDISON P.
	
	public List findFacturaByProveedor(Class clase, int idProveedor) {
		Query q;
		List listado;
		String sentenciaJPQL = "SELECT * FROM Entrada v INNER JOIN Proveedor c on c.id_proveedor=v.id_proveedor WHERE v.estado=true AND c.id_proveedor="
				+ idProveedor;

		q = em.createNativeQuery(sentenciaJPQL, clase);
		listado = q.getResultList();
		return listado;
	}

	public List findSubcategoriaByCategoria(Class clase, int idCategoria) {
		Query q;
		List listado;
		String sentenciaJPQL;
		sentenciaJPQL = "SELECT o FROM " + clase.getSimpleName() + " o WHERE o.categoria.idCategoria=" + idCategoria;

		q = em.createQuery(sentenciaJPQL);
		listado = q.getResultList();
		return listado;
	}

	public List findClasificacionBySubcategoria(Class clase, int idSubcategoria) {
		Query q;
		List listado;
		String sentenciaJPQL;
		sentenciaJPQL = "SELECT o FROM " + clase.getSimpleName() + " o WHERE o.subcategoria.idSubcategoria="
				+ idSubcategoria;
		q = em.createQuery(sentenciaJPQL);
		listado = q.getResultList();
		return listado;
	}
	

}
