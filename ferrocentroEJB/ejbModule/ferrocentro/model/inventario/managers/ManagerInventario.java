package ferrocentro.model.inventario.managers;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import minimarketdemo.model.auditoria.managers.ManagerAuditoria;
import minimarketdemo.model.core.entities.DevolucionVenta;
import minimarketdemo.model.core.entities.Venta;
import minimarketdemo.model.core.managers.ManagerDAO;


@Stateless
@LocalBean
public class ManagerInventario {
	@EJB
	private ManagerDAO mDAO;
	@EJB
	private ManagerAuditoria mAuditoria;
	
    public ManagerInventario() {
        // TODO Auto-generated constructor stub
    }

    
    public List<DevolucionVenta> findAllDevolucionesTotales(){
      	 String sentenciaJPQL = "o.estado=true AND o.tipoDevolucion='T'";
      	 return mDAO.findAllListaDevoluciones(DevolucionVenta.class,sentenciaJPQL);
      }
    
    public List<DevolucionVenta> findAllDevolucionesParciales(){
      	 String sentenciaJPQL = "o.estado=true AND o.tipoDevolucion='P'";
      	 return mDAO.findAllListaDevoluciones(DevolucionVenta.class,sentenciaJPQL);
      }
}
