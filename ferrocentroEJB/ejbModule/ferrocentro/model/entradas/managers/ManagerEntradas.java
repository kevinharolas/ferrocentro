package ferrocentro.model.entradas.managers;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import minimarketdemo.model.auditoria.managers.ManagerAuditoria;
import minimarketdemo.model.core.entities.Categoria;
import minimarketdemo.model.core.entities.Clasificacion;
import minimarketdemo.model.core.entities.DetalleDevolucionEntrada;
import minimarketdemo.model.core.entities.DetalleEntrada;
import minimarketdemo.model.core.entities.DetalleOrdenCompra;
import minimarketdemo.model.core.entities.DevolucionEntrada;
import minimarketdemo.model.core.entities.Entrada;
import minimarketdemo.model.core.entities.ListaPrecio;
import minimarketdemo.model.core.entities.Marca;
import minimarketdemo.model.core.entities.MetodoPago;
import minimarketdemo.model.core.entities.OrdenCompra;
import minimarketdemo.model.core.entities.Producto;
import minimarketdemo.model.core.entities.Proforma;
import minimarketdemo.model.core.entities.Proveedor;
import minimarketdemo.model.core.entities.SegUsuario;
import minimarketdemo.model.core.entities.Subcategoria;
import minimarketdemo.model.core.entities.TipoPago;
import minimarketdemo.model.core.entities.TipoProducto;
import minimarketdemo.model.core.entities.UnidadMedida;
import minimarketdemo.model.core.entities.Venta;
import minimarketdemo.model.core.managers.ManagerDAO;
import minimarketdemo.model.seguridades.dtos.LoginDTO;

/**
 * Session Bean implementation class ManagerEntradas
 */
@Stateless
@LocalBean
public class ManagerEntradas {
	@EJB
	private ManagerDAO mDAO;
	@EJB
	private ManagerAuditoria mAuditoria;

	/**
	 * Default constructor.
	 */
	public ManagerEntradas() {
		// TODO Auto-generated constructor stub

	}

	public List<OrdenCompra> allOrdenCompras() {
		String sentenciaJPQL = "o.estado=true";
		return mDAO.findWhere(OrdenCompra.class, sentenciaJPQL);
	}

	public List<TipoPago> allTipoPagos() {
		return mDAO.findAll(TipoPago.class);
	}

	public List<MetodoPago> allMetodoPagos() {
		return mDAO.findAll(MetodoPago.class);
	}

	public List<OrdenCompra> allProformas() {
		String sentenciaJPQL = "o.estado=true";
		return mDAO.findWhere(OrdenCompra.class, sentenciaJPQL);
	}

	public List<Entrada> findFacturaDevolverByEstadoDevolver(boolean devuelto) {
		String sentenciaJPQL = "o.estado=true AND o.entradaDevuelta=" + false;
		return mDAO.findWhere(Entrada.class, sentenciaJPQL);

	}

	public List<Entrada> findFacturaByEstadoEntrada() {
		String sentenciaJPQL = "o.estado=true AND o.estadoEntrada='P'";
		return mDAO.findWhere(Entrada.class, sentenciaJPQL);
	}

	public List<Entrada> findFacturaByFechas(Date fechaInicio, Date fechaFin) throws Exception {
		String sql = "SELECT v from Entrada v WHERE v.fecha BETWEEN :fechaInicio AND :fechaFin AND v.estado=true ORDER BY v.fecha";
		return mDAO.findFacturaByRangoFecha(Entrada.class, sql, fechaInicio, fechaFin);
	}

	public List<OrdenCompra> findProformaByFechas(Date fechaInicio, Date fechaFin) throws Exception {
		String sql = "SELECT v from OrdenCompra v WHERE v.fecha BETWEEN :fechaInicio AND :fechaFin AND v.estado=true ORDER BY v.fecha";
		return mDAO.findFacturaByRangoFecha(OrdenCompra.class, sql, fechaInicio, fechaFin);
	}

	public List<Entrada> findFacturaByProveedor(int idProveedor) {
		return mDAO.findFacturaByProveedor(Entrada.class, idProveedor);
	}

	public Proveedor findOneProveedor(String identificacionProveedor) throws Exception {
		return (Proveedor) mDAO.findWhithQueryName(Proveedor.class, "Proveedor.findByIdentificacion", "identificacion",
				identificacionProveedor);
	}

	public Producto findOneProducto(String descripcionProducto) throws Exception {
		return (Producto) mDAO.findWhithQueryName(Producto.class, "Producto.findByDescripcion", "descripcion",
				descripcionProducto);
	}

	/* ============================================================== */
	/* PROVEEDOR */
	/* ============================================================== */

	public List<Proveedor> allProveedors() {
		String sentenciaJPQL = "o.estado=true";
		return mDAO.findWhere(Proveedor.class, sentenciaJPQL);
	}

	public void insertarProveedor(LoginDTO loginDTO, Proveedor nuevoProveedor) throws Exception {
		if (nuevoProveedor == null)
			throw new Exception("Debe ingresar los datos del proveedor");
		SegUsuario usuario = (SegUsuario) mDAO.findById(SegUsuario.class, loginDTO.getIdSegUsuario());
		nuevoProveedor.setSegUsuario(usuario);
		nuevoProveedor.setEstado(true);
		mDAO.insertar(nuevoProveedor);
		mAuditoria.mostrarLog(loginDTO, getClass(), "Proveedor insertado",
				"Proveedor: " + nuevoProveedor.getCorreo() + " insertado");
	}

	public void eliminarProveedor(Proveedor pro) throws Exception {
		Proveedor proveedor = (Proveedor) mDAO.findById(Proveedor.class, pro.getIdProveedor());
		proveedor.setEstado(false);
		mDAO.actualizar(proveedor);
		mAuditoria.mostrarLog(getClass(), "Proveedor eliminado", "Proveedor: " + proveedor.getCorreo() + " eliminado");
	}

	public void actualizarProveedor(Proveedor pro) throws Exception {
		Proveedor proveedor = (Proveedor) mDAO.findById(Proveedor.class, pro.getIdProveedor());
		proveedor.setApellido(pro.getApellido());
		proveedor.setNombre(pro.getNombre());
		proveedor.setNombreComercial(pro.getNombreComercial());
		proveedor.setCiudad(pro.getNombreComercial());
		proveedor.setDireccionn(pro.getDireccionn());
		proveedor.setTelefono(pro.getTelefono());
		proveedor.setCorreo(pro.getCorreo());
		mDAO.actualizar(proveedor);
		mAuditoria.mostrarLog(getClass(), "Proveedor actualizado",
				"Proveedor: " + proveedor.getCorreo() + " actualizado");
	}

	/* ============================================================== */
	/* UNIDAD_MEDIDA */
	/* ============================================================== */
	public List<UnidadMedida> allUnidadM() {
		String sentenciaJPQL = "o.estado=true";
		return mDAO.findWhere(UnidadMedida.class, sentenciaJPQL);
	}

	public void insertarUnidadM(LoginDTO loginDTO, UnidadMedida nuevaUnidadM) throws Exception {
		if (nuevaUnidadM == null)
			throw new Exception("Debe ingresar los datos del proveedor");
		SegUsuario usuario = (SegUsuario) mDAO.findById(SegUsuario.class, loginDTO.getIdSegUsuario());
		nuevaUnidadM.setSegUsuario(usuario);
		nuevaUnidadM.setEstado(true);
		mDAO.insertar(nuevaUnidadM);
		mAuditoria.mostrarLog(loginDTO, getClass(), "Proveedor insertado",
				"Proveedor: " + nuevaUnidadM.getDescripcion() + " insertado");
	}

	public void eliminarUnidadM(UnidadMedida unid) throws Exception {
		UnidadMedida unidad = (UnidadMedida) mDAO.findById(UnidadMedida.class, unid.getIdUnidadMedida());
		unidad.setEstado(false);
		mDAO.actualizar(unidad);
		mAuditoria.mostrarLog(getClass(), "Unidad eliminado", "Unidad: " + unidad.getDescripcion() + " eliminado");
	}

	public void actualizarUnidadM(UnidadMedida unid) throws Exception {
		UnidadMedida unidad = (UnidadMedida) mDAO.findById(UnidadMedida.class, unid.getIdUnidadMedida());
		unidad.setDescripcion(unid.getDescripcion());
		mDAO.actualizar(unidad);
		mAuditoria.mostrarLog(getClass(), "Unidad actualizado", "Unidad: " + unidad.getDescripcion() + " actualizado");
	}

	/* ============================================================== */
	/* MARCA */
	/* ============================================================== */
	public List<Marca> allMarcas() {
		String sentenciaJPQL = "o.estado=true";
		return mDAO.findWhere(Marca.class, sentenciaJPQL);
	}

	public void insertarMarca(LoginDTO loginDTO, Marca nuevaMarca) throws Exception {
		if (nuevaMarca == null)
			throw new Exception("Debe ingresar los datos del marca");
		SegUsuario usuario = (SegUsuario) mDAO.findById(SegUsuario.class, loginDTO.getIdSegUsuario());
		nuevaMarca.setSegUsuario(usuario);
		nuevaMarca.setEstado(true);
		mDAO.insertar(nuevaMarca);
		mAuditoria.mostrarLog(loginDTO, getClass(), "Marca insertada",
				"Marca: " + nuevaMarca.getDescripcion() + " insertada");
	}

	public void eliminarMarca(Marca pro) throws Exception {
		Marca marca = (Marca) mDAO.findById(Marca.class, pro.getIdMarca());
		marca.setEstado(false);
		mDAO.actualizar(marca);
		mAuditoria.mostrarLog(getClass(), "Marca eliminada", "Marca: " + marca.getIdMarca() + " eliminada");
	}

	public void actualizarMarca(Marca unid) throws Exception {
		Marca marca = (Marca) mDAO.findById(Marca.class, unid.getIdMarca());
		marca.setDescripcion(unid.getDescripcion());
		mDAO.actualizar(marca);
		mAuditoria.mostrarLog(getClass(), "Marca actualizada", "Marca: " + marca.getDescripcion() + " actualizada");
	}

	/* ============================================================== */
	/* CATEGORÍA */
	/* ============================================================== */
	public List<Categoria> allCategorias() {
		String sentenciaJPQL = "o.estado=true";
		return mDAO.findWhere(Categoria.class, sentenciaJPQL);
	}

	public void insertarCategoria(LoginDTO loginDTO, Categoria nuevaCategoria) throws Exception {
		if (nuevaCategoria == null)
			throw new Exception("Debe ingresar los datos de la categoría");
		SegUsuario usuario = (SegUsuario) mDAO.findById(SegUsuario.class, loginDTO.getIdSegUsuario());
		nuevaCategoria.setSegUsuario(usuario);
		nuevaCategoria.setEstado(true);
		mDAO.insertar(nuevaCategoria);
		mAuditoria.mostrarLog(loginDTO, getClass(), "Categoría insertada",
				"Categoria: " + nuevaCategoria.getIdCategoria() + " insertado");
	}

	public void eliminarCategoria(Categoria pro) throws Exception {
		Categoria categoria = (Categoria) mDAO.findById(Categoria.class, pro.getIdCategoria());
		categoria.setEstado(false);
		mDAO.actualizar(categoria);
		mAuditoria.mostrarLog(getClass(), "Categoría eliminado",
				"Categoría: " + categoria.getIdCategoria() + " eliminado");
	}

	public void actualizarCategoria(Categoria unid) throws Exception {
		Categoria categoria = (Categoria) mDAO.findById(Categoria.class, unid.getIdCategoria());
		categoria.setDescripcion(unid.getDescripcion());
		mDAO.actualizar(categoria);
		mAuditoria.mostrarLog(getClass(), "Categoría actualizado",
				"Categoría: " + categoria.getDescripcion() + " actualizado");
	}

	/* ============================================================== */
	/* SUBCATEGORÍA */
	/* ============================================================== */
	public List<Subcategoria> allSubCat() {
		String sentenciaJPQL = "o.estado=true";
		return mDAO.findWhere(Subcategoria.class, sentenciaJPQL);
	}

	public void insertarSubCat(LoginDTO loginDTO, Subcategoria nuevasubcat, int idCategoria) throws Exception {
		if (nuevasubcat == null)
			throw new Exception("Debe ingresar los datos de la subcategoría");
		SegUsuario usuario = (SegUsuario) mDAO.findById(SegUsuario.class, loginDTO.getIdSegUsuario());
		Categoria categoria = (Categoria) mDAO.findById(Categoria.class, idCategoria);
		nuevasubcat.setSegUsuario(usuario);
		nuevasubcat.setCategoria(categoria);
		nuevasubcat.setDescripcion(nuevasubcat.getDescripcion());
		nuevasubcat.setEstado(true);
		mDAO.insertar(nuevasubcat);
		mAuditoria.mostrarLog(loginDTO, getClass(), "Subcategoría insertada",
				"Subcategoría: " + nuevasubcat.getDescripcion() + " insertada");
	}

	public void eliminarSubCat(Subcategoria subc) throws Exception {
		Subcategoria subcat = (Subcategoria) mDAO.findById(Subcategoria.class, subc.getIdSubcategoria());
		subcat.setEstado(false);
		mDAO.actualizar(subcat);
		mAuditoria.mostrarLog(getClass(), "Subcategoría eliminada",
				"Subcategoría: " + subcat.getDescripcion() + " eliminada");
	}

	public void actualizarSubCat(Subcategoria subc) throws Exception {
		Subcategoria subcat = (Subcategoria) mDAO.findById(Subcategoria.class, subc.getIdSubcategoria());
		subcat.setDescripcion(subc.getDescripcion());
		subcat.setIdSubcategoria(subc.getIdSubcategoria());
		mDAO.actualizar(subcat);
		mAuditoria.mostrarLog(getClass(), "Subcategoría actualizada",
				"Subcategoría: " + subcat.getDescripcion() + " actualizada");
	}

	/* ============================================================== */
	/* CLASIFICACIÓN */
	/* ============================================================== */
	public List<Clasificacion> allClasificaciones() {
		String sentenciaJPQL = "o.estado=true";
		return mDAO.findWhere(Clasificacion.class, sentenciaJPQL);
	}

	public void insertarClasificacion(LoginDTO loginDTO, Clasificacion nuevaClasificacion, int idSubcategoria)
			throws Exception {
		if (nuevaClasificacion == null)
			throw new Exception("Debe ingresar los datos de la clasificación");
		SegUsuario usuario = (SegUsuario) mDAO.findById(SegUsuario.class, loginDTO.getIdSegUsuario());
		Subcategoria subcategoria = (Subcategoria) mDAO.findById(Subcategoria.class, idSubcategoria);
		nuevaClasificacion.setSegUsuario(usuario);
		nuevaClasificacion.setSubcategoria(subcategoria);
		nuevaClasificacion.setDescripcion(nuevaClasificacion.getDescripcion());
		nuevaClasificacion.setEstado(true);
		mDAO.insertar(nuevaClasificacion);
		mAuditoria.mostrarLog(loginDTO, getClass(), "Clasificación insertada",
				"Clasificación: " + nuevaClasificacion.getDescripcion() + " insertada");
	}

	public void eliminarClasificacion(Clasificacion cla) throws Exception {
		Clasificacion clasificacion = (Clasificacion) mDAO.findById(Clasificacion.class, cla.getIdClasificacion());
		clasificacion.setEstado(false);
		mDAO.actualizar(clasificacion);
		mAuditoria.mostrarLog(getClass(), "Clasificación eliminada",
				"Clasificación:: " + clasificacion.getDescripcion() + " eliminada");
	}

	public void actualizarClasificacion(Clasificacion cla) throws Exception {
		Clasificacion clasificacion = (Clasificacion) mDAO.findById(Clasificacion.class, cla.getIdClasificacion());
		clasificacion.setDescripcion(cla.getDescripcion());
		clasificacion.setIdClasificacion(cla.getIdClasificacion());
		mDAO.actualizar(clasificacion);
		mAuditoria.mostrarLog(getClass(), "Clasificación actualizada",
				"Clasificación: " + clasificacion.getDescripcion() + " actualizada");
	}

	/* ============================================================== */
	/* TIPO DE PRODUCTO */
	/* ============================================================== */
	public List<TipoProducto> allTipoProductos() {
		String sentenciaJPQL = "o.estado=true";
		return mDAO.findWhere(TipoProducto.class, sentenciaJPQL);
	}

	public void insertarTipoProducto(LoginDTO loginDTO, TipoProducto nuevoTipoProducto) throws Exception {
		if (nuevoTipoProducto == null)
			throw new Exception("Debe ingresar los datos del tipo de producto");
		SegUsuario usuario = (SegUsuario) mDAO.findById(SegUsuario.class, loginDTO.getIdSegUsuario());
		nuevoTipoProducto.setSegUsuario(usuario);
		nuevoTipoProducto.setEstado(true);
		mDAO.insertar(nuevoTipoProducto);
		mAuditoria.mostrarLog(loginDTO, getClass(), "Tipo de producto insertado",
				"Tipo de producto: " + nuevoTipoProducto.getDescripcion() + " insertado");
	}

	public void eliminarTipoProducto(TipoProducto tipro) throws Exception {
		TipoProducto tipoProducto = (TipoProducto) mDAO.findById(TipoProducto.class, tipro.getIdTipoProducto());
		tipoProducto.setEstado(false);
		mDAO.actualizar(tipoProducto);
		mAuditoria.mostrarLog(getClass(), "Tipo de producto eliminado",
				"Marca: " + tipoProducto.getIdTipoProducto() + " eliminado");
	}

	public void actualizarTipoProducto(TipoProducto tipro) throws Exception {
		TipoProducto tipoProducto = (TipoProducto) mDAO.findById(TipoProducto.class, tipro.getIdTipoProducto());
		tipoProducto.setDescripcion(tipro.getDescripcion());
		mDAO.actualizar(tipoProducto);
		mAuditoria.mostrarLog(getClass(), "Tipo de producto actualizado",
				"Tipo de producto: " + tipoProducto.getDescripcion() + " actualizado");
	}

	/* ============================================================== */
	/* LISTA PRECIO */
	/* ============================================================== */
	public List<ListaPrecio> allListaPrecios() {
		String sentenciaJPQL = "o.estado=true";
		return mDAO.findWhere(ListaPrecio.class, sentenciaJPQL);
	}

	public void insertarListaPrecio(LoginDTO loginDTO, ListaPrecio nuevaListaPrecio) throws Exception {
		if (nuevaListaPrecio == null)
			throw new Exception("Debe ingresar los datos de la lista de precios");
		SegUsuario usuario = (SegUsuario) mDAO.findById(SegUsuario.class, loginDTO.getIdSegUsuario());
		nuevaListaPrecio.setSegUsuario(usuario);
		nuevaListaPrecio.setEstado(true);
		mDAO.insertar(nuevaListaPrecio);
		mAuditoria.mostrarLog(loginDTO, getClass(), "Lista de precios",
				"Lista de precios: " + nuevaListaPrecio.getDescripcion() + " insertada");
	}

	public void eliminarListaPrecio(ListaPrecio pro) throws Exception {
		ListaPrecio listaPrecio = (ListaPrecio) mDAO.findById(ListaPrecio.class, pro.getIdListaPrecio());
		listaPrecio.setEstado(false);
		mDAO.actualizar(listaPrecio);
		mAuditoria.mostrarLog(getClass(), "Lista de precios eliminado",
				"Lista de precios: " + listaPrecio.getIdListaPrecio() + " eliminado");
	}

	public void actualizarListaPrecio(ListaPrecio lispre) throws Exception {
		ListaPrecio listaPrecio = (ListaPrecio) mDAO.findById(ListaPrecio.class, lispre.getIdListaPrecio());
		listaPrecio.setDescripcion(lispre.getDescripcion());
		mDAO.actualizar(listaPrecio);
		mAuditoria.mostrarLog(getClass(), "Lista de precios actualizada",
				"Lista de precios: " + listaPrecio.getDescripcion() + " actualizado");
	}

	/* ============================================================== */
	/* PRODUCTO */
	/* ============================================================== */
	public List<Producto> allProductos() {
		String sentenciaJPQL = "o.estado=true";
		return mDAO.findWhere(Producto.class, sentenciaJPQL);
	}

	public List<Subcategoria> actionListenerFiltrarSubcategoria(int idCategoria) {
		return mDAO.findSubcategoriaByCategoria(Subcategoria.class, idCategoria);
	}

	public List<Clasificacion> actionListenerFiltrarClasificacion(int idSubcategoria) {
		return mDAO.findClasificacionBySubcategoria(Clasificacion.class, idSubcategoria);
	}

	public void insertarProducto(LoginDTO loginDTO, Producto nuevoProducto, int idUnidadM, int idMarca,
			int idClasificacion, int idTipoProducto, int idListaPrecio) throws Exception {
		if (nuevoProducto == null)
			throw new Exception("Debe ingresar los datos del producto");

		SegUsuario usuario = (SegUsuario) mDAO.findById(SegUsuario.class, loginDTO.getIdSegUsuario());

		UnidadMedida unidadM = (UnidadMedida) mDAO.findById(UnidadMedida.class, idUnidadM);
		Marca marca = (Marca) mDAO.findById(Marca.class, idMarca);
		Clasificacion clasificacion = (Clasificacion) mDAO.findById(Clasificacion.class, idClasificacion);
		TipoProducto tipoProducto = (TipoProducto) mDAO.findById(TipoProducto.class, idTipoProducto);
		ListaPrecio listaPrecio = (ListaPrecio) mDAO.findById(ListaPrecio.class, idListaPrecio);

		nuevoProducto.setSegUsuario(usuario);

		nuevoProducto.setUnidadMedida(unidadM);// vista
		nuevoProducto.setMarca(marca);// vista
		nuevoProducto.setClasificacion(clasificacion);// vista
		nuevoProducto.setTipoProducto(tipoProducto);// vista
		nuevoProducto.setListaPrecio(listaPrecio);// vista

		nuevoProducto.setDescripcion(nuevoProducto.getDescripcion());// vista

		nuevoProducto.setFechaCaducidad(new Date());// vista
		nuevoProducto
				.setPrecio(new BigDecimal(nuevoProducto.getPrecio().doubleValue()).setScale(2, RoundingMode.HALF_UP)); // vista
		nuevoProducto.setPrecioVenta(nuevoProducto.getPrecio());// default
		nuevoProducto.setStock(nuevoProducto.getStock());// vista
		nuevoProducto.setStockMinimo(nuevoProducto.getStock());// default
		nuevoProducto.setStockMaximo(nuevoProducto.getStock());// default
		nuevoProducto.setUtilidad(new BigDecimal(0));// default
		nuevoProducto.setDescuento(new BigDecimal(0));// default
		nuevoProducto.setImpuesto(new BigDecimal(0));// default
		nuevoProducto.setEstado(true);// default

		mDAO.insertar(nuevoProducto);
		mAuditoria.mostrarLog(loginDTO, getClass(), "Producto insertado",
				"Producto: " + nuevoProducto.getIdProducto() + " insertado");
	}

	public void eliminarProducto(Producto pro) throws Exception {
		Producto producto = (Producto) mDAO.findById(Producto.class, pro.getIdProducto());
		producto.setEstado(false);
		mDAO.actualizar(producto);
		mAuditoria.mostrarLog(getClass(), "Producto eliminado", "Producto: " + producto.getIdProducto() + " eliminado");
	}

	public void actualizarProducto(Producto pro) throws Exception {
		Producto producto = (Producto) mDAO.findById(Producto.class, pro.getIdProducto());
		producto.setUnidadMedida(pro.getUnidadMedida());
		producto.setMarca(pro.getMarca());
//		producto.setClasificacion(pro.getClasificacion());
//		producto.setTipoProducto(pro.getTipoProducto());
//		producto.setListaPrecio(pro.getListaPrecio());
		producto.setDescripcion(pro.getDescripcion());
//		producto.setFechaCaducidad(pro.getFechaCaducidad());
		producto.setPrecio(pro.getPrecio());
//		producto.setPrecioVenta(pro.getPrecioVenta());
//		producto.setStock(pro.getStock());
//		producto.setStockMinimo(pro.getStockMinimo());
//		producto.setStockMaximo(pro.getStockMaximo());
//		producto.setUtilidad(pro.getUtilidad());
//		producto.setDescuento(pro.getDescuento());
//		producto.setImpuesto(pro.getImpuesto());
		mDAO.actualizar(producto);
		mAuditoria.mostrarLog(getClass(), "Producto actualizado",
				"Producto: " + producto.getDescripcion() + " actualizado");
	}

	/* ============================================================== */
	/* FACTURA */
	/* ============================================================== */
	public List<Entrada> allFacturas() {
		String sentenciaJPQL = "o.estado=true";
		return mDAO.findWhere(Entrada.class, sentenciaJPQL);
	}

	public Entrada adicionarProducto(Entrada factura, int cantidad, Producto prodSeleccionado) throws Exception {
		// comprobar si es la primera vez
		if (factura == null) {
			factura = new Entrada();
			factura.setDetalleEntradas(new ArrayList<DetalleEntrada>());
			factura.setFecha(new Date());
			factura.setTotal(new BigDecimal(0));
			factura.setEstado(true);
		}

		// adicionamos el detalle:
		Producto prod = (Producto) mDAO.findById(Producto.class, prodSeleccionado.getIdProducto());
		// calculos
		double subtotal = prod.getPrecioVenta().doubleValue() * cantidad;
		double IVA = subtotal * prod.getImpuesto().doubleValue();
		double total = subtotal + IVA;

		DetalleEntrada detalle = new DetalleEntrada();
		detalle.setProducto(prod);
		detalle.setEntrada(factura);
		detalle.setCantidad(cantidad);
		detalle.setPrecio(new BigDecimal(subtotal).setScale(2, RoundingMode.HALF_UP));
		
		factura.setTotal(factura.getTotal().add(new BigDecimal(total).setScale(2, RoundingMode.HALF_UP)));
		factura.getDetalleEntradas().add(detalle);
		return factura;
	}

	public void registrarFactura(LoginDTO loginDTO, Entrada factura, String identificacionProveedor, int idTipoPago,
			boolean tipoPago, Date fechaPago, int idMetodoPago, double descuentoFactura) throws Exception {
		if (factura == null || factura.getDetalleEntradas() == null || factura.getDetalleEntradas().size() == 0)
			throw new Exception("Debe seleccionar al menos un producto");

		Proveedor proveedor = (Proveedor) mDAO.findWhithQueryName(Proveedor.class, "Proveedor.findByIdentificacion",
				"identificacion", identificacionProveedor);
		TipoPago tiPago = (TipoPago) mDAO.findById(TipoPago.class, idTipoPago);
		MetodoPago metodoPago = (MetodoPago) mDAO.findById(MetodoPago.class, idMetodoPago);
		SegUsuario usuario = (SegUsuario) mDAO.findById(SegUsuario.class, loginDTO.getIdSegUsuario());
		factura.setProveedor(proveedor);
		factura.setTipoPago(tiPago);
		factura.setSegUsuario(usuario);
		factura.setMetodoPago(metodoPago);// ???
		factura.setEntradaDevuelta(false);
		factura.setNumEntrada(numeroFactura(Entrada.class, "idEntrada"));

		double descuento = descuentoFactura / 100;
		factura.setImpuesto(new BigDecimal(descuento).setScale(2, RoundingMode.HALF_UP));
		double totalFactura = factura.getTotal().doubleValue();
		factura.setTotal(new BigDecimal(totalFactura * (1 - descuento)).setScale(2, RoundingMode.HALF_UP));

		
		if (!tipoPago && fechaPago != null) {
			factura.setFechaPago(fechaPago);
			factura.setEstadoEntrada("P"); // Pendiente
			factura.setDeudaParcial(factura.getTotal());
		} else {
			factura.setFechaPago(new Date());
			factura.setEstadoEntrada("C"); // Cancelado
			factura.setDeudaParcial(new BigDecimal(0).setScale(2, RoundingMode.HALF_UP));
		}

		for (int i = 0; i < factura.getDetalleEntradas().size(); i++) { // AUMENTA el stock de productos
			int idProducto = factura.getDetalleEntradas().get(i).getProducto().getIdProducto();
			Producto prod = (Producto) mDAO.findById(Producto.class, idProducto);
			int cant = factura.getDetalleEntradas().get(i).getCantidad();
			int stock = prod.getStock();
			int suma = stock + cant;
			prod.setStock(suma);
			mDAO.actualizar(prod);
		}

		mDAO.insertar(factura);
		mAuditoria.mostrarLog(loginDTO, getClass(), "Factura registrada",
				"Factura: " + factura.getFecha() + " registrada");
	}

	public void anularFactura(Entrada fac) throws Exception {
		Entrada factura = (Entrada) mDAO.findById(Entrada.class, fac.getIdEntrada());
		factura.setEstado(false);
		mDAO.actualizar(factura);
		mAuditoria.mostrarLog(getClass(), "Factura anulada", "Factura:" + factura.getIdEntrada() + "anulada");
	}

	public void pagarFactura(Entrada fac, double valorPagarFactura) throws Exception {
		Entrada factura = (Entrada) mDAO.findById(Entrada.class, fac.getIdEntrada());
		double nuevoValor = factura.getDeudaParcial().doubleValue() - valorPagarFactura;
		factura.setDeudaParcial(new BigDecimal(nuevoValor).setScale(2, RoundingMode.HALF_UP));

		if (factura.getDeudaParcial().doubleValue() <= 0) {
			factura.setEstadoEntrada("C");
		}
		mDAO.actualizar(factura);
	}

	// ============================== PROFORMA==============================
	public OrdenCompra adicionarProductoOrdenCompra(OrdenCompra proforma, int cantidad, Producto prodSeleccionado)
			throws Exception {
		// comprobar si es la primera vez
		if (proforma == null) {
			proforma = new OrdenCompra();
			proforma.setDetalleOrdenCompras(new ArrayList<DetalleOrdenCompra>());
			proforma.setFecha(new Date());
			proforma.setTotal(new BigDecimal(0));
			proforma.setEstado(true);
		}

		// adicionamos el detalle:
		Producto prod = (Producto) mDAO.findById(Producto.class, prodSeleccionado.getIdProducto());
		;
		// calculos
		double subtotal = prod.getPrecioVenta().doubleValue() * cantidad;
		double IVA = subtotal * prod.getImpuesto().doubleValue();
		double total = subtotal + IVA;

		DetalleOrdenCompra detalle = new DetalleOrdenCompra();
		detalle.setCantidad(cantidad);
		detalle.setPrecio(new BigDecimal(subtotal).setScale(2, RoundingMode.HALF_UP));
		detalle.setOrdenCompra(proforma);
		detalle.setCantidad(detalle.getCantidad());
		detalle.setProducto(prod);
		proforma.setTotal(proforma.getTotal().add(new BigDecimal(total).setScale(2, RoundingMode.HALF_UP)));
		proforma.getDetalleOrdenCompras().add(detalle);
		return proforma;
	}

	public void registrarOrdenCompra(LoginDTO loginDTO, OrdenCompra proforma, String identificacionProveedor,
			Date fechaVencimientoOrdenCompra,double descuentoOrdenCompra) throws Exception {
		if (proforma == null || proforma.getDetalleOrdenCompras() == null
				|| proforma.getDetalleOrdenCompras().size() == 0)
			throw new Exception("Debe seleccionar al menos un producto");
		System.out.println("estoy apunto de regitrat p");
		SegUsuario usuario = (SegUsuario) mDAO.findById(SegUsuario.class, loginDTO.getIdSegUsuario());
		Proveedor proveedor = (Proveedor) mDAO.findWhithQueryName(Proveedor.class, "Proveedor.findByIdentificacion",
				"identificacion", identificacionProveedor);
		proforma.setSegUsuario(usuario);
		proforma.setProveedor(proveedor);
		proforma.setFecha(new Date());
		proforma.setFechaVencimiento(fechaVencimientoOrdenCompra);
		proforma.setNumOrdenCompra(numeroFactura(OrdenCompra.class, "idOrdenCompra"));
		
		double descuento = descuentoOrdenCompra / 100;
		proforma.setImpuesto(new BigDecimal(descuento).setScale(2, RoundingMode.HALF_UP));
		double totalProforma = proforma.getTotal().doubleValue();
		proforma.setTotal(new BigDecimal(totalProforma * (1 - descuento)).setScale(2, RoundingMode.HALF_UP));
		
		mDAO.insertar(proforma);
		mAuditoria.mostrarLog(loginDTO, getClass(), "Orden de compra registrada",
				"Orden de compra: " + proforma.getFecha() + " registrada");
	}

	public void anularOrdenCompra(OrdenCompra proform) throws Exception {
		OrdenCompra proforma = (OrdenCompra) mDAO.findById(OrdenCompra.class, proform.getIdOrdenCompra());
		proforma.setEstado(false);
		mDAO.actualizar(proforma);
		mAuditoria.mostrarLog(getClass(), "Proforma anulada", "Proforma:" + proforma.getIdOrdenCompra() + "anulada");
	}

//	=========================PROFORMA-->FACTURA================================
	public void registrarOrdenCompraEntrada(LoginDTO loginDTO, Entrada factura, int idPago, boolean tipoPago, Date fechaPago,
			int idMetodoPago) throws Exception {
		if (factura == null || factura.getDetalleEntradas() == null || factura.getDetalleEntradas().size() == 0)
			throw new Exception("Debe seleccionar al menos un producto");

		TipoPago pago = (TipoPago) mDAO.findById(TipoPago.class, idPago);
		MetodoPago Mpago = (MetodoPago) mDAO.findById(MetodoPago.class, idMetodoPago);
		SegUsuario usuario = (SegUsuario) mDAO.findById(SegUsuario.class, loginDTO.getIdSegUsuario());
		factura.setTipoPago(pago);
		factura.setSegUsuario(usuario);
		factura.setMetodoPago(Mpago);
		factura.setEntradaDevuelta(false);
		factura.setNumEntrada(numeroFactura(Entrada.class, "idEntrada"));

		if (!tipoPago && fechaPago != null) {
			factura.setFechaPago(fechaPago);
			factura.setEstadoEntrada("P"); // Pendiente
			factura.setDeudaParcial(factura.getTotal());
		} else {
			factura.setFechaPago(new Date());
			factura.setEstadoEntrada("C"); // Cancelado
			factura.setDeudaParcial(new BigDecimal(0).setScale(2, RoundingMode.HALF_UP));
		}

		for (int i = 0; i < factura.getDetalleEntradas().size(); i++) { // AUMENTA el stock de productos
			int idProducto = factura.getDetalleEntradas().get(i).getProducto().getIdProducto();
			Producto prod = (Producto) mDAO.findById(Producto.class, idProducto);
			int cant = factura.getDetalleEntradas().get(i).getCantidad();
			int stock = prod.getStock();
			prod.setStock(stock + cant);
			mDAO.actualizar(prod);
		}

		mDAO.insertar(factura);
		mAuditoria.mostrarLog(loginDTO, getClass(), "Factura registrada",
				"Factura: " + factura.getFecha() + " registrada");
	}
	
	// =======================DEVOLUCION
	// ======================================================================
	public void registarDevolucion(LoginDTO loginDTO, Entrada fac, String descripcion, List<DetalleEntrada> detEntrada,
			boolean devolverTodo) throws Exception {
		Entrada factura = (Entrada) mDAO.findById(Entrada.class, fac.getIdEntrada());
		SegUsuario usuario = (SegUsuario) mDAO.findById(SegUsuario.class, loginDTO.getIdSegUsuario());

		DevolucionEntrada devolucion = new DevolucionEntrada();
		devolucion.setEntrada(factura);
		devolucion.setSegUsuario(usuario);
		devolucion.setFechaDevolucion(new Date());
		devolucion.setDescripcion(descripcion);
		devolucion.setEstado(true);
		devolucion.setSaldoDevolver(new BigDecimal(0).setScale(2, RoundingMode.HALF_UP));
		factura.setEntradaDevuelta(true);

		if (devolverTodo) {
			devolucion.setTipoDevolucion("Devolución Total");
		} else {
			devolucion.setTipoDevolucion("Devolución Parcial");
		}
		mDAO.insertar(devolucion);

		int count = (int) mDAO.count(DevolucionEntrada.class, "idDevolucionEntrada");
		DevolucionEntrada sameDevolucion = (DevolucionEntrada) mDAO.findById(DevolucionEntrada.class,
				loginDTO.getIdSegUsuario());

		if (devolverTodo) {
			factura.setEstado(false);
			sameDevolucion.setSaldoDevolver(
					new BigDecimal(factura.getTotal().doubleValue()).setScale(2, RoundingMode.HALF_UP));

			for (int i = 0; i < fac.getDetalleEntradas().size(); i++) {
				// DIMINUIR Stock;
				Producto prod = (Producto) mDAO.findById(Producto.class,
						fac.getDetalleEntradas().get(i).getProducto().getIdProducto());
				int cant = fac.getDetalleEntradas().get(i).getCantidad();
				int stock = prod.getStock();
				prod.setStock(stock - cant); //?? REvisar VENTAS
				mDAO.actualizar(prod);
			}
			mDAO.actualizar(sameDevolucion);

		} else {
			double nuevoTotal = 0;
			for (int i = 0; i < detEntrada.size(); i++) {
				DetalleDevolucionEntrada detalleDevolucion = new DetalleDevolucionEntrada();
				DetalleEntrada detalleEntrada = (DetalleEntrada) mDAO.findById(DetalleEntrada.class,
						detEntrada.get(i).getIdDetalleEntrada());
				detalleDevolucion.setDevolucionEntrada(sameDevolucion);
				detalleDevolucion.setProducto(detEntrada.get(i).getProducto());
				detalleDevolucion.setCantidadDevuelto(detEntrada.get(i).getCantidad());

				// Calculo para devolver saldo
				double cantidadRestante = detalleEntrada.getCantidad().doubleValue()
						- detEntrada.get(i).getCantidad().doubleValue();
				double subtotal = detEntrada.get(i).getProducto().getPrecio().doubleValue() * cantidadRestante;
				double IVA = subtotal * detEntrada.get(i).getProducto().getImpuesto().doubleValue();
				;
				nuevoTotal += subtotal + IVA;
				mDAO.insertar(detalleDevolucion);

				// DISMINUIR Stock;
				Producto prod = (Producto) mDAO.findById(Producto.class, detEntrada.get(i).getProducto().getIdProducto());
				int cant = detEntrada.get(i).getCantidad();
				int stock = prod.getStock();
				prod.setStock((stock - cant));//??
				mDAO.actualizar(prod);
			}

			// Continuacion de entidad devolucion
			double totalFactura = fac.getTotal().doubleValue();
			sameDevolucion
					.setSaldoDevolver(new BigDecimal((totalFactura - nuevoTotal)).setScale(2, RoundingMode.HALF_UP));
			mDAO.actualizar(sameDevolucion);
		}

		mDAO.actualizar(factura);
		mAuditoria.mostrarLog(loginDTO, getClass(), "Entrada Devuelta",
				"Devolución: " + devolucion.getDescripcion() + " registrada");
	}
	
	// Metodo extra

		public String numeroFactura(Class clase, String identificador) {
			String numFactura = "";
			int count = 0;

			try {
				count = (int) mDAO.count(clase, identificador);
				if (count == 0) {
					numFactura = "FK-000000" + (count + 1);
					System.out.println("num: " + count + 1);
				} else {
					System.out.println("num: " + count + 1);
					if (count < 10) {
						numFactura = "FK-00000" + (count + 1);
					} else if (count < 100 && count >= 10) {
						numFactura = "FK-0000" + (count + 1);

					} else if (count < 1000 && count >= 100) {
						numFactura = "FK-000" + (count + 1);

					} else if (count < 10000 && count >= 1000) {
						numFactura = "FK-00" + (count + 1);

					} else if (count < 100000 && count >= 10000) {
						numFactura = "FK-0" + (count + 1);
					}
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return numFactura;
		}
}
