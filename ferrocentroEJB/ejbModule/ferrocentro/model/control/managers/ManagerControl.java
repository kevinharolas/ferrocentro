package ferrocentro.model.control.managers;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import minimarketdemo.model.auditoria.managers.ManagerAuditoria;
import minimarketdemo.model.core.entities.Cliente;
import minimarketdemo.model.core.entities.Control;
import minimarketdemo.model.core.entities.DetalleControl;
import minimarketdemo.model.core.entities.DetalleVenta;
import minimarketdemo.model.core.entities.Producto;
import minimarketdemo.model.core.entities.SegUsuario;
import minimarketdemo.model.core.entities.TipoPago;
import minimarketdemo.model.core.entities.Venta;
import minimarketdemo.model.core.managers.ManagerDAO;
import minimarketdemo.model.seguridades.dtos.LoginDTO;

/**
 * Session Bean implementation class ManagerControl
 */
@Stateless
@LocalBean
public class ManagerControl {
	@EJB
	private ManagerDAO mDAO;
	@EJB
	private ManagerAuditoria mAuditoria;

	public ManagerControl() {
		// TODO Auto-generated constructor stub
	}

	public List<Control> allControles() {
		String sentenciaJPQL = "o.estado=true";
		return mDAO.findWhere(Control.class, sentenciaJPQL);
	}

	public List<TipoPago> allTipoPago() {
		String sentenciaJPQL = "o.estado=true";
		return mDAO.findWhere(TipoPago.class, sentenciaJPQL);
	}

	public List<Producto> allProductos() {
		return mDAO.findAll(Producto.class);
	}

//===================== TIPO DE PAGO ======================================
	public void insertarPago(LoginDTO loginDTO, TipoPago nuevoPago) throws Exception {
		if (nuevoPago == null)
			throw new Exception("Debe ingresar los datos del pago");

		nuevoPago.setEstado(true);
		mDAO.insertar(nuevoPago);
		mAuditoria.mostrarLog(loginDTO, getClass(), "Pago insertado",
				"Pago: " + nuevoPago.getDescripcion() + " insertado");
	}

	public void eliminarPago(LoginDTO loginDTO, TipoPago pag) throws Exception {
		TipoPago pago = (TipoPago) mDAO.findById(TipoPago.class, pag.getIdTipoPago());
		pago.setEstado(false);
		mDAO.actualizar(pago);
		mAuditoria.mostrarLog(loginDTO, getClass(), "Pago eliminado", "Pago: " + pago.getDescripcion() + " eliminado");
	}

	public void actualizarPago(LoginDTO loginDTO, TipoPago pag) throws Exception {
		TipoPago pago = (TipoPago) mDAO.findById(TipoPago.class, pag.getIdTipoPago());
		pago.setDescripcion(pago.getDescripcion());
		mDAO.actualizar(pago);
		mAuditoria.mostrarLog(loginDTO, getClass(), "Pago actualizado",
				"Pago: " + pago.getDescripcion() + " actualizado");
	}

	// ============================== Modificar Stock Por Da�os o Situaciones
	// ==============================

	public void modificarStockProducto(LoginDTO loginDTO, Producto pro, int cantidad, boolean tipoAltaBaja)
			throws Exception {
		Producto producto = (Producto) mDAO.findById(Producto.class, pro.getIdProducto());
		int stock = producto.getStock();

		if (tipoAltaBaja) {
			producto.setStock(stock + cantidad);
		} else {
			producto.setStock(stock - cantidad);
		}

		mDAO.actualizar(producto);
		mAuditoria.mostrarLog(loginDTO, getClass(), "Stock Modificado",
				"Stock de Producto: " + producto.getDescripcion() + " modificado");
	}

	// ============================== Ajustar Stock Minimo o Maximo
	// ==============================
	public void AjustarStockProductoMinMax(LoginDTO loginDTO, Producto pro, int cantidad, boolean tipoMinMax)
			throws Exception {
		Producto producto = (Producto) mDAO.findById(Producto.class, pro.getIdProducto());

		if (tipoMinMax) {
			producto.setStockMaximo(cantidad);
			mAuditoria.mostrarLog(loginDTO, getClass(), "Stock M�ximo Modificado",
					"Stock de Producto: " + producto.getDescripcion() + " modificado");
		} else {
			producto.setStockMinimo(cantidad);
			mAuditoria.mostrarLog(loginDTO, getClass(), "Stock M�nimo Modificado",
					"Stock de Producto: " + producto.getDescripcion() + " modificado");
		}

		mDAO.actualizar(producto);
	}

	// =================Modificar Precio de Producto =================
	public void modificarPrecioProducto(LoginDTO loginDTO, Producto pro, double valor) throws Exception {
		System.out.println("valor modificar: " + valor);
		Producto prod = (Producto) mDAO.findById(Producto.class, pro.getIdProducto());
		prod.setPrecioVenta(new BigDecimal(valor).setScale(2, RoundingMode.HALF_UP));
		// mDAO.actualizar(prod);
		// mAuditoria.mostrarLog(loginDTO, getClass(), "Precio modificado","Precio :" +
		// prod.getDescripcion() + " Modificado");
	}

	// =================Ajustar Utilidad por Producto =================
	public void ajustarPorcetajeUtilidad(LoginDTO loginDTO, Producto pro, double porcentaje) throws Exception {
		Producto producto = (Producto) mDAO.findById(Producto.class, pro.getIdProducto());

		double valorPorcentaje = porcentaje / 100;
		double precioVenta = producto.getPrecio().doubleValue() / (1 - valorPorcentaje);
		producto.setUtilidad(new BigDecimal(valorPorcentaje).setScale(2, RoundingMode.HALF_UP));
		producto.setPrecioVenta(new BigDecimal(precioVenta).setScale(2, RoundingMode.HALF_UP));
		mDAO.actualizar(producto);
		mAuditoria.mostrarLog(loginDTO, getClass(), "Utilidad agregada",
				"Utilidad de :" + producto.getDescripcion() + " agregado");
	}

	// =================Ajustar Descuento por Producto =================
	public void ajustarPorcetajeDescuento(LoginDTO loginDTO, Producto pro, double porcentaje) throws Exception {
		Producto producto = (Producto) mDAO.findById(Producto.class, pro.getIdProducto());

		double valorPorcentaje = porcentaje / 100;
		double precioVenta = producto.getPrecioVenta().doubleValue() * (1 - valorPorcentaje);
		producto.setDescuento(new BigDecimal(valorPorcentaje).setScale(2, RoundingMode.HALF_UP));
		producto.setPrecioVenta(new BigDecimal(precioVenta).setScale(2, RoundingMode.HALF_UP));
		mDAO.actualizar(producto);
		mAuditoria.mostrarLog(loginDTO, getClass(), "Descuento agregada",
				"Descuento de :" + producto.getDescripcion() + " agregado");
	}

}
