package ferrocentro.controller.control;

import java.io.FileNotFoundException;
import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.event.AjaxBehaviorEvent;
import javax.inject.Inject;
import javax.inject.Named;

import com.itextpdf.text.DocumentException;

import ferrocentro.controller.factura.ReporteProforma;
import ferrocentro.model.control.managers.ManagerControl;
import ferrocentro.model.ventas.managers.ManagerVentas;
import minimarketdemo.controller.JSFUtil;
import minimarketdemo.controller.seguridades.BeanSegLogin;
import minimarketdemo.model.core.entities.Cliente;
import minimarketdemo.model.core.entities.Control;
import minimarketdemo.model.core.entities.TipoPago;
import minimarketdemo.model.core.entities.Producto;
import minimarketdemo.model.core.entities.Subcategoria;

@Named
@SessionScoped
public class BeanKymsControl implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@EJB
	private ManagerControl mControl;
	@EJB
	private ManagerVentas mVenta;
	@Inject
	private BeanSegLogin beanSegLogin;

	// Listas

	private List<Producto> listaProductos;

	public BeanKymsControl() {
		// TODO Auto-generated constructor stub
	}

	@PostConstruct
	public void inicializar() {
		listaProductos = mControl.allProductos();
		listaPagos = mControl.allTipoPago();
		nuevoPago= new TipoPago();
		System.out.println("controller control para reportejjjjjjjjjjj");
		
	
	}
	
	// ============================== PAGO ================================
	
	private List<TipoPago> listaPagos;
	private TipoPago nuevoPago;
	private TipoPago editarPago;

		public void actionListenerInsertarPago() {
			try {
				mControl.insertarPago(beanSegLogin.getLoginDTO(),nuevoPago);
				nuevoPago=null;
				nuevoPago = new TipoPago();
				listaPagos =  mControl.allTipoPago();
				JSFUtil.crearMensajeINFO("Pago insertado");
			} catch (Exception e) {
				JSFUtil.crearMensajeERROR(e.getMessage());
				e.printStackTrace();
			}
		}
	
		public void actionListenerEliminarPago(TipoPago pa) {
			try {
			    mControl.eliminarPago(beanSegLogin.getLoginDTO(),pa);
				listaPagos =  mControl.allTipoPago();
				JSFUtil.crearMensajeINFO("Pago eliminado");
			} catch (Exception e) {
				JSFUtil.crearMensajeERROR(e.getMessage());
				e.printStackTrace();
			}
		}
		
		public void actionListenerActualizarPago() {
			try {
				mControl.actualizarPago(beanSegLogin.getLoginDTO(),editarPago);
				JSFUtil.crearMensajeINFO("Pago actualizado");
			} catch (Exception e) {
				JSFUtil.crearMensajeERROR(e.getMessage());
				e.printStackTrace();
			}
		}
		
	    public void actionSelectPago(TipoPago pag) {
	        editarPago = pag;
	    }
	    
	    
	// ============================ CONTROL STOCK ==========================
		
		private int cantidad;
		private String  descripcionProducto;
		private Producto productoSeleccionado; //Producto seleccionado con el autocomplete
		private boolean ModificarStock;
		private boolean AjusteStockMaxMin;
		
	public List<String> completeTextProducto(String query) { //BuscarProducto
	    	String queryLowerCase = query.toLowerCase();
	    	List<String> prodList = new ArrayList<>();
	    	for (Producto prod : listaProductos) {
	    	prodList.add(prod.getDescripcion());
	    	}
	    	return prodList.stream().filter(t -> t.toLowerCase().startsWith(queryLowerCase)).collect(Collectors.toList());
	 }
	
	
	public void MostrarStock(AjaxBehaviorEvent e) {
		try {
			productoSeleccionado= mVenta.findOneProducto(descripcionProducto);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	public void actionListenerGuardarModificacionStock() {
		try {
			 mControl.modificarStockProducto(beanSegLogin.getLoginDTO(), productoSeleccionado, cantidad, ModificarStock);
			 cantidad=0;
			 productoSeleccionado= mVenta.findOneProducto(descripcionProducto);
			JSFUtil.crearMensajeINFO("Stock Modificado");
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void actionListenerGuardarAjusteStockMaxMin() {
		try {
			 mControl.AjustarStockProductoMinMax(beanSegLogin.getLoginDTO(), productoSeleccionado, cantidad, AjusteStockMaxMin);
			 cantidad=0;
			 productoSeleccionado= mVenta.findOneProducto(descripcionProducto);
			 if(AjusteStockMaxMin) {
					JSFUtil.crearMensajeINFO("Stock M�ximo Modificado");
		    	}else {
		    		JSFUtil.crearMensajeINFO("Stock M�nimo Modificado");
		    	}
		
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}

	// =================AJUSTES DE PRECIOS UTILIDAD Y DESCUENTO ===================
	private String valorPrecioVenta;
	private String porcentajeUtilidad;
	private String porcentajeDescuento;
	
	public void actionListenerAjustarPrecio() {
		try {
			mControl.modificarPrecioProducto(beanSegLogin.getLoginDTO(), productoSeleccionado, Double.parseDouble(valorPrecioVenta));
			valorPrecioVenta="";
			productoSeleccionado= mVenta.findOneProducto(descripcionProducto);
			JSFUtil.crearMensajeINFO("Precio de Venta Modificado");
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}
	public void actionListenerAjustarUtilidad() {
		try {
			mControl.ajustarPorcetajeUtilidad(beanSegLogin.getLoginDTO(), productoSeleccionado, Double.parseDouble(porcentajeUtilidad));
			porcentajeUtilidad="";
			productoSeleccionado= mVenta.findOneProducto(descripcionProducto);
			JSFUtil.crearMensajeINFO("Utilida agregada");
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}
	public void actionListenerAjustarDescuento() {
		try {
			mControl.ajustarPorcetajeDescuento(beanSegLogin.getLoginDTO(), productoSeleccionado, Double.parseDouble(porcentajeDescuento));
			porcentajeDescuento="";
			productoSeleccionado= mVenta.findOneProducto(descripcionProducto);
			JSFUtil.crearMensajeINFO("Descuento agregada");
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}
	
	
	
	// =================GETTERS AND SETTERS ===================
	public List<Producto> getListaProductos() {
		return listaProductos;
	}

	public void setListaProductos(List<Producto> listaProductos) {
		this.listaProductos = listaProductos;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public String getValorPrecioVenta() {
		return valorPrecioVenta;
	}

	public void setValorPrecioVenta(String valorPrecioVenta) {
		this.valorPrecioVenta = valorPrecioVenta;
	}

	public String getPorcentajeUtilidad() {
		return porcentajeUtilidad;
	}

	public void setPorcentajeUtilidad(String porcentajeUtilidad) {
		this.porcentajeUtilidad = porcentajeUtilidad;
	}

	public String getPorcentajeDescuento() {
		return porcentajeDescuento;
	}

	public void setPorcentajeDescuento(String porcentajeDescuento) {
		this.porcentajeDescuento = porcentajeDescuento;
	}
	public List<TipoPago> getListaPagos() {
		return listaPagos;
	}

	public void setListaPagos(List<TipoPago> listaPagos) {
		this.listaPagos = listaPagos;
	}

	public TipoPago getNuevoPago() {
		return nuevoPago;
	}

	public void setNuevoPago(TipoPago nuevoPago) {
		this.nuevoPago = nuevoPago;
	}

	public TipoPago getEditarPago() {
		return editarPago;
	}

	public void setEditarPago(TipoPago editarPago) {
		this.editarPago = editarPago;
	}

	public String getDescripcionProducto() {
		return descripcionProducto;
	}

	public void setDescripcionProducto(String descripcionProducto) {
		this.descripcionProducto = descripcionProducto;
	}

	public Producto getProductoSeleccionado() {
		return productoSeleccionado;
	}

	public void setProductoSeleccionado(Producto productoSeleccionado) {
		this.productoSeleccionado = productoSeleccionado;
	}

	public boolean isModificarStock() {
		return ModificarStock;
	}

	public void setModificarStock(boolean modificarStock) {
		ModificarStock = modificarStock;
	}

	public boolean isAjusteStockMaxMin() {
		return AjusteStockMaxMin;
	}

	public void setAjusteStockMaxMin(boolean ajusteStockMaxMin) {
		AjusteStockMaxMin = ajusteStockMaxMin;
	}
	

}
