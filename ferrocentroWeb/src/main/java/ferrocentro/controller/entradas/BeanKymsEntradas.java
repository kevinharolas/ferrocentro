/**
 * 
 */
package ferrocentro.controller.entradas;

import java.io.FileNotFoundException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.event.AjaxBehaviorEvent;
import javax.inject.Inject;
import javax.inject.Named;

import com.itextpdf.text.DocumentException;

import ferrocentro.controller.factura.ReporteFactura;
import ferrocentro.controller.factura.ReporteProforma;
import ferrocentro.model.entradas.managers.ManagerEntradas;
//import jdk.internal.loader.AbstractClassLoaderValue.Sub;
import minimarketdemo.controller.JSFUtil;
import minimarketdemo.controller.seguridades.BeanSegLogin;
import minimarketdemo.model.core.entities.Categoria;
import minimarketdemo.model.core.entities.Clasificacion;
import minimarketdemo.model.core.entities.Cliente;
import minimarketdemo.model.core.entities.DetalleEntrada;
import minimarketdemo.model.core.entities.DetalleOrdenCompra;
//import minimarketdemo.model.core.entities.DetalleVenta;
//import minimarketdemo.model.core.entities.DetalleVenta;
//import minimarketdemo.model.core.entities.DetalleVenta;
import minimarketdemo.model.core.entities.Entrada;
import minimarketdemo.model.core.entities.ListaPrecio;
import minimarketdemo.model.core.entities.Marca;
import minimarketdemo.model.core.entities.MetodoPago;
import minimarketdemo.model.core.entities.OrdenCompra;
import minimarketdemo.model.core.entities.Producto;
import minimarketdemo.model.core.entities.Proforma;
import minimarketdemo.model.core.entities.Proveedor;
import minimarketdemo.model.core.entities.Subcategoria;
import minimarketdemo.model.core.entities.TipoPago;
import minimarketdemo.model.core.entities.TipoProducto;
import minimarketdemo.model.core.entities.UnidadMedida;
//import minimarketdemo.model.core.entities.Venta;
//import minimarketdemo.model.core.entities.Venta;

@Named
@SessionScoped
/**
 * @author Edison Pinanjota
 *
 */
public class BeanKymsEntradas implements Serializable {

	@EJB
	private ManagerEntradas mEntrada;

	@Inject
	private BeanSegLogin beanSegLogin;

	// Proveedor
	private List<Proveedor> listaProveedores;
	private Proveedor nuevoProveedor;
	private Proveedor editarProveedor;
	private String identificacionProveedor; // Variable de autocomplete
	private Proveedor informacionProveedor; // Infomacion, despues de ser seleccionado con el autocomplete

	// Unidad Medida
	private UnidadMedida nuevaUnidadM;
	private UnidadMedida editarUnidadM;
	private List<UnidadMedida> listaUnidad;

	// Marca
	private List<Marca> listaMarcas;
	private Marca nuevaMarca;
	private Marca editarMarca;

	// Categoria
	private List<Categoria> listaCategorias;
	private Categoria nuevaCategoria;
	private Categoria editarCategoria;

	// Subcategoria
	private List<Subcategoria> listaSubCat; // reutilizada en productos anterior version
	private Subcategoria nuevaSubCat;
	private Subcategoria editarSubCat;
	private int idCategoria;

	// Clasificación
	private List<Clasificacion> listaClasificaciones; // reutilizada en productos
	private Clasificacion nuevaClasificacion;
	private Clasificacion editarClasificacion;
	private int idSubcategoria;

	// Tipo de producto
	private List<TipoProducto> listaTipoProductos;
	private TipoProducto nuevoTipoProducto;
	private TipoProducto editarTipoProducto;

	// Lista precio
	private List<ListaPrecio> listaListaPrecios;
	private ListaPrecio nuevaListaPrecio;
	private ListaPrecio editarListaPrecio;

	// Producto
	private List<Producto> listaProductos;
	private Producto nuevoProducto;
	private Producto editarProducto;

	private List<TipoPago> listaTipoPagos;
	private List<MetodoPago> listaMetodoPagos;

	private int idUnidadM;
	private int idMarca;
	private int idClasificacion;
	private int idTipoProducto;// ??
	private int idListaPrecio;// ??

	// ================================= REGISTRAR FACTURA
	// ============================================

	private Entrada factura; // Factura registrar
	private Entrada facturaPagar; // Factura que se va a pagar
	private Entrada facturaVer; // Factura seleccionada para ver
//	private Entrada facturaDevolucion; // Factura seleccionada para devolucion
	private int cantidad;
	private int idTipoPago;
	private int idMetodoPago;
	private Date fechPago;
	private boolean PagoSeleccionado; // Select para saber si paga al contado o queda pendiente
	private int mostrarStock;
	private int mostrarStockMinimo;
	private int mostrarStockMaximo;

	private String descuentoFactura;

	private String descripcionProducto;
	private Producto productoSeleccionado; // Producto seleccionado con el autocomplete
	private String numFactura;

	// =============================== VER FACTURAS PARA DEVOLUCION
	// =========================
	private List<Entrada> listaFacturasParaDevolver;
	private Entrada facturaDevolucion; // Factura seleccionada para devolucion
//	private List<DetalleEntrada> detallesListarDevolucion; // Para adicionar a la segunda tabla de la derecha
	private List<DetalleEntrada> detallesDevolucion; // Para adicionar al arreglo que se va a enviar al servidor
	private String descripcionDevolucion;
	private int cantidadDevuelto;
	private boolean devolverTodo;

	// ================================= REGISTRAR PROFORMA
	// ============================================
	private OrdenCompra proforma; // Proforma registrar

	private OrdenCompra proformVer; // Proforma seleccionada para ver
	private OrdenCompra proformFacturar; // Proforma seleccionada para ver
	private int cantidadDetProforma;
	private List<OrdenCompra> listaOrdenCompras;
	private List<OrdenCompra> listaOrdenComprasPorFecha;

	private Date fechaVencimientoOrdenCompra;
	private String numProforma;

	// LISTA PARA CALCULAR TOTALES
	private List<DetalleOrdenCompra> listaTotal;
	private double subtotalFactura;
	private double IVAFactura;
	private double totalFactura;

	/**
	 * 
	 */
	public BeanKymsEntradas() {
		// TODO Auto-generated constructor stub
	}

	@PostConstruct
	public void inicializar() {
		listaProveedores = mEntrada.allProveedors();
		listaUnidad = mEntrada.allUnidadM();
		listaMarcas = mEntrada.allMarcas();
		listaCategorias = mEntrada.allCategorias();
		listaSubCat = mEntrada.allSubCat();
		listaClasificaciones = mEntrada.allClasificaciones();
		listaTipoProductos = mEntrada.allTipoProductos();
		listaListaPrecios = mEntrada.allListaPrecios();
		listaProductos = mEntrada.allProductos();

		listaFacturas = mEntrada.allFacturas();
		listaFacturasParaDevolver = mEntrada.findFacturaDevolverByEstadoDevolver(false);
		listaOrdenCompras = mEntrada.allOrdenCompras();
		listaTipoPagos = mEntrada.allTipoPagos();
		listaMetodoPagos = mEntrada.allMetodoPagos();

		nuevoProveedor = new Proveedor();
		nuevaUnidadM = new UnidadMedida();
		nuevaMarca = new Marca();
		nuevaCategoria = new Categoria();
		nuevaSubCat = new Subcategoria();
		nuevaClasificacion = new Clasificacion();
		nuevoTipoProducto = new TipoProducto();
		nuevaListaPrecio = new ListaPrecio();
		nuevoProducto = new Producto();

		detallesDevolucion = new ArrayList<DetalleEntrada>();
//		detallesListarDevolucion = new ArrayList<DetalleEntrada>();
		devolverTodo = true;
		PagoSeleccionado = true;

		numFactura = mEntrada.numeroFactura(Entrada.class, "idEntrada");
		numProforma = mEntrada.numeroFactura(OrdenCompra.class, "idOrdenCompra");

	}

	/* ============================================================== */
	/* PROVEEDOR */
	/* ============================================================== */
	public void actionSelectProveedor(Proveedor cli) {
		editarProveedor = cli;
	}

	public void actionListenerInsertarProveedor() {
		try {
			mEntrada.insertarProveedor(beanSegLogin.getLoginDTO(), nuevoProveedor);
			nuevoProveedor = new Proveedor();
			listaProveedores = mEntrada.allProveedors();
			JSFUtil.crearMensajeINFO("Proveedor insertado");
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}

	public void actionListenerEliminarProveedor(Proveedor pro) {
		try {
			mEntrada.eliminarProveedor(pro);
			listaProveedores = mEntrada.allProveedors();
			JSFUtil.crearMensajeINFO("Proveedor eliminado");
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}

	public void actionListenerActualizarProveedor() {
		try {
			mEntrada.actualizarProveedor(editarProveedor);
			JSFUtil.crearMensajeINFO("Proveedor actualizado");
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}

	/* ============================================================== */
	/* UNIDAD DE MEDIDA */
	/* ============================================================== */
	public void actionSelectUnidadM(UnidadMedida unid) {
		editarUnidadM = unid;
	}

	public void actionListenerInsertarUnidadM() {
		try {
			mEntrada.insertarUnidadM(beanSegLogin.getLoginDTO(), nuevaUnidadM);
			nuevaUnidadM = new UnidadMedida();
			listaUnidad = mEntrada.allUnidadM();
			JSFUtil.crearMensajeINFO("Unidad de Medida insertado");
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}

	public void actionListenerEliminarUnidadM(UnidadMedida unid) {
		try {
			System.out.println("MENSAJE:.................................: " + unid.getDescripcion());
			mEntrada.eliminarUnidadM(unid);
			listaUnidad = mEntrada.allUnidadM();
			JSFUtil.crearMensajeINFO("Unidad de Medida eliminado");
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}

	public void actionListenerActualizarUnidadMedida() {
		try {
			mEntrada.actualizarUnidadM(editarUnidadM);
			JSFUtil.crearMensajeINFO("Unidad de Medida actualizado");
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}

	/* ============================================================== */
	/* MARCA */
	/* ============================================================== */
	public void actionSelectMarca(Marca cli) {
		editarMarca = cli;
	}

	public void actionListenerInsertarMarca() {
		try {
			mEntrada.insertarMarca(beanSegLogin.getLoginDTO(), nuevaMarca);
			nuevaMarca = new Marca();
			listaMarcas = mEntrada.allMarcas();
			JSFUtil.crearMensajeINFO("Marca insertada");
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}

	public void actionListenerEliminarMarca(Marca pro) {
		try {
			mEntrada.eliminarMarca(pro);
			listaMarcas = mEntrada.allMarcas();
			JSFUtil.crearMensajeINFO("Marca eliminada");
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}

	public void actionListenerActualizarMarca() {
		try {
			mEntrada.actualizarMarca(editarMarca);
			JSFUtil.crearMensajeINFO("Marca actualizada");
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}

	/* ============================================================== */
	/* CATEGORÍA */
	/* ============================================================== */
	public void actionSelectCategoria(Categoria cli) {
		editarCategoria = cli;
	}

	public void actionListenerInsertarCategoria() {
		try {
			mEntrada.insertarCategoria(beanSegLogin.getLoginDTO(), nuevaCategoria);
			nuevaCategoria = new Categoria();
			listaCategorias = mEntrada.allCategorias();
			JSFUtil.crearMensajeINFO("Categoria insertada");
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}

	public void actionListenerEliminarCategoria(Categoria pro) {
		try {
			mEntrada.eliminarCategoria(pro);
			listaCategorias = mEntrada.allCategorias();
			JSFUtil.crearMensajeINFO("Categoria eliminada");
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}

	public void actionListenerActualizarCategoria() {
		try {
			mEntrada.actualizarCategoria(editarCategoria);
			JSFUtil.crearMensajeINFO("Categoria actualizada");
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}

	/* ============================================================== */
	/* SUBCATEGORÍA */
	/* ============================================================== */

	public void actionSelectSubCat(Subcategoria subc) {
		editarSubCat = subc;
	}

	public void actionListenerInsertarSubCat() {
		try {
			mEntrada.insertarSubCat(beanSegLogin.getLoginDTO(), nuevaSubCat, idCategoria);
			nuevaSubCat = new Subcategoria();
			listaSubCat = mEntrada.allSubCat();
			JSFUtil.crearMensajeINFO("Sub Categoria insertada");
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}

	public void actionListenerEliminarSubCat(Subcategoria subc) {
		try {
			mEntrada.eliminarSubCat(subc);
			listaSubCat = mEntrada.allSubCat();
			JSFUtil.crearMensajeINFO("Sub Categoria eliminado");
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}

	public void actionListenerActualizarSubCat() {
		try {
			mEntrada.actualizarSubCat(editarSubCat);
			JSFUtil.crearMensajeINFO("Sub Categoria actualizada");
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}

	/* ============================================================== */
	/* CLASIFICACIÓN */
	/* ============================================================== */

	public void actionSelectClasificacion(Clasificacion cla) {
		editarClasificacion = cla;
	}

	public void actionListenerInsertarClasificacion() {
		try {
			mEntrada.insertarClasificacion(beanSegLogin.getLoginDTO(), nuevaClasificacion, idSubcategoria);
			nuevaClasificacion = new Clasificacion();
			listaClasificaciones = mEntrada.allClasificaciones();
			JSFUtil.crearMensajeINFO("Clasificación insertada");
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}

	public void actionListenerEliminarClasificacion(Clasificacion cla) {
		try {
			mEntrada.eliminarClasificacion(cla);
			listaClasificaciones = mEntrada.allClasificaciones();
			JSFUtil.crearMensajeINFO("Clasificación eliminada");
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}

	public void actionListenerActualizarClasificacion() {
		try {
			mEntrada.actualizarClasificacion(editarClasificacion);
			JSFUtil.crearMensajeINFO("Clasificación actualizada");
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}

	/* ============================================================== */
	/* TIPO PRODUCTO */
	/* ============================================================== */

	public void actionSelectTipoProducto(TipoProducto cli) {
		editarTipoProducto = cli;
	}

	public void actionListenerInsertarTipoProducto() {
		try {
			mEntrada.insertarTipoProducto(beanSegLogin.getLoginDTO(), nuevoTipoProducto);
			nuevoTipoProducto = new TipoProducto();
			listaTipoProductos = mEntrada.allTipoProductos();
			JSFUtil.crearMensajeINFO("Tipo de producto insertado");
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}

	public void actionListenerEliminarTipoProducto(TipoProducto tipro) {
		try {
			mEntrada.eliminarTipoProducto(tipro);
			listaTipoProductos = mEntrada.allTipoProductos();
			JSFUtil.crearMensajeINFO("Tipo de producto eliminado");
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}

	public void actionListenerActualizarTipoProducto() {
		try {
			mEntrada.actualizarTipoProducto(editarTipoProducto);
			JSFUtil.crearMensajeINFO("Tipo producto actualizado");
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}

	/* ============================================================== */
	/* LISTA PRECIO */
	/* ============================================================== */

	public void actionSelectListaPrecio(ListaPrecio cli) {
		editarListaPrecio = cli;
	}

	public void actionListenerInsertarListaPrecio() {
		try {
			mEntrada.insertarListaPrecio(beanSegLogin.getLoginDTO(), nuevaListaPrecio);
			nuevaListaPrecio = new ListaPrecio();
			listaListaPrecios = mEntrada.allListaPrecios();
			JSFUtil.crearMensajeINFO("Lista de precios insertada");
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}

	public void actionListenerEliminarListaPrecio(ListaPrecio pro) {
		try {
			mEntrada.eliminarListaPrecio(pro);
			listaListaPrecios = mEntrada.allListaPrecios();
			JSFUtil.crearMensajeINFO("Lista de precios eliminada");
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}

	public void actionListenerActualizarListaPrecio() {
		try {
			mEntrada.actualizarListaPrecio(editarListaPrecio);
			JSFUtil.crearMensajeINFO("Lista de precios actualizada");
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}

	/* ============================================================== */
	/* PRODUCTO */
	/* ============================================================== */

	public void actionSelectProducto(Producto cli) {
		editarProducto = cli;
	}

	public void mostrarSubcategoria(AjaxBehaviorEvent e) {
		listaSubCat = mEntrada.actionListenerFiltrarSubcategoria(idCategoria);
//		mostrarClasificacion(e);
	}

	public void mostrarClasificacion(AjaxBehaviorEvent e) {
		listaClasificaciones = mEntrada.actionListenerFiltrarClasificacion(idSubcategoria);
	}

	public void actionListenerInsertarProducto() {
		try {
			mEntrada.insertarProducto(beanSegLogin.getLoginDTO(), nuevoProducto, idUnidadM, idMarca, idClasificacion,
					idTipoProducto, idListaPrecio);
			nuevoProducto = new Producto();
			listaProductos = mEntrada.allProductos();
			JSFUtil.crearMensajeINFO("Producto insertado");
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}

	public void actionListenerEliminarProducto(Producto pro) {
		try {
			mEntrada.eliminarProducto(pro);
			listaProductos = mEntrada.allProductos();
			JSFUtil.crearMensajeINFO("Producto eliminado");
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}

	public void actionListenerActualizarProducto() {
		try {
			mEntrada.actualizarProducto(editarProducto);
			JSFUtil.crearMensajeINFO("Producto actualizado");
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}

	/* ============================================================== */
	/* REGISTRAR FACTURA */
	/* ============================================================== */

	public List<String> completeTextProveedor(String query) { // Buscar Proveedor
		String queryLowerCase = query.toLowerCase();
		List<String> proveedorList = new ArrayList<>();
		for (Proveedor proveedor : listaProveedores) {
			proveedorList.add(proveedor.getIdentificacion());
		}
		return proveedorList.stream().filter(t -> t.toLowerCase().startsWith(queryLowerCase))
				.collect(Collectors.toList());
	}

	public List<String> completeTextProducto(String query) { // BuscarProducto
		String queryLowerCase = query.toLowerCase();
		List<String> prodList = new ArrayList<>();
		for (Producto prod : listaProductos) {
			prodList.add(prod.getDescripcion());
		}
		return prodList.stream().filter(t -> t.toLowerCase().startsWith(queryLowerCase)).collect(Collectors.toList());
	}

	public void obtenerInformacionProveedor(AjaxBehaviorEvent e) {
		try {
			informacionProveedor = mEntrada.findOneProveedor(identificacionProveedor);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	public void handleChange() { // Bloquea el calendario de credito
		if (idTipoPago == 2) {
			PagoSeleccionado = false;
		} else {
			PagoSeleccionado = true;
		}
	}

	public void actionListenerAdicionarProducto() throws Exception {
		double resta = productoSeleccionado.getStock() - cantidad;
		if (resta > 0) {
			factura = mEntrada.adicionarProducto(factura, cantidad, productoSeleccionado);
			CalcularFactura(factura);
			cantidad = 0;
			int cont = 0;
			int posicion = 0;
			for (Producto p : listaProductos) {
				if (p.getIdProducto() == productoSeleccionado.getIdProducto()) {
					posicion = cont;
				}
				cont++;
			}
			listaProductos.remove(posicion);

			if (resta <= productoSeleccionado.getStockMinimo()) {
				JSFUtil.crearMensajeWARN("Productos por terminarse STOCK " + resta);
			}

		} else {
			JSFUtil.crearMensajeERROR("No existe suficiente productos");
		}
	}

	public void actionListenerEliminarDetalle(int rowIndex) { // Modificar eliminacion de detalle en la interfaz;
		Producto p = factura.getDetalleEntradas().get(rowIndex).getProducto();
		listaProductos.add(p);
		factura.getDetalleEntradas().remove(rowIndex);
		CalcularFactura(factura);
	}

	public void actionListenerRegistrarFactura() {
		try {
			mEntrada.registrarFactura(beanSegLogin.getLoginDTO(), factura, identificacionProveedor, idTipoPago,
					PagoSeleccionado, fechPago, idMetodoPago, Double.parseDouble(descuentoFactura));
			factura = new Entrada();
			listaProductos = mEntrada.allProductos();
			listaFacturas = mEntrada.allFacturas();
			informacionProveedor = new Proveedor();
			listaFacturasParaDevolver = mEntrada.findFacturaDevolverByEstadoDevolver(false);
			numFactura = mEntrada.numeroFactura(Entrada.class, "idEntrada");
			JSFUtil.crearMensajeINFO("Factura registrada correctamente");
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
		subtotalFactura = 0;
		IVAFactura = 0;
		totalFactura = 0;
		mostrarStock = 0;
	}

	private void CalcularFactura(Entrada facVer) {
		subtotalFactura = 0;
		IVAFactura = 0;
		totalFactura = 0;
		for (int i = 0; i < facVer.getDetalleEntradas().size(); i++) {
			subtotalFactura += facVer.getDetalleEntradas().get(i).getPrecio().doubleValue();
		}
		IVAFactura = Math.round((subtotalFactura * 0.12) * 100.0) / 100.0;
		totalFactura = Math.round((subtotalFactura + IVAFactura) * 100.0) / 100.0;
	}

	public void MostrarStock(AjaxBehaviorEvent e) {
		try {
			String[] parts = descripcionProducto.split(":");
			productoSeleccionado = mEntrada.findOneProducto(parts[0]);
			mostrarStock = productoSeleccionado.getStock();
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	public void GenerarPDFactura() {

//		ReporteFactura m = new ReporteFactura();
//		try {
//			m.Generar(facturaVer, subtotalFactura, IVAFactura);???
//			JSFUtil.crearMensajeINFO("Reporte generado");
//		} catch (FileNotFoundException e) {
//			e.printStackTrace();
//		} catch (DocumentException e) {
//			e.printStackTrace();
//		}
	}

	// ================================= REGISTRAR PROFORMA
	// ============================================

	public void actionListenerAdicionarProductoProforma() throws Exception {
		double resta = productoSeleccionado.getStock() - cantidadDetProforma;
		if (resta > 0) {
			proforma = mEntrada.adicionarProductoOrdenCompra(proforma, cantidadDetProforma, productoSeleccionado);
			CalcularProforma(proforma);
			cantidadDetProforma = 0;

			int cont = 0;
			int posicion = 0;
			for (Producto p : listaProductos) {
				if (p.getIdProducto() == productoSeleccionado.getIdProducto()) {
					posicion = cont;
				}
				cont++;
			}
			listaProductos.remove(posicion);

			if (resta <= productoSeleccionado.getStockMinimo()) {
				JSFUtil.crearMensajeWARN("Productos por terminarse STOCK: " + resta);// ??
			}

		} else {
			JSFUtil.crearMensajeERROR("No existe suficientes productos");// ??
		}
	}

	public void actionListenerEliminarDetalleProforma(int rowIndex) { // Modificar eliminacion de detalle en la
																		// interfaz;
		Producto p = proforma.getDetalleOrdenCompras().get(rowIndex).getProducto();
		listaProductos.add(p);
		proforma.getDetalleOrdenCompras().remove(rowIndex);
		CalcularProforma(proforma);
	}

	public void actionListenerRegistrarOrdenCompra() {
		try {
			mEntrada.registrarOrdenCompra(beanSegLogin.getLoginDTO(), proforma, identificacionProveedor,
					fechaVencimientoOrdenCompra, Double.parseDouble(descuentoFactura));
			proforma = new OrdenCompra();
			listaProductos = mEntrada.allProductos();
			listaOrdenCompras = mEntrada.allOrdenCompras();
			numProforma = mEntrada.numeroFactura(OrdenCompra.class, "idOrdenCompra");
			JSFUtil.crearMensajeINFO("Orden de Compra registrada correctamente");
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
		subtotalFactura = 0;
		IVAFactura = 0;
		totalFactura = 0;
		mostrarStock = 0;
	}

//	========================PROFORMA--->FACTURA
	public void actionListenerRegistrarProformaParaFacturar() { // Facturar Profroma
		try {
			Entrada facturaProforma = new Entrada();
			facturaProforma.setDetalleEntradas(new ArrayList<DetalleEntrada>());
			facturaProforma.setFecha(new Date());
			facturaProforma.setTotal(new BigDecimal(0));
			facturaProforma.setEstado(true);

			for (int i = 0; i < proformFacturar.getDetalleOrdenCompras().size(); i++) {
				DetalleEntrada detalle = new DetalleEntrada();
				detalle.setCantidad(proformFacturar.getDetalleOrdenCompras().get(i).getCantidad());
				detalle.setPrecio(
						new BigDecimal(proformFacturar.getDetalleOrdenCompras().get(i).getPrecio().doubleValue())
								.setScale(2, RoundingMode.HALF_UP));
				detalle.setEntrada(facturaProforma);
				detalle.setProducto(proformFacturar.getDetalleOrdenCompras().get(i).getProducto());
				facturaProforma.getDetalleEntradas().add(detalle);
			}

			facturaProforma.setTotal(facturaProforma.getTotal()
					.add(new BigDecimal(proformFacturar.getTotal().doubleValue()).setScale(2, RoundingMode.HALF_UP)));
			facturaProforma.setProveedor(proformFacturar.getProveedor());
			facturaProforma.setImpuesto(
					new BigDecimal(proformFacturar.getImpuesto().doubleValue()).setScale(2, RoundingMode.HALF_UP));
			mEntrada.registrarOrdenCompraEntrada(beanSegLogin.getLoginDTO(), facturaProforma, idTipoPago,
					PagoSeleccionado, fechPago, idMetodoPago);
			facturaProforma = new Entrada();
			listaProductos = mEntrada.allProductos();
			listaFacturas = mEntrada.allFacturas();
			listaFacturasParaDevolver = mEntrada.findFacturaDevolverByEstadoDevolver(false);
			numFactura = mEntrada.numeroFactura(Entrada.class, "idEntrada");
			JSFUtil.crearMensajeINFO("Factura registrada correctamente");
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
		subtotalFactura = 0;
		IVAFactura = 0;
		totalFactura = 0;
		mostrarStock = 0;
	}
	
//	========================PROFORMA--->FACTURA 2
	public void actionListenerRegistrarProformaParaFacturar2() { // Facturar Profroma
		try {
//			Entrada facturaProforma = new Entrada();
//			factura=null;
			factura.setDetalleEntradas(new ArrayList<DetalleEntrada>());
			factura.setFecha(new Date());
			factura.setTotal(new BigDecimal(0));
			factura.setEstado(true);

			for (int i = 0; i < proformFacturar.getDetalleOrdenCompras().size(); i++) {
				DetalleEntrada detalle = new DetalleEntrada();
				detalle.setCantidad(proformFacturar.getDetalleOrdenCompras().get(i).getCantidad());
				detalle.setPrecio(
						new BigDecimal(proformFacturar.getDetalleOrdenCompras().get(i).getPrecio().doubleValue())
								.setScale(2, RoundingMode.HALF_UP));
				detalle.setEntrada(factura);
				detalle.setProducto(proformFacturar.getDetalleOrdenCompras().get(i).getProducto());
				factura.getDetalleEntradas().add(detalle);

			}

			factura.setTotal(factura.getTotal()
					.add(new BigDecimal(proformFacturar.getTotal().doubleValue()).setScale(2, RoundingMode.HALF_UP)));
			factura.setProveedor(proformFacturar.getProveedor());
			factura.setImpuesto(
					new BigDecimal(proformFacturar.getImpuesto().doubleValue()).setScale(2, RoundingMode.HALF_UP));
			mEntrada.registrarOrdenCompraEntrada(beanSegLogin.getLoginDTO(), factura, idTipoPago,
					PagoSeleccionado, fechPago, idMetodoPago);
			factura = new Entrada();
			listaProductos = mEntrada.allProductos();
			listaFacturas = mEntrada.allFacturas();
			listaFacturasParaDevolver = mEntrada.findFacturaDevolverByEstadoDevolver(false);
			numFactura = mEntrada.numeroFactura(Entrada.class, "idEntrada");
			JSFUtil.crearMensajeINFO("Factura registrada correctamente");
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
		subtotalFactura = 0;
		IVAFactura = 0;
		totalFactura = 0;
		mostrarStock = 0;
	}

	public String actionCargarProforma(OrdenCompra prof) {
		proformVer = prof;
		CalcularProforma(proformVer);
		return "modeloOrdenCompra";
	}

	public String actionCargarProformaParaFacturar(OrdenCompra prof) {
		proformFacturar = prof;
		CalcularProforma(proformFacturar);
//		return "factura";
		return "facturarOrdenCompra";
	}

	public void actionListenerAnularProforma(OrdenCompra prof) {
		try {
			mEntrada.anularOrdenCompra(prof);
			JSFUtil.crearMensajeINFO("Orden de Compra Anulada");
			listaOrdenCompras = mEntrada.allProformas();
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}

	public void actionCargarFacturarProforma(OrdenCompra prof) {
		try {
			mEntrada.anularOrdenCompra(prof);
			JSFUtil.crearMensajeINFO("Orden de Compra Anulada");
			listaOrdenCompras = mEntrada.allProformas();
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}

	public void actionListarProformasByFechas(AjaxBehaviorEvent e) {
		try {
			listaOrdenComprasPorFecha = mEntrada.findProformaByFechas(fechaInicio, fechaFin);
		} catch (Exception e1) {
			System.out.println("Aun nos has seleccionado la fecha:" + e1.getMessage());
			e1.printStackTrace();
		}
	}

	private void CalcularProforma(OrdenCompra proforma) {
		subtotalFactura = 0;
		IVAFactura = 0;
		totalFactura = 0;
		for (int i = 0; i < proforma.getDetalleOrdenCompras().size(); i++) {
			subtotalFactura += proforma.getDetalleOrdenCompras().get(i).getPrecio().doubleValue();
		}
		IVAFactura = Math.round((subtotalFactura * 0.12) * 100.0) / 100.0;
		totalFactura = Math.round((subtotalFactura + IVAFactura) * 100.0) / 100.0;
	}

	// ========================== PAGAR FACTURAS CON CREDITO
	// ==================================
	private double valorPagarFactura;

	public void actionCargarFacturaPagar(Entrada fac) {
		facturaPagar = fac;
	}

	public void actionListenerPagarFactura() {

		try {
			mEntrada.pagarFactura(facturaPagar, valorPagarFactura);
			JSFUtil.crearMensajeINFO("Pago Registrado");
			listaFacturasPorEstado = mEntrada.findFacturaByEstadoEntrada();
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}

	// ==================================== VER FACTURAS
	// =========================================================
	private List<Entrada> listaFacturas;
	private List<Entrada> listaFacturasPorEstado;
	private List<Entrada> listaFacturasPorFecha;
	private List<Entrada> listaFacturasPorProveedor;
	private int buscarProveedor; // Para listar facturas por cliente
	private Date fechaInicio; // Buscar por fecha
	private Date fechaFin; // Buscar por fecha

	public String actionCargarFactura(Entrada factura) {
		facturaVer = factura;
		CalcularFactura(facturaVer);
		return "modeloFactura";
	}

	public void actionListenerAnularFactura(Entrada factura) {
		try {
			mEntrada.anularFactura(factura);
			JSFUtil.crearMensajeINFO("Factura Anulada");
			listaFacturas = mEntrada.allFacturas();
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}

	public void actionListarAllFacturas() {
		listaFacturas = mEntrada.allFacturas();
	}

	public void actionListarFacturasByEstado() {
		listaFacturasPorEstado = mEntrada.findFacturaByEstadoEntrada();
	}

	public void actionListarFacturasByFechas(AjaxBehaviorEvent e) {
		try {
			listaFacturasPorFecha = mEntrada.findFacturaByFechas(fechaInicio, fechaFin);
		} catch (Exception e1) {
			System.out.println("Aun nos has seleccionado la fecha:" + e1.getMessage());
			e1.printStackTrace();
		}
	}

	public void actionListarFacturasByProveedor() {
		listaFacturasPorProveedor = mEntrada.findFacturaByProveedor(buscarProveedor);
	}

	// =============================== VER FACTURAS PARA DEVOLUCION
	// =========================
	public String actionSelectFacturaDevolucion(Entrada fac) {
		descripcionDevolucion = "";
		facturaDevolucion = fac;
		return "devolucion";
	}

	public void actionAdicionarDetalleDevolucion(DetalleEntrada dta) { // Para adicionar al arreglo que se va a enviar
																		// al
																		// servidor
		JSFUtil.crearMensajeINFO("Detalle Seleccionado");
		detallesDevolucion.add(dta);
	}

	public void actionListenerRegistrarDevolucion() {
		try {
			mEntrada.registarDevolucion(beanSegLogin.getLoginDTO(), facturaDevolucion, descripcionDevolucion,
					detallesDevolucion, devolverTodo);
			JSFUtil.crearMensajeINFO("Devolución Registrada");
			facturaDevolucion = new Entrada();
			descripcionDevolucion = "";
			devolverTodo = true;
			listaFacturasParaDevolver = mEntrada.findFacturaDevolverByEstadoDevolver(false);
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}

	/* ===========GETTERS AND SETTERS========= */

	public List<Proveedor> getListaProveedores() {
		return listaProveedores;
	}

	public void setListaProveedores(List<Proveedor> listaProveedores) {
		this.listaProveedores = listaProveedores;
	}

	public Proveedor getNuevoProveedor() {
		return nuevoProveedor;
	}

	public void setNuevoProveedor(Proveedor nuevoProveedor) {
		this.nuevoProveedor = nuevoProveedor;
	}

	public Proveedor getEditarProveedor() {
		return editarProveedor;
	}

	public void setEditarProveedor(Proveedor editarProveedor) {
		this.editarProveedor = editarProveedor;
	}

	/* ===========GETTERS AND SETTERS========= */

	public List<Marca> getListaMarcas() {
		return listaMarcas;
	}

	public void setListaMarcas(List<Marca> listaMarcas) {
		this.listaMarcas = listaMarcas;
	}

	public Marca getNuevaMarca() {
		return nuevaMarca;
	}

	public void setNuevaMarca(Marca nuevaMarca) {
		this.nuevaMarca = nuevaMarca;
	}

	public Marca getEditarMarca() {
		return editarMarca;
	}

	public void setEditarMarca(Marca editarMarca) {
		this.editarMarca = editarMarca;
	}

	/* ===========GETTERS AND SETTERS========= */

	public List<UnidadMedida> getListaUnidad() {
		return listaUnidad;
	}

	public void setListaUnidad(List<UnidadMedida> listaUnidad) {
		this.listaUnidad = listaUnidad;
	}

	public UnidadMedida getNuevaUnidadM() {
		return nuevaUnidadM;
	}

	public void setNuevaUnidadM(UnidadMedida nuevaUnidadM) {
		this.nuevaUnidadM = nuevaUnidadM;
	}

	public UnidadMedida getEditarUnidadM() {
		return editarUnidadM;
	}

	public void setEditarUnidadM(UnidadMedida editarUnidadM) {
		this.editarUnidadM = editarUnidadM;
	}

	/* ===========GETTERS AND SETTERS========= */

	public List<Categoria> getListaCategorias() {
		return listaCategorias;
	}

	public void setListaCategorias(List<Categoria> listaCategorias) {
		this.listaCategorias = listaCategorias;
	}

	public Categoria getNuevaCategoria() {
		return nuevaCategoria;
	}

	public void setNuevaCategoria(Categoria nuevaCategoria) {
		this.nuevaCategoria = nuevaCategoria;
	}

	public Categoria getEditarCategoria() {
		return editarCategoria;
	}

	public void setEditarCategoria(Categoria editarCategoria) {
		this.editarCategoria = editarCategoria;
	}

	/* ===========GETTERS AND SETTERS========= */

	public List<Subcategoria> getListaSubCat() {
		return listaSubCat;
	}

	public void setListaSubCat(List<Subcategoria> listaSubCat) {
		this.listaSubCat = listaSubCat;
	}

	public Subcategoria getNuevaSubCat() {
		return nuevaSubCat;
	}

	public void setNuevaSubCat(Subcategoria nuevaSubCat) {
		this.nuevaSubCat = nuevaSubCat;
	}

	public Subcategoria getEditarSubCat() {
		return editarSubCat;
	}

	public void setEditarSubCat(Subcategoria editarSubCat) {
		this.editarSubCat = editarSubCat;
	}

	public int getIdCategoria() {
		return idCategoria;
	}

	public void setIdCategoria(int idCategoria) {
		this.idCategoria = idCategoria;
	}

	/* ===========GETTERS AND SETTERS========= */

	public Clasificacion getNuevaClasificacion() {
		return nuevaClasificacion;
	}

	public List<Clasificacion> getListaClasificaciones() {
		return listaClasificaciones;
	}

	public void setListaClasificaciones(List<Clasificacion> listaClasificaciones) {
		this.listaClasificaciones = listaClasificaciones;
	}

	public void setNuevaClasificacion(Clasificacion nuevaClasificacion) {
		this.nuevaClasificacion = nuevaClasificacion;
	}

	public Clasificacion getEditarClasificacion() {
		return editarClasificacion;
	}

	public void setEditarClasificacion(Clasificacion editarClasificacion) {
		this.editarClasificacion = editarClasificacion;
	}

	/* ===========GETTERS AND SETTERS========= */
	public int getIdSubcategoria() {
		return idSubcategoria;
	}

	public void setIdSubcategoria(int idSubcategoria) {
		this.idSubcategoria = idSubcategoria;
	}

	public int getIdClasificacion() {
		return idClasificacion;
	}

	public void setIdClasificacion(int idClasificacion) {
		this.idClasificacion = idClasificacion;
	}

	/* ===========GETTERS AND SETTERS========= */

	public List<TipoProducto> getListaTipoProductos() {
		return listaTipoProductos;
	}

	public void setListaTipoProductos(List<TipoProducto> listaTipoProductos) {
		this.listaTipoProductos = listaTipoProductos;
	}

	public TipoProducto getNuevoTipoProducto() {
		return nuevoTipoProducto;
	}

	public void setNuevoTipoProducto(TipoProducto nuevoTipoProducto) {
		this.nuevoTipoProducto = nuevoTipoProducto;
	}

	public TipoProducto getEditarTipoProducto() {
		return editarTipoProducto;
	}

	public void setEditarTipoProducto(TipoProducto editarTipoProducto) {
		this.editarTipoProducto = editarTipoProducto;
	}

	/* ===========GETTERS AND SETTERS========= */

	public List<ListaPrecio> getListaListaPrecios() {
		return listaListaPrecios;
	}

	public void setListaListaPrecios(List<ListaPrecio> listaListaPrecios) {
		this.listaListaPrecios = listaListaPrecios;
	}

	public ListaPrecio getNuevaListaPrecio() {
		return nuevaListaPrecio;
	}

	public void setNuevaListaPrecio(ListaPrecio nuevaListaPrecio) {
		this.nuevaListaPrecio = nuevaListaPrecio;
	}

	public ListaPrecio getEditarListaPrecio() {
		return editarListaPrecio;
	}

	public void setEditarListaPrecio(ListaPrecio editarListaPrecio) {
		this.editarListaPrecio = editarListaPrecio;
	}

	/* ===========GETTERS AND SETTERS========= */

	public List<Producto> getListaProductos() {
		return listaProductos;
	}

	public void setListaProductos(List<Producto> listaProductos) {
		this.listaProductos = listaProductos;
	}

	public Producto getNuevoProducto() {
		return nuevoProducto;
	}

	public void setNuevoProducto(Producto nuevoProducto) {
		this.nuevoProducto = nuevoProducto;
	}

	public Producto getEditarProducto() {
		return editarProducto;
	}

	public void setEditarProducto(Producto editarProducto) {
		this.editarProducto = editarProducto;
	}

	public int getIdUnidadM() {
		return idUnidadM;
	}

	public void setIdUnidadM(int idUnidadM) {
		this.idUnidadM = idUnidadM;
	}

	public int getIdMarca() {
		return idMarca;
	}

	public void setIdMarca(int idMarca) {
		this.idMarca = idMarca;
	}

	public int getIdTipoProducto() {
		return idTipoProducto;
	}

	public void setIdTipoProducto(int idTipoProducto) {
		this.idTipoProducto = idTipoProducto;
	}

	public int getIdListaPrecio() {
		return idListaPrecio;
	}

	public void setIdListaPrecio(int idListaPrecio) {
		this.idListaPrecio = idListaPrecio;
	}

	public Date getFechaVencimientoOrdenCompra() {
		return fechaVencimientoOrdenCompra;
	}

	public void setFechaVencimientoOrdenCompra(Date fechaVencimientoOrdenCompra) {
		this.fechaVencimientoOrdenCompra = fechaVencimientoOrdenCompra;
	}

	public String getIdentificacionProveedor() {
		return identificacionProveedor;
	}

	public void setIdentificacionProveedor(String identificacionProveedor) {
		this.identificacionProveedor = identificacionProveedor;
	}

	public Proveedor getInformacionProveedor() {
		return informacionProveedor;
	}

	public void setInformacionProveedor(Proveedor informacionProveedor) {
		this.informacionProveedor = informacionProveedor;
	}

	public Entrada getFactura() {
		return factura;
	}

	public void setFactura(Entrada factura) {
		this.factura = factura;
	}

	public Entrada getFacturaPagar() {
		return facturaPagar;
	}

	public void setFacturaPagar(Entrada facturaPagar) {
		this.facturaPagar = facturaPagar;
	}

	public Entrada getFacturaVer() {
		return facturaVer;
	}

	public void setFacturaVer(Entrada facturaVer) {
		this.facturaVer = facturaVer;
	}

	public Integer getCantidad() {
		return cantidad;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}

	public int getIdTipoPago() {
		return idTipoPago;
	}

	public void setIdTipoPago(int idTipoPago) {
		this.idTipoPago = idTipoPago;
	}

	public int getIdMetodoPago() {
		return idMetodoPago;
	}

	public void setIdMetodoPago(int idMetodoPago) {
		this.idMetodoPago = idMetodoPago;
	}

	public Date getFechPago() {
		return fechPago;
	}

	public void setFechPago(Date fechPago) {
		this.fechPago = fechPago;
	}

	public boolean isPagoSeleccionado() {
		return PagoSeleccionado;
	}

	public void setPagoSeleccionado(boolean pagoSeleccionado) {
		PagoSeleccionado = pagoSeleccionado;
	}

	public Integer getMostrarStock() {
		return mostrarStock;
	}

	public void setMostrarStock(Integer mostrarStock) {
		this.mostrarStock = mostrarStock;
	}

	public String getDescripcionProducto() {
		return descripcionProducto;
	}

	public void setDescripcionProducto(String descripcionProducto) {
		this.descripcionProducto = descripcionProducto;
	}

	public Producto getProductoSeleccionado() {
		return productoSeleccionado;
	}

	public void setProductoSeleccionado(Producto productoSeleccionado) {
		this.productoSeleccionado = productoSeleccionado;
	}

	public List<Entrada> getListaFacturasParaDevolver() {
		return listaFacturasParaDevolver;
	}

	public void setListaFacturasParaDevolver(List<Entrada> listaFacturasParaDevolver) {
		this.listaFacturasParaDevolver = listaFacturasParaDevolver;
	}

	public Entrada getFacturaDevolucion() {
		return facturaDevolucion;
	}

	public void setFacturaDevolucion(Entrada facturaDevolucion) {
		this.facturaDevolucion = facturaDevolucion;
	}

	public List<DetalleEntrada> getDetallesDevolucion() {
		return detallesDevolucion;
	}

	public void setDetallesDevolucion(List<DetalleEntrada> detallesDevolucion) {
		this.detallesDevolucion = detallesDevolucion;
	}

	public String getDescripcionDevolucion() {
		return descripcionDevolucion;
	}

	public void setDescripcionDevolucion(String descripcionDevolucion) {
		this.descripcionDevolucion = descripcionDevolucion;
	}

	public int getCantidadDevuelto() {
		return cantidadDevuelto;
	}

	public void setCantidadDevuelto(int cantidadDevuelto) {
		this.cantidadDevuelto = cantidadDevuelto;
	}

	public boolean isDevolverTodo() {
		return devolverTodo;
	}

	public void setDevolverTodo(boolean devolverTodo) {
		this.devolverTodo = devolverTodo;
	}

	public OrdenCompra getProforma() {
		return proforma;
	}

	public void setProforma(OrdenCompra proforma) {
		this.proforma = proforma;
	}

	public OrdenCompra getProformVer() {
		return proformVer;
	}

	public void setProformVer(OrdenCompra proformVer) {
		this.proformVer = proformVer;
	}

	public OrdenCompra getProformFacturar() {
		return proformFacturar;
	}

	public void setProformFacturar(OrdenCompra proformFacturar) {
		this.proformFacturar = proformFacturar;
	}

	public int getCantidadDetProforma() {
		return cantidadDetProforma;
	}

	public void setCantidadDetProforma(int cantidadDetProforma) {
		this.cantidadDetProforma = cantidadDetProforma;
	}

	public List<DetalleOrdenCompra> getListaTotal() {
		return listaTotal;
	}

	public void setListaTotal(List<DetalleOrdenCompra> listaTotal) {
		this.listaTotal = listaTotal;
	}

	public double getSubtotalFactura() {
		return subtotalFactura;
	}

	public void setSubtotalFactura(double subtotalFactura) {
		this.subtotalFactura = subtotalFactura;
	}

	public double getIVAFactura() {
		return IVAFactura;
	}

	public void setIVAFactura(double iVAFactura) {
		IVAFactura = iVAFactura;
	}

	public double getTotalFactura() {
		return totalFactura;
	}

	public void setTotalFactura(double totalFactura) {
		this.totalFactura = totalFactura;
	}

	public List<Entrada> getListaFacturas() {
		return listaFacturas;
	}

	public void setListaFacturas(List<Entrada> listaFacturas) {
		this.listaFacturas = listaFacturas;
	}

	public List<Entrada> getListaFacturasPorEstado() {
		return listaFacturasPorEstado;
	}

	public void setListaFacturasPorEstado(List<Entrada> listaFacturasPorEstado) {
		this.listaFacturasPorEstado = listaFacturasPorEstado;
	}

	public List<Entrada> getListaFacturasPorFecha() {
		return listaFacturasPorFecha;
	}

	public void setListaFacturasPorFecha(List<Entrada> listaFacturasPorFecha) {
		this.listaFacturasPorFecha = listaFacturasPorFecha;
	}

	public List<Entrada> getListaFacturasPorProveedor() {
		return listaFacturasPorProveedor;
	}

	public void setListaFacturasPorProveedor(List<Entrada> listaFacturasPorProveedor) {
		this.listaFacturasPorProveedor = listaFacturasPorProveedor;
	}

	public int getBuscarProveedor() {
		return buscarProveedor;
	}

	public void setBuscarProveedor(int buscarProveedor) {
		this.buscarProveedor = buscarProveedor;
	}

	public Date getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public Date getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}

	public double getValorPagarFactura() {
		return valorPagarFactura;
	}

	public void setValorPagarFactura(double valorPagarFactura) {
		this.valorPagarFactura = valorPagarFactura;
	}

	public List<TipoPago> getListaTipoPagos() {
		return listaTipoPagos;
	}

	public void setListaTipoPagos(List<TipoPago> listaTipoPagos) {
		this.listaTipoPagos = listaTipoPagos;
	}

	public List<MetodoPago> getListaMetodoPagos() {
		return listaMetodoPagos;
	}

	public void setListaMetodoPagos(List<MetodoPago> listaMetodoPagos) {
		this.listaMetodoPagos = listaMetodoPagos;
	}

	public List<OrdenCompra> getListaOrdenCompras() {
		return listaOrdenCompras;
	}

	public void setListaOrdenCompras(List<OrdenCompra> listaOrdenCompras) {
		this.listaOrdenCompras = listaOrdenCompras;
	}

	public List<OrdenCompra> getListaOrdenComprasPorFecha() {
		return listaOrdenComprasPorFecha;
	}

	public void setListaOrdenComprasPorFecha(List<OrdenCompra> listaOrdenComprasPorFecha) {
		this.listaOrdenComprasPorFecha = listaOrdenComprasPorFecha;
	}

	public int getMostrarStockMinimo() {
		return mostrarStockMinimo;
	}

	public void setMostrarStockMinimo(int mostrarStockMinimo) {
		this.mostrarStockMinimo = mostrarStockMinimo;
	}

	public int getMostrarStockMaximo() {
		return mostrarStockMaximo;
	}

	public void setMostrarStockMaximo(int mostrarStockMaximo) {
		this.mostrarStockMaximo = mostrarStockMaximo;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public void setMostrarStock(int mostrarStock) {
		this.mostrarStock = mostrarStock;
	}

	public String getDescuentoFactura() {
		return descuentoFactura;
	}

	public void setDescuentoFactura(String descuentoFactura) {
		this.descuentoFactura = descuentoFactura;
	}

	public String getNumProforma() {
		return numProforma;
	}

	public void setNumProforma(String numProforma) {
		this.numProforma = numProforma;
	}

	public String getNumFactura() {
		return numFactura;
	}

	public void setNumFactura(String numFactura) {
		this.numFactura = numFactura;
	}
	
	public void GenerarPDFProforma() {
//		ReporteProforma m = new ReporteProforma();
//		try {
//			m.Generar(proformVer, subtotalFactura, IVAFactura);
//			JSFUtil.crearMensajeINFO("Reporte generado");
//		} catch (FileNotFoundException e) {
//			e.printStackTrace();
//		} catch (DocumentException e) {
//			e.printStackTrace();
//		}
	}

}