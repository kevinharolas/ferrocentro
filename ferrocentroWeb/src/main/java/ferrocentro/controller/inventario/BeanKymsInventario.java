package ferrocentro.controller.inventario;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import ferrocentro.model.inventario.managers.ManagerInventario;
import ferrocentro.model.ventas.managers.ManagerVentas;
import minimarketdemo.controller.JSFUtil;
import minimarketdemo.model.core.entities.DevolucionVenta;
import minimarketdemo.model.core.entities.Venta;

@Named
@SessionScoped
public class BeanKymsInventario implements Serializable {

	@EJB
	private ManagerInventario mInventario;
	
	//Listas
	private List<DevolucionVenta> listaDevoluciones;
	
	public BeanKymsInventario() {
		// TODO Auto-generated constructor stub
	}
	@PostConstruct
	public void inicializar() {
		listaDevoluciones=mInventario.findAllDevolucionesTotales();
	}

	
	//==============================DEVOLUCION ======================
	private DevolucionVenta devolucionVer;
	private boolean tipoDevolucion;
	public String actionCargarDevolucion(DevolucionVenta dev) {
		devolucionVer = dev;
		return "modeloDevolucion";
	}
	
	
	public void actionListenerAnularDevolucion(DevolucionVenta dev) {
		System.out.print("anular");
	}
	
	
	//Listar Devoluciones
	public void listarDevoluciones() {
		if(tipoDevolucion) {
			listaDevoluciones=mInventario.findAllDevolucionesTotales();
			
		}else {
			listaDevoluciones=mInventario.findAllDevolucionesParciales();
			
		}
	}
	
	
	

	//=====================GETTERS AND SETTERS ==========================
	
	public List<DevolucionVenta> getListaDevoluciones() {
		return listaDevoluciones;
	}
	public void setListaDevoluciones(List<DevolucionVenta> listaDevoluciones) {
		this.listaDevoluciones = listaDevoluciones;
		
	}
	public DevolucionVenta getDevolucionVer() {
		return devolucionVer;
	}
	public void setDevolucionVer(DevolucionVenta devolucionVer) {
		this.devolucionVer = devolucionVer;
	}
	public boolean isTipoDevolucion() {
		return tipoDevolucion;
	}
	public void setTipoDevolucion(boolean tipoDevolucion) {
		this.tipoDevolucion = tipoDevolucion;
	}
	
	

}
